<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Slug;
use app\components\Sql;
use app\components\helpers\DataTypeHelper;
use app\models\Gender;
use app\models\Colors;
use app\models\Size;
use app\models\Categories;
use app\models\Filters;

class Products extends Model
{
	public $filter;
	public $product;
	public $gender = [
		0 => 'undefined',
		1 => 'Unisex',
		2 => 'Women',
		3 => 'Men',
		4 => 'Boys',
	];
	public $designers_cats = false;

	public function allBrands(){
		$uniqBrend = (new \yii\db\Query())
			->select(['id', 'brand'])
	    	->from('products')
			->groupBy('brand')
			->distinct()
	    	->all();

		return 1;
	}

	/*
	Modified
	*/

	public function setFilter($key, $value) {
		$this->filter[$key] = $value;
	}

	public function getFilter($key = false) {
		if ($key === false) {
			if (empty($this->filter))
				return false;

			return $this->filter;
		}

		if (empty($this->filter[$key]))
			return false;

		return $this->filter[$key];
	}


	public function get_products_data_xml() {
		$Filter = new Filters();
		$Sql = new Sql();
		$Sql->enable_log = false;


		$filter = $this->getFilter();
		if (empty($filter["offset"]))
			$filter["offset"] = 0;
		$Sql->set_limit($filter["offset"], $filter["limit"]);
		$q = $Sql->sl("product_slug", "is_visible=1");

		return $q;

	}
	 public function products_by_gender($gender_id) {
		 $Sql = new Sql();
		 $Sql->enable_log = false;

		 $leftJoins[] 	=' LEFT JOIN product_gender 	 	 ON product_gender.product_id 		= product_slug.product_id ';
		 $where[] 			= ' AND product_gender.gender_id=:gender_id ';
		 $where_params["gender_id"] = $gender_id;


		 $products = $Sql->select("

				SELECT * FROM product_slug

				LEFT JOIN product_price ON product_price.product_id = product_slug.product_id AND product_price.type = 'retail'
				" . implode(" ", $leftJoins) . "

				WHERE
						product_slug.is_visible=1
						" . implode(" ", $where) . "

					" . $this->get_limit() . "",

				$where_params);


				$cnt = $Sql->select("

	 				SELECT count(distinct(product_slug.product_id)) as cnt FROM product_slug

	 				LEFT JOIN product_price ON product_price.product_id = product_slug.product_id AND product_price.type = 'retail'
	 				" . implode(" ", $leftJoins) . "

	 				WHERE
	 						product_slug.is_visible=1
	 						" . implode(" ", $where) . "

	 					",

	 				$where_params);

			if (!empty($products)) {
				$ids = array();
				foreach ($products as $product)
					$ids[] = $product["product_id"];


				$products_data = $Sql->select("SELECT name, URL_productImage as image, product_id  FROM products WHERE product_id IN (in:sku)", array("in:sku" =>$ids), "product_id");


				foreach($products as $i => $product) {

					if (empty($products_data[$product["product_id"]]))
						continue;

					foreach ($products_data as $k => $v)
						foreach($v as $k2=>$v2)
							$products[$i][$k2] = $v2;

				}


			}

			if (empty($cnt))
				$cnt[0]["cnt"] = 0;

			return array("products" =>$products, "count" => $cnt[0]["cnt"]);
	 }

	public function get_products_data() {
		$Filter = new Filters();
		$Sql = new Sql();
		$Sql->enable_log = false;


		$filter = $this->getFilter();


		$cnt = 0;
		$where = array();
		foreach ($filter as $key => $value) {

			if ($key == "limit" OR $key == "offset")
				continue;
			$cnt++;
			$Filter->set_param($key, $value);

			if (!is_array($value))
				$value = array($value);

			if ($key == "color_id")
				$where[]= " product_colors." . $key . " IN (in:". $key . ") ";
			else if ($key == "brand_id")
				$where[]= " product_brand." . $key . " IN (in:". $key . ") ";
			else if ($key == "gender_id")
				$where[]= " (product_gender." . $key . " IN (in:". $key . ") OR product_gender.gender_id=3) ";
			else if ($key == "size_id")
				$where[]= " product_size." . $key . " IN (in:". $key . ") ";
			else if ($key == "category_id")
				$where[]= " product_categories." . $key . " IN (in:". $key . ") ";
			else
				$where[]= " " . $key . " IN (in:". $key . ") ";

			if ($key == "category_id")
				$value = $this->get_related_categories($value);
			$where_data["in:" . $key] = $value;
		}

		$limit = 24;
		$offset = 0;

		if (!empty($filter["limit"]))
			$limit = $filter["limit"];

		if (!empty($filter["offset"]))
			$offset = $filter["offset"];


		if ($cnt == 1 AND !empty($filter["gender_id"])) {
			return $this->products_by_gender($filter["gender_id"]);
		}

		$count = 0;

		//$where[] =" product_slug.is_visible=1 ";
		if (empty($where)) {
			 $where[] = ' 1 = 1';
			 $where_data =[];
		 }

		$products = $Sql->select("
      SELECT *,product_slug.is_visible as is_visible, product_slug.product_id as product_id,
			products.name as name, URL_productImage as image
			 FROM product_slug
      LEFT JOIN product_colors      ON product_colors.product_id      = product_slug.product_id
      LEFT JOIN product_categories  ON product_categories.product_id  = product_slug.product_id
      LEFT JOIN product_size        ON product_size.product_id        = product_slug.product_id
      LEFT JOIN product_gender      ON product_gender.product_id      = product_slug.product_id
      LEFT JOIN product_brand       ON product_brand.product_id       = product_slug.product_id
			LEFT JOIN products       			ON products.id       = product_slug.product_id
			LEFT JOIN product_price        ON product_price.product_id      = product_slug.product_id AND product_price.type='retail'
			WHERE  " . implode(" AND " , $where) . "

			GROUP BY product_slug.product_id
			limit " . $offset . "," . $limit . "
    ", $where_data);



		$count = $Sql->select("
      SELECT count(distinct(product_slug.product_id)) as cnt
			 FROM product_slug
      LEFT JOIN product_colors      ON product_colors.product_id      = product_slug.product_id
      LEFT JOIN product_categories  ON product_categories.product_id  = product_slug.product_id
      LEFT JOIN product_size        ON product_size.product_id        = product_slug.product_id
      LEFT JOIN product_gender      ON product_gender.product_id      = product_slug.product_id
      LEFT JOIN product_brand       ON product_brand.product_id       = product_slug.product_id
			LEFT JOIN products       			ON products.id       = product_slug.product_id
			LEFT JOIN product_price        ON product_price.product_id      = product_slug.product_id AND product_price.type='retail'
			WHERE  " . implode(" AND " , $where) . "



    ", $where_data);

		$count = $count[0]["cnt"];
		//$count = $Filter->count_products();



		return array("products" =>$products, "count" => $count);


	}




	function get_limit() {
		$limit 	= 10;
		$offset = 0;

		if (!empty($this->filter["offset"]))
			$offset = $this->filter["offset"];

		if (!empty($this->filter["limit"]))
			$limit = $this->filter["limit"];

		return " limit " . $offset . "," . $limit . " ";
	}



	public function get_fields(array $request) {
		$fields = array(
			'main_table.*',
			'pp.price',
			'pp.currency',
			'pp.type',
		);

		if (!empty($request['gender_id'])) {
			$fields[] = 'pg.gender_id';
		}
		if (!empty($request['category_id'])) {
			$fields[] = 'pc.category_id';
		}

		return $fields;
	}

	public function get_where($source, array $request) {
		$gender_m = new Gender();

		// Join
		$source->leftJoin('product_price pp', 'pp.product_id = main_table.product_id');
		if (!empty($request['gender_id'])) {
			$source->leftJoin('product_gender pg', 'pg.product_id = main_table.product_id');
		}
		if (!empty($request['category_id'])) {
			$source->leftJoin('product_categories pc', 'pc.product_id = main_table.product_id');
		}

		// Where
		$source->where(['main_table.is_visible' => '1']); // #FIX IT
		if (!empty($request['gender_id'])) {
			$gender_unisex_id = $gender_m->get_unisex_id();
			$gender_ids = $request['gender_id'];
			$gender_ids[] = $gender_unisex_id;

			$source->andWhere(['pg.gender_id' => $gender_ids]);
		}
		if (!empty($request['category_id'])) {
			$source->andWhere([ 'pc.category_id' => $request['category_id'] ]);
		}

		$source->limit($request['limit'])->offset($request['offset']);

		return $source;
	}

	/*
	Deprecated
	*/
	public function getItemsIds($categ_ids, $limit, $offset, $gender, $brandID) {

		if ($brandID == "all") {
			$data = (new Query())->select('*')->from('categories_brands');
			$data->andWhere(['in', 'gender', $gender ]);
			$q_brandID= $data->all();
			$brandID = array();

			foreach ($q_brandID as $brand)
				$brandID[] = $brand["brand_id"];

		}


		if (!empty($brandID)) {
			$data = (new Query())->select('t1.product_id')->from('product_brand t1');
		} else {
			$data = (new Query())->select('t1.product_id')->from('product_categories t1');
		}

		if (!empty($_REQUEST['min_price']))
			$min_price = $_REQUEST['min_price'];
		if (!empty($_REQUEST['max_price']))
			$max_price = $_REQUEST['max_price'];
		if (!empty($_REQUEST['color'])  && $_REQUEST['color'] != "all")
			$color = explode(";", $_REQUEST["color"]);
		if (!empty($_REQUEST['size'])  && $_REQUEST['size'] != "all")
			$size = explode(";", $_REQUEST["size"]);
		if (!empty($_REQUEST['sub_category']) && $_REQUEST['sub_category'] != 'all')
			$sub_category = explode(";", $_REQUEST["sub_category"]);
		if (!empty($_REQUEST['designers']) && $_REQUEST['designers'] != "all")
			$designers = explode(";", $_REQUEST["designers"]);
		if (!empty($_REQUEST['category_id']))
			$category_id = $_REQUEST['category_id'];






		//joins
		if(!empty($min_price) || !empty($max_price))
			$data->leftJoin('product_price pp', 't1.product_id = pp.product_id');
		if(!empty($gender))
			$data->leftJoin('product_gender pg', 't1.product_id = pg.product_id');
		if(!empty($color))
			$data->leftJoin('product_colors pcol', 't1.product_id = pcol.product_id');
		if(!empty($designers))
			$data->leftJoin('product_brand pbrand', 't1.product_id = pbrand.product_id');
		if(!empty($size))
			$data->leftJoin('product_size psize', 't1.product_id = psize.product_id');

		if (empty($brandID))
			$data->leftJoin('categories_hierarchy ch', 't1.categories_id = ch.category');
		else if (!empty($brandID)) {
			//$data->leftJoin('product_categories cp', 't1.product_id_id = cp.product_id_id');
			//$data->leftJoin('categories_hierarchy ch', 'cp.categories_id = ch.category');

		}

		if (!empty($brandID))
			$data->leftJoin('product_categories pc', 't1.product_id = pc.product_id');


		$data->leftJoin('product_visible pv', 't1.product_id = pv.product_id');

		//wheres
		if(!empty($min_price))
			$data->andWhere(['>=', 'pp.product_price', $min_price]);
		if(!empty($max_price))
			$data->andWhere(['<=', 'pp.product_price', $max_price]);
		if(!empty($gender))
			$data->andWhere(['in', 'pg.gender_id', $gender]);
		if(!empty($color))
			$data->andWhere(['in', 'pcol.color_id', $color ]);
		if(!empty($size))
			$data->andWhere(['in', 'psize.size_id', $size ]);

		if(!empty($designers))
			$data->andWhere(['in', 'pbrand.brand_id', $designers ]);

		if (!empty($brandID)) {
			$data->andWhere(['in', 'brand_id', $brandID ]);
			if (!empty($sub_category))
				$data = $data->andWhere(['in', 'categories_id', $sub_category]);
			elseif (!empty($category_id))
				$data = $data->andWhere(['in', 'categories_id', $category_id]);
		} else {
			if (!empty($sub_category)) {
				$full_sub = $this->get_relations_categories($sub_category);
				$data = $data->andWhere(['in', 'categories_id', $full_sub]);
			} else {
				$data = $data->andWhere(['in', 'categories_id', $categ_ids]);
			}
		}

		$data = $data->andWhere(['=', 'pv.visible', 1]);

		$data = $data->groupBy('t1.product_id_id');

		$count = $data->count();

		if(!empty($limit))
			$data = $data->limit($limit);
		if(!empty($offset))
			$data = $data->offset($offset);

		$data = $data->all();


		$ids = [];
		foreach ($data as $key => $value) {
			$ids[] = $value['product_id'];
		}

		return [$count, $ids];
	}

	public function getRelatedById($ids) {
		 $data = (new Query())->select('*')
						->from('relations_categories')
						->where(['IN', 'main_category', $ids]);
		$data = $data->all();
		$relatedIDs = [];
		foreach($data as $k => $v) {
			$relatedIDs[] = $v['related'];
		}
		return $relatedIDs;
	}


	public function getCategoriesIDs($gender, $categId) {
		$query = (new Query())->select("ch.*")->from('categories_hierarchy ch')
						->leftJoin('categories c', 'ch.category_id = c.id');


		if(!empty($categId)) {
			$query = $query->orWhere(['=', 'ch.category_id', $categId]);
			$query = $query->orWhere(['=', 'ch.parent_category', $categId]);
		}

		$data = $query->all();

		$ids = [];
		foreach ($data as $key => $value) {
			if($this->designers_cats === true) {
				$ids[] = $value['brand_id'];
			} else {
				$ids[] = $value['category_id'];
			}
		}

		$relatedIDs = $this->getRelatedById($ids);
		foreach ($relatedIDs as $value) {
			$ids[] = $value;
		}

		return $ids;

	}

	/*
	Deprecated
	*/
	public function getWhere() {
		$categories = new Categories();
		$gender_m = new Gender();

		$where = ['and' => [], 'or' => []];

		$gender_id = false;
		$categId = false;
		$brandID = false;
		$categ_ids = array();

		if (!empty($_REQUEST['gender']) && !empty($_REQUEST["category_id"])) {
			$categId  =  $_REQUEST["category_id"];
			$gender_data = $gender_m->get_by_string($_REQUEST['gender']);
			$gender_id = (isset($gender_data['id'])) ? $gender_data['id'] : 0;

			$categ_ids = $this->getCategoriesIDs($gender_id, $categId);
		} else if (!empty($_REQUEST["category_id"])) {
			$categ_ids = $this->getCategoriesIDs(false, false);
		} else if (empty($_REQUEST["brand_id"])) {
			$categ_ids = $this->getCategoriesIDs(false, false);
		}


		if (!empty($_REQUEST["brand_id"])) {
			$brandID = $_REQUEST["brand_id"];

			if (!empty($_REQUEST['gender'])) {
				$gender_id = array( $categories->gender[ucfirst($_REQUEST['gender'])] );
			} else {
				$gender_id = array(2, 3); //Women, Men
			}
			$gender_id[] = 1; // Unisex
		}


		$offset = false;
		if (!empty($_REQUEST['offset'])) {
			$offset = $_REQUEST['offset'];
		}
		$limit = false;
		if (!empty($_REQUEST['limit'])) {
			$limit = $_REQUEST['limit'];
		}


		list($count, $ids) = $this->getItemsIds($categ_ids, $limit, $offset, $gender_id, $brandID);

		$where['count'] = $count;
		$where['and'][] = ['IN', 'id', $ids];

		if(!empty($_REQUEST['search'])) {
			$where['or'][] = ['like', 'name',  $_REQUEST['search']];
			$where['or'][] = ['like', 'description_long',  $_REQUEST['search']];
			$where['or'][] = ['like', 'manufacturer_name',  $_REQUEST['search']];
			$where['or'][] = ['like', 'keywords',  $_REQUEST['search']];
			$where['or'][] = ['like', 'product_type',  $_REQUEST['search']];
		}

		return $where;
	}

	public function getProductColor($where = []) {
		$query = new Query();
		$query = $query->select('c.id, c.color');
		$query = $query->from('colors c');

		if(!empty($_REQUEST['gender']) && !empty($_REQUEST['slug'])) {
			$query->leftJoin('product_colors pc', 'pc.color_id=c.id');
			foreach ($where['and']  as $key => $value) {
				$query = $query->andWhere(['in', 'pc.product_id', $value[2]]);
			}
		}

		$colors = $query->orderBy(['color' => SORT_ASC])->all();
		return $colors;
	}

	public function get_product_price_for_filter($params = array()) {
		$Categories = new Categories();

		$query = (new Query())->select(['min(p.price_retail) min_price', 'max(p.price_retail) max_price'])->from('products p')
			->leftJoin('product_gender pg', 'pg.product_id = p.product_id');

		//Joins
		if (!empty($params['sub_category']) || !empty($params['category_id']))
			$query->leftJoin('product_categories pc', 'pc.product_id = pg.product_id');
		if (!empty($params['brand_id']))
			$query->leftJoin('product_brand pb', 'pb.product_id = pg.product_id');
		if (!empty($params['color']))
			$query->leftJoin('product_colors pcolor', 'pcolor.product_id = pg.product_id');
		if (!empty($params['size']))
			$query->leftJoin('product_size ps', 'ps.product_id = pg.product_id');

		//Where
		$params['gender_id'][] = 1;
		$query->where([ 'pg.gender_id' => $params['gender_id'] ]);
		if (!empty($params['sub_category'])) {



			$query->andWhere([ 'pc.category_id' => $params['sub_category'] ]);
		}
		elseif (!empty($params['category_id'])) {

			if (!is_array($params["category_id"])) {
				$childs = $Categories->get_childs($params["category_id"]);
				$params['category_id'] = array_merge(array($params['category_id']), $childs);
			}

			$query->andWhere([ 'pc.category_id' => $params['category_id'] ]);
		}
		if (!empty($params['brand_id']))
			$query->andWhere([ 'pb.brand_id' => $params['brand_id'] ]);
		if (!empty($params['color']))
			$query->andWhere([ 'pcolor.color_id' => $params['color'] ]);
		if (!empty($params['size']))
			$query->andWhere([ 'ps.size_id' => $params['size'] ]);

		$query = $query->all();

		if (!empty($query)) $query = current($query);

		return $query;
	}

	public function getRandomRelatedProduct() {
		$data = array();

		if (!empty($_REQUEST['path'])) {
			$brand = (new \yii\db\Query())->select(['id'])->from('brands')->where(['slug' => $_REQUEST['path']])->all();
		}
		if (!empty($brand[0]['id'])) {
			$data = (new \yii\db\Query())->select(['products.*', 'pg.gender_id'])->from('products')
				->leftJoin('product_brand pb', 'pb.product_id = products.id')
				->leftJoin('product_gender pg', 'pg.product_id = pb.product_id')
				->where(['pb.brand_id' => $brand[0]['id']])
				->andWhere(['pg.gender_id' => [3,2]])
				->orderBy(['RAND()' => SORT_DESC])
				->limit(4)
				->all();
		}

		return $data;
	}

	/*
	* @params['product_id'] string
	* @params['gender_id'] int
	*
	* @return array $data Random related produc
	*/
	public function get_rand_related_product(array $params = array()) {
		$Sql = new Sql();
		$Slug = new Slug();
		$Sql->enable_log = false;


		$categories_m = new Categories();

		if (empty($params))
			return false;



		if (empty($params['product_id'])) {
			$params = json_decode($params["data"],1);
			foreach ($params as $cat_name) {
				$slug = $Slug->slugify($cat_name);
				$slugs[$slug] = $slug;
			}


			$brandQ = $Sql->select("SELECT id FROM brands WHERE slug IN (in:slug)", array("in:slug" => $slugs));

			$categories = [];
			//$categories = $Sql->select("SELECT id FROM categories WHERE slug IN (in:slug)", array("in:slug" => $slugs));
			if (empty($categories) AND empty($brandQ)) return false;

			$cats =[];
			foreach ($categories as $category)
				$cats[] = $category["id"];


			$brand =[];
			foreach ($brandQ as $b) {

				$brand[] = $b["id"];
			}

			$category_id = [];
			if (!empty($cats))
				$category_id = $categories_m->get_related_categories2($cats);
		}
		else
			$category_id = $categories_m->get_category_id_by_product_id($params['product_id']);


		$where = ' WHERE ps.is_visible=1 AND (pc.category_id in (in:c) OR pb.brand_id in (in:b)) ';
		$brand = false;
		$cats    = false;

		if (empty($cats))
			$where = ' WHERE ps.is_visible=1 AND (pb.brand_id in (in:b)) ';
		if (empty($brand))
			$where = ' WHERE ps.is_visible=1 AND (pc.category_id in (in:c)) ';

		if (!empty($params['gender_id'])) {

			 $rand = rand(0, 17000);
			 $i = 0;
			 foreach ($category_id as $k){
				 if ($i == 3) break;

				 $category_id2[] = $k;
				 $i++;
				 break;
			 }

			 $category_id = $category_id2;
			 $query = $Sql->select(
			 "SELECT ps.slug, ps.product_id, p.name,  p.price_retail, p.URL_productImage as image FROM product_slug ps
			 LEFT JOIN product_categories pc ON pc.product_id = ps.product_id
			 LEFT JOIN product_gender pg ON pg.product_id = ps.product_id
			 LEFT JOIN product_brand pb ON pb.product_id = ps.product_id
			 LEFT JOIN products p ON p.id = ps.product_id
				" . $where . " AND pg.gender_id=:g
				limit ".$rand.",8
				", array("in:c" => $category_id, "g"=>$params['gender_id']));

			 if (empty($query)) {
				 $query = $Sql->select(
				 "SELECT ps.slug, ps.product_id, p.name,  p.price_retail, p.URL_productImage as image FROM product_slug ps
				 LEFT JOIN product_categories pc ON pc.product_id = ps.product_id
				 LEFT JOIN product_gender pg ON pg.product_id = ps.product_id
				 LEFT JOIN product_brand pb ON pb.product_id = ps.product_id
				 LEFT JOIN products p ON p.id = ps.product_id
					" . $where . " AND pg.gender_id IN (in:g)
					limit 0,8
					", array("in:c" => $category_id, "in:g"=>array($params['gender_id'], 3)));
			 }
			}
		 else {


			 $query = $Sql->select(
			 	"SELECT ps.slug, ps.product_id, p.name,  p.price_retail, p.URL_productImage as image FROM product_slug ps
				LEFT JOIN product_categories pc ON pc.product_id = ps.product_id
				LEFT JOIN product_gender pg ON pg.product_id = ps.product_id
				LEFT JOIN product_brand pb ON pb.product_id = ps.product_id
				LEFT JOIN products p ON p.id = ps.product_id
				" . $where . "
				ORDER BY RAND()
				limit 0,8

				", array("in:c" => $category_id, "in:b" => $brand)
		 	);

		}



		return $query;
	}

	public function get_relations_categories($category) {
		$query = (new Query())->select('*')->from('relations_categories rc')->where(['rc.main_category' => $category])->all();

		$data = $category;
		foreach ($query as $q) {
			$data[] = $q['related'];
		}

		return $data;
	}

	public function set_product($product) {
		$this->product = $product;
	}

	public function get_color() {
		$p = $this->product;

		if (empty($p["Color"]))
			return false;


		$explode = explode("/", $p["Color"]);
		return $explode;
	}

	public function get_id() {
		$p = $this->product;

		if (empty($p["id"]))
			return false;

		return $p["id"];
	}

	public function get_parent_id() {
		$p = $this->product;

		if (empty($p["parent_id"]))
			return false;

		return $p["parent_id"];
	}

	public function get_skunumber() {
		$p = $this->product;

		if (empty($p["product_id"]))
			return false;

		return $p["product_id"];
	}

	public function get_brandName() {
		$p = $this->product;

		if (empty($p["brand"]))
			if (!empty($p["manufacturer_name"]))
				return $p["manufacturer_name"];
			else
				return false;

		return $p["brand"];
	}

	public function get_size() {
		$p = $this->product;

		if (empty($p["Size"]))
			return false;

		$s1 = explode(",", $p["Size"]);

		if (empty($s1[1]))
			$s1 = explode("/", $p["Size"]);



		return $s1;
	}

	public function get_gender() {
		$p = $this->product;

		if (empty($p["Gender"]))
			return false;

		// filters
		$men 		= array("men", "male", "man");
		$women 	= array( "woman", "women", "female");
		$unisex = array("unisex");

		if (in_array(strtolower($p["Gender"]), $men))
			$p["Gender"] = "Men";
		else
		if (in_array(strtolower($p["Gender"]), $women))
			$p["Gender"] = "Women";
		else
		if (in_array(strtolower($p["Gender"]), $unisex))
			$p["Gender"] = "Unisex";


		return $p["Gender"];
	}

	public function get_retail_price() {
		$p = $this->product;

		if (empty($p["price_retail"]))
			return false;

		return $p["price_retail"];
	}

	public function get_sale_price() {
		$p = $this->product;

		if (empty($p["price_sale"]))
			return false;

		return $p["price_sale"];
	}

	public function get_cat_secondary() {
		$p = $this->product;

		if (empty($p["category_secondary"]))
			return false;

		return $this->category_string_tree($this->filterCategories($p["category_secondary"]));
	}

	public function get_cat_primary() {
		$p = $this->product;

		if (empty($p["category_primary"]))
			return false;

		return $this->category_string_tree($this->filterCategories($p["category_primary"]));
	}

	public function get_product_type() {
		$p = $this->product;

		if (empty($p["Product_Type"]))
			return false;

		return $this->category_string_tree($this->filterCategories($p["Product_Type"]));
	}
	public function category_string_tree($data) {
			$exploded = explode("~~", $data);

			if (empty($exploded[1]))
				$exploded = explode(">", $data);

			if (empty($exploded[1]))
				return $data;
			else
				return $exploded;

	}

	public function filterCategories($v) {

		$array = array("male", "female", "men", "women");

		if (in_array(strtolower($v), $array))
			return false;

		return $v;
	}

	public function get_name() {
		$p = $this->product;

		if (empty($p["name"]))
			return false;

		return $p["name"];
	}

	public function get_image() {
		$p = $this->product;

		if (empty($p["URL_productImage"]))
			return false;

		return $p["URL_productImage"];
	}

	public function get_product_by_slug($slug = '') {

		$Sql = new Sql();
		$Sql->enable_log = false;

		$colors_m = new Colors();
		$size_m = new Size();

		$q = $Sql->select("SELECT *, URL_productImage as image, products.sku_number as sku_number, brands.slug as brand_slug, product_slug.slug as product_slug   FROM product_slug
			LEFT JOIN products ON products.id = product_slug.product_id
			LEFT JOIN product_gender ON product_gender.product_id = product_slug.product_id
			LEFT JOIN gender ON gender.id = product_gender.gender_id
			LEFT JOIN product_brand ON product_brand.product_id = product_slug.product_id
			LEFT JOIN brands ON brands.id = product_brand.brand_id
			WHERE product_slug.slug=:slug AND product_slug.is_visible = 1
			limit 1
		", array("slug" => $slug));

		if (empty($q))
			return false;

		// Get colors
		$colors_q = $colors_m->get_colors_by_product_id($q[0]["product_id"]);
		$q[0]["colors"] = $colors_q;

		$size_q = $size_m->get_size_by_product_id($q[0]["product_id"]);
		$q[0]["size"] = $size_q;

		return $q;
	}

	public function get_product_color_size_by_product_id($product_id = '') {
		$colors_m = new Colors();
		$size_m = new Size();
		$data = array();

		$data['colors'] = $colors_m->get_colors_by_product_id($product_id);
		$data['size'] = $size_m->get_size_by_product_id($product_id);

		return $data;
	}


	public function get_related_categories($cat) {
		$Sql = new Sql();
		if (is_string($cat))
			$cat = array($cat);
		$allCats = $Sql->sl("relations_categories", "main_category IN (in:c)", array("in:c" => $cat));

		if (!empty($allCats))
			foreach ($allCats as $r) {
				$newCats[$r["main_category"]] = $r["main_category"];
				$newCats[$r["related"]] = $r["related"];
			}

		foreach ($cat as $r) {
			$newCats[$r] = $r;
		}

		return $newCats;
	}
}
