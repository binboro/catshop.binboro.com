<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

/**
 * To manipulate with designer_url table
 */
class BrandUrl extends Model
{
    private $fields = array(
                'brand_id',
                'category_id',
                'slug',
            );

    public function insert_batch(array $data) {
        return Yii::$app->db->createCommand()->batchInsert('brand_url', $this->fields, $data)->execute();
    }

    public function get_by_slug($slug = '') {
        return (new Query())->select('*')->from('brand_url')->where(['slug' => $slug])->all();
    }
}
