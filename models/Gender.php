<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Gender extends Model
{
    public function get_by_string($string = '') {
        $string = strtolower($string);

        $query = (new Query())->select('*')->from('gender')->where(['gender' => $string])->all();

        $data = array();
        if (!empty($query)) $data = current($query);

        return $data;
    }

    public function get_unisex_id() {
        $query = (new Query())->select('id')->from('gender')->where(['gender' => 'unisex'])->all();

        $data = array();
        if (!empty($query)) $data = current($query);
        $data = $data['id'];

        return $data;
    }

    public function get_gender_category_relations() {
        $query = (new Query())->select('*')->from('categories c')
            ->innerJoin('relations_gender_category rgc', 'rgc.gender_category_id = c.id')
            ->all();

        $data = array();
        foreach ($query as $q) {
            $data[ $q['slug'] ] = $q;
        }

        return $data;
    }

    public function get_gender_by_id($id) {
        $query = (new Query())->select('*')->from('gender g')->where(['g.id' => $id])->all();

        if (!empty($query)) $query = current($query);

        return $query;
    }

    public function get_women_men_data() {
        $slugs = array('women', 'men');

        $query = (new Query())->select('*')->from('gender g')
            ->where(['g.slugified_id' => $slugs])
            ->indexBy('slugified_id')
            ->all();

        return $query;
    }
}
