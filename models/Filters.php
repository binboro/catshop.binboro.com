<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\Designers;
use app\models\Colors;
use app\models\Size;
use app\models\Categories;
use app\models\Products;
use app\components\helpers\DataTypeHelper;
use app\components\Log;
use app\components\Sql;

class Filters extends Model
{


    public $params;
    public $filter_data;
    public $sql_params = array("color_id", "brand_id", "category_id", "gender_id", "size_id");

    public function get_param($index) {
        if (empty($this->params[$index]))
          return false;

        return $this->params[$index];
    }

    public function get_all_sql_params() {

      $params = array();

      foreach ($this->sql_params as $field)
        if (!empty($this->params[$field]))
          $params[$field] = $this->params[$field];
        else
          $params[$field] = 0;

      if (empty($params))
        return false;

      return $params;
    }

    public function set_param($index, $value) {
      $this->params[$index] = $value;
    }

    public function get_all_filters_data() {
      $Lg = new Log(false);
      $Lg->show("Start get_all_filters_data");
      $this->get_filter_data();

      $data = array(
        "designers_f"    => $this->get_designers_for_filter(),
        "categories_f"   => $this->get_categories_for_filter(),
        "colors_f"       => $this->get_colors_for_filter(),
        "size_f"         => $this->get_sizes_for_filter(),
        "price_f" => array()

      );
      $Lg->show("End get_all_filters_data");

      return $data;
    }



    public function get_filter_data() {
      return false;
      if (!empty($this->filter_data))
        return $this->filter_data;

      $Lg = new Log(true);
      $Sql = new Sql();
      $Sql->enable_log = false;
      $params = $this->get_all_sql_params();

      foreach ($params as $k => $v) {
        if ($v == 0) {
          $where[] = ' AND '  . $k . '=' . $v . ' ';
        }
        else {
          $where[] = ' AND '  . $k . ' IN (in:' . $k . ') ';
          $where_values["in:" . $k][] = $v;
        }
      }

      $this->filter_data = $Sql->select("SELECT * FROM filter_index WHERE 1 " . implode(" ", $where) . " ORDER BY cnt DESC ", $where_values );


    }


    public function get_designers_for_filter() {


      $Lg = new Log(false);
      $Sql = new Sql();
      $Sql->enable_log = false;

      $Products = new Products();
      $params = $this->get_all_sql_params();

      if (!empty($params["brand_id"])) {
        return array();
      }

      foreach ($params as $k => $v) {
        if ($k == "brand_id")
          continue;



        if (empty($v)) {
          $where[] = ' AND filter_index.'  . $k . '=0 ';
        }
        else if ($k == "category_id") {
            $v = $Products->get_related_categories(array($v));

            $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
            $where_values["in:" . $k] = $v;
        }
        else if ($k == "gender_id") {
            $v = array($v, 3);
            $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
            $where_values["in:" . $k] = $v;
        }
        else {
          $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
          $where_values["in:" . $k][] = $v;
        }

      }

      $q = $Sql->select("
      SELECT * FROM filter_index
      LEFt JOIN brands ON brands.id=filter_index.brand_id
      WHERE brands.is_visible = 1 " . implode(" ", $where) . " AND filter_index.brand_id!=0 GROUP BY filter_index.brand_id ORDER BY brands.brand ASC  LIMIT 1000 ", $where_values );
      $Lg->show("get_designers_for_filter foreach S");


      return $q;
    }

    public function get_categories_for_filter() {
      $Lg = new Log(false);
      $Sql = new Sql();
      $Sql->enable_log = false;
      $params = $this->get_all_sql_params();


      if (!empty($params["category_id"])) {
        // get child categories
        if (is_array($params["category_id"]))
          $cats = $params["category_id"];
        else
          $cats = array($params["category_id"]);

        $q = $Sql->select("SELECT * FROM categories_hierarchy WHERE parent_category IN (in:c)", array("in:c" => $cats));

        if (empty($q))
          return array();

          foreach ($q as $r)
            $child_cats[] = $r["category_id"];

          $where[] = ' AND categories.id IN (in:cats) ';
          $where_values["in:cats"] = $child_cats;
      }

      foreach ($params as $k => $v) {
        if ($k == "category_id")
          continue;

        if (empty($v)) {
          $where[] = ' AND filter_index.'  . $k . '=0 ';
        }
        else {
          $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
          $where_values["in:" . $k][] = $v;
        }
      }


      $q = $Sql->select("
      SELECT * FROM filter_index
      LEFT JOIN categories ON categories.id=filter_index.category_id
      WHERE 1 " . implode(" ", $where) . " AND filter_index.category_id!=0 GROUP BY filter_index.category_id ORDER BY categories.category ASC LIMIT 1000 ", $where_values );
      $Lg->show("get_designers_for_filter foreach S");

      return $q;
    }

    public function get_sizes_for_filter() {
      $Lg = new Log(false);
      $Sql = new Sql();
      $Sql->enable_log = false;
      $params = $this->get_all_sql_params();


      foreach ($params as $k => $v) {
        if ($k == "size_id")
          continue;

        if (empty($v)) {
          $where[] = ' AND filter_index.'  . $k . '=0 ';
        }
        else {
          $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
          $where_values["in:" . $k][] = $v;
        }
      }

      $q = $Sql->select("
      SELECT * FROM filter_index
      LEFT JOIN size ON size.id=filter_index.size_id
      WHERE 1 " . implode(" ", $where) . " AND filter_index.size_id!=0 GROUP BY size.size  ORDER BY size.size ASC LIMIT 1000 ", $where_values );
      $Lg->show("get_designers_for_filter foreach S");

      return $q;
    }

    public function get_colors_for_filter() {
      $Lg = new Log(false);
      $Sql = new Sql();
      $Sql->enable_log = false;
      $params = $this->get_all_sql_params();


      foreach ($params as $k => $v) {
        if ($k == "color_id")
          continue;

        if (empty($v)) {
          $where[] = ' AND filter_index.'  . $k . '=0 ';
        }
        else {
          $where[] = ' AND filter_index.'  . $k . ' IN (in:' . $k . ') ';
          $where_values["in:" . $k][] = $v;
        }
      }

      $q = $Sql->select("
      SELECT * FROM filter_index
      LEFT JOIN colors ON colors.id=filter_index.color_id
      WHERE 1 " . implode(" ", $where) . " AND filter_index.color_id!=0 GROUP BY colors.color ORDER BY colors.color ASC LIMIT 1000 ", $where_values );
      $Lg->show("get_designers_for_filter foreach S");

      return $q;
    }



  public function count_products() {
    $Sql = new Sql();
    $Sql->enable_log = false;

    $id = $this->make_id( $this->get_all_sql_params() );

    if ($id == false ) {

      $where_data  = array();
      $params = $this->get_all_sql_params();

      $where = '';
      foreach ($params as $k => $v) {
        if (empty($v)) $v = 0;

        if (!is_array($v)) {
          $where .= " AND " . $k . " =".$v." ";
        }
        else {
        $where .= " AND " . $k . " IN (in:".$k.") ";
        $where_data["in:" . $k] = $v;
        }
      }

      $count = $Sql->select("SELECT  SUM(cnt) as cnt FROM filter_index WHERE  1 " . $where." ", $where_data);
      return  $count[0]["cnt"];
    }

    $count = $Sql->select("SELECT cnt as cnt FROM filter_index WHERE  match_id='".$id."' ");

    if (empty($count))
      return 0;

    return  $count[0]["cnt"];


  }


  public function make_id($filter_where_tmp) {
    foreach ($filter_where_tmp as $v)
      if (is_array($v))
        return false;

    return md5($filter_where_tmp["brand_id"] . "_" . $filter_where_tmp["color_id"] . "_" . $filter_where_tmp["size_id"] . "_" . $filter_where_tmp["category_id"]);
  }


}
