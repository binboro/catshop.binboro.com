<?php

namespace app\models;
use Yii;
use yii\db\Query;
use app\models\Gender;
use app\components\Sql;

class Categories extends \yii\db\ActiveRecord {
    const DESIGNER_ID = 7259;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }


    public function get_categories_by_product($product_id) {

      $Sql = new Sql();
      $categories = $Sql->sl("product_categories", "product_id=:id", array("id" => $product_id));

      foreach ($categories as $cat)
        $ids[] = $cat["category_id"];

      unset($categories);


      $categories = $this->get_related_categories2($ids);

      return $categories;
    }


    public function get_related_categories2($cat) {
  		$Sql = new Sql();
  		$Sql->enable_log = false;

      if (is_string($cat))
        $cat = array($cat);


  		$allCats = $Sql->sl("relations_categories", "main_category IN (in:c)", array("in:c" => $cat));
  		$allCats2 = $Sql->sl("categories_hierarchy", "parent_category IN (in:c)", array("in:c" => $cat));

      if (!empty($allCats2))
    		foreach ($allCats2 as $r2) {
    			$newCats[$r2["parent_category"]] = $r2["parent_category"];
    			$newCats[$r2["category_id"]] = $r2["category_id"];
    		}


  		if (!empty($allCats))
  			foreach ($allCats as $r) {
  				$newCats[$r["main_category"]] = $r["main_category"];
  				$newCats[$r["related"]] = $r["related"];

  			}

  		foreach ($cat as $r) {
  			$newCats[$r] = $r;
  		}

      $allCats2 = $Sql->sl("categories_hierarchy", "parent_category IN (in:c)", array("in:c" => $newCats));

      foreach ($allCats2 as $r2) {
        $newCats[$r2["parent_category"]] = $r2["parent_category"];
        $newCats[$r2["category_id"]] = $r2["category_id"];
      }

  		return $newCats;
  	}
    /*
      Looks like there are tons if trash below

    */


    public $categories_hierarhy = [
      'Clothing' => [
        'SWEATSHIRTS', 'SKIRT', 'SHORTS', 'BLAZERS & SPORTCOATS', 'DRESS SHIRTS', 'JACKET', 'BEDSKIRTS', 'QUILTS & COVERLETS',
        'SPORT SHIRTS', 'TOP', 'TEES', 'DRESS', 'PANTS', 'ACTIVEWEAR', 'JUMPSUIT ROMPERS', 'JEANS', 'SWIMSUITS', 'SWEATER', 'POLOS',
        'ONE PIECES', 'SUITS & TUXEDOS', 'COAT', 'SOCKS', 'UNDERWEAR', 'BLAZERS', 'BRAS', 'SHAPEWEAR', 'PANTIES',
        'LINGERIE', 'TIGHTS & HOSIERY',
      ],
      'Jewelry' => [
        'RING', 'NECKLACE', 'EARRINGS', 'BRACELETS',
      ],
      'Bags' => [
        'HANDBAG', 'LUGGAGE',
      ],
      'Accessories' => [
        'SUNGLASSES', 'HATS', 'SCARVES & WRAPS', 'BELTS', 'GLOVES', 'TECH ACCESSORIES & CASES', 'SCARVES',
      ],
      'Shoes' => [
        'SNEAKERS', 'SANDALS', 'SHOE', 'DRESS SHOES', 'BOOTS', 'SLIPPERS', 'RAIN & COLD WEATHER'
      ]

    ];

    public $gender = [
      'undefined' => 0,
      'Unisex' => 1,
      'Women' => 2,
      'Men' => 3,
      'Boys' => 4,
    ];


    public function findInArray($category, $withKey) {
      if($withKey) {
        if(!empty($this->categories_hierarhy[$category])){
          return $category;
        }
      }

      foreach ($this->categories_hierarhy as $key => $value) {
        if(in_array($category, $value))
          return $key;
      }
      return false;
    }

    public function insertHierarhy($cat_hierarhy) {
      $categories = $this->getAllCategories();
      $relations = $this->getAllRelations();

      $toInsert = [];
      foreach ($cat_hierarhy as $key => $value) {
        if(empty($categories[$value['category']])) {
          continue;
        }

        $categoryID = $categories[$value['category']];

        if(!empty($relations[$categoryID . '_' . $value['mainID'] . '_' . $value['gender']])) {
          continue;
        }

        $toInsert[] = [$categoryID, $value['mainID'], $value['gender'], 1];
        $relations[$categoryID . '_' . $value['mainID'] . '_' . $value['gender']] = [$categoryID, $value['mainID'], $value['gender'], 1];
      }

      Yii::$app->db->createCommand()->batchInsert('categories_hierarchy',
            ['category', 'parent_category', 'gender', 'order'], $toInsert)->execute();
    }



    public function getAllRelations() {
      $sql = "SELECT * FROM `categories_hierarchy`";
      $categories_data = Yii::$app->db->createCommand($sql)->queryAll();
      if (empty($categories_data)) {
        return [];
      }

      $categories = [];
      foreach ($categories_data as $key => $value) {
        $categories[$value['category'] . '_' . $value['parent_category'] . '_' . $value['gender']] = $value;
      }
      return $categories;
    }


    public function get_cats_children($parent, $q, $gender_name) {
      $cats = array();
      foreach ($q as $r) {

        if (!empty($exists[$r["id"]]))
          continue;

        $gn = $this->get_gender_name($r["gender"]);

        if ($gn != $gender_name)
          continue;

        if ($r["parent_category"] != $parent["id"])
          continue;

        $cats[] = $r;

        $exists[$r["id"]] = true;

      }

      return $cats;
    }

    public function get_gender_name($g) {
      if ($g > 3) $g = 3;

      if ($g == 2 OR $g == 3)
          $gender_name = "men";
      else if ($g == 1 OR $g == 3)
          $gender_name = "women";

      return $gender_name;
    }

    public function get_designers_children($parent, $q, $gender_id) {
      $Sql = new Sql();
      $Sql->enable_log = false;

      if ($gender_id > 2) {
        $q = $Sql->select("SELECT b.*, ch.gender, b.brand as category
        FROM brands b
        LEFT JOIN categories_brands as ch ON b.id = ch.brand_id
        WHERE ch.brand_id IS NOT NULL AND (ch.gender=1 OR ch.gender=2)
         AND ch.gender IS NOT NULL
        GROUP BY brand_id
        ORDER BY b.brand

        ");
      }
      else {
      $q = $Sql->select("SELECT b.*, ch.gender, b.brand as category
      FROM brands b
      LEFT JOIN categories_brands as ch ON b.id = ch.brand_id
      WHERE ch.brand_id IS NOT NULL AND ch.gender=".$gender_id."
       AND ch.gender IS NOT NULL
      GROUP BY brand_id
      ORDER BY b.brand

      ");
      }


      return $q;
    }
    public function getAll(){

      $Sql = new Sql();
      $Sql->enable_log = false;
      $cats = array();

      // Categories
      $q = $Sql->select("
            SELECT ch.*, c.category as category, cg.gender_id as gender, c.slug as slug, c.id as id
            FROM categories_hierarchy as ch
            LEFT JOIN categories as c ON c.id=ch.category_id
            LEFT JOIN categories_gender as cg ON cg.category_id=c.id
            WHERE cg.gender_id IS NOT NULL
            ");

      $exists = array();

      $cats = array();

      foreach ($q as $r) {


          if ($r["parent_category"] != 0)
            continue;


          $gender_name = $this->get_gender_name($r["gender"]);

          $md5 = md5($gender_name ."_" . $r["parent_category"] . "_" . $r["id"]);
          if (!empty($exists[$md5]))
            continue;


          if ($r["id"] == 7259)
            $children = $this->get_designers_children($r, $q, $r["gender"]);
          else
            $children = $this->get_cats_children($r, $q, $gender_name);

          if (!empty($children))
            $r["children"] = $children;

          $cats[ $gender_name ] [] = $r;


          $exists[$md5] = true;
      }



      return $cats;

    }

    public function getDesignersAsCategories($parent_id, $gender) {
      echo $parent_id . " " . $gender;
    }


    public function getAllCategoriesIds() {
      $data = (new Query())->select('*')
                           ->from('categories')
                           ->where(['>', 'id', 2])
                           ->andWhere(['!=', 'category_name', '0'])
                           ->orderBy('category_name')->all();
      $categories = [];
      foreach ($data as $key => $value) {
        $categories[$value['id']] = $value['category_name'];
      }

      return $categories;
    }

    public function getCategoriesWithHierarhy()
    {
      $data = (new Query())->select('c.*, ch.parent_category, ch.gender, ch.order, ch.show')
                           ->from('categories c')
                           ->leftJoin('categories_hierarchy as ch', 'c.id = ch.category')
                           ->where(['>', 'c.id', 2])
                           ->andWhere(['!=', 'c.category_name', '0'])
                           ->orderBy('c.category_name')
                           ->all();
      $categories = [];
      foreach ($data as $key => $value) {
        if(empty($categories[$value['parent_category']])) {
          $categories[$value['parent_category']] = [];
        }

        if(empty($value['parent_category'])) {
          $value['parent_category'] = 0;
        }

        $categories[$value['parent_category']][] = $value;
      }
      return $categories;
    }


    public function getFromHierarhyTable() {
      $data = (new Query())->select('*')->from('categories_hierarchy')->all();
      $categories = [];
      foreach ($data as $key => $value) {
        $categories[$value['category']] = $value;
      }

      return $categories;
    }


    public function saveChanges($data) {

      $categoryRelated = $this->getRelated();
      $cathegoryHierarhy = $this->getFromHierarhyTable();
      $toInsert = [];
      foreach ($data as $key => $value) {
        $related = [];
        if(!empty($value['related']))
          $related = $value['related'];

        $this->changeRelated($key, $categoryRelated, $related);

        if(!empty($cathegoryHierarhy[$key])) {
          $parentCompare = ($cathegoryHierarhy[$key]['parent_category'] == $value['parent']);
          $genderCompare = ($cathegoryHierarhy[$key]['gender'] == $value['gender']);
          $showCompare = ($cathegoryHierarhy[$key]['show'] == $value['show']);
          if($parentCompare && $genderCompare && $showCompare) {
              continue;
          } else {
           Yii::$app->db->createCommand()->update('categories_hierarchy',
              [
                'parent_category' => $value['parent'],
                'gender' => $value['gender'],
                'show' => $value['show'],
              ],
              'category = :category',
              ['category' => $key]
            )->execute();
          }

        } else {
          $toInset[]  = [$key, $value['parent'], $value['gender'], 1, $value['show']];
        }

      }

      if(count($toInsert) > 0) {
        Yii::$app->db->createCommand()->batchInsert('products',
            ['category', 'parent_category', 'gender', 'order', 'show'],
             $toInset)->execute();
      }

      return;

    }

    public function changeRelated($id, $relatedAll, $current) {

      if(empty($current) && !empty($relatedAll[$id])){
         Yii::$app->db->createCommand()->delete('relations_categories', "main_category=:main_category", ['main_category' => $id])->execute();
      }

      if(!empty($current) && !empty($relatedAll[$id])){
          $intersect = array_intersect($current, $relatedAll[$id]);
          $relateInsert = array_diff($current, $intersect);
          $relateDelete = array_diff($current, $relatedAll[$id]);

          if(count($relateDelete)) {
            Yii::$app->db->createCommand("DELETE FROM `relations_categories`
              WHERE main_category='" . $id . "' AND related IN ('" . implode("','", $relateInsert) . "')")->execute();
          }

          if(count($relateInsert)) {
            $toInsert = [];
            foreach ($relateInsert as $value) {
              $toInsert[] = [$id, $value];
            }
            Yii::$app->db->createCommand()->batchInsert('relations_categories', ['main_category', 'related'], $toInset)->execute();
          }
      }

      if(!empty($current) && empty($relatedAll[$id])) {
        $toInsert = [];
        foreach ($current as $value) {
          $toInsert[] = [$id, $value];
        }
        Yii::$app->db->createCommand()->batchInsert('relations_categories', ['main_category', 'related'], $toInsert)->execute();
      }

      return;
    }


    public function getRelated() {
      $data = (new Query())->select('*')->from('relations_categories')->all();
      $categories = [];
      foreach ($data as $key => $value) {
        if(empty($categories[$value['main_category']]))
          $categories[$value['main_category']] = [];

        $categories[$value['main_category']][] = $value['related'];
      }

      return $categories;
    }

    public function getBySlug($slug) {
      return (new Query())->select('*')->from('categories')->where(['slug' => $slug])->one();
    }

    public function getChildren($parent, $gender) {

      if ($parent == 4)
        return $this->get_brands_for_category_menu($gender);

      $categories_data = (new Query())->select('c.*, ch.parent_category, cg.gender_id as gender, ch.order, ch.show, count(pc.product_id) as cnt')
               ->from('categories c')
               ->leftJoin('categories_hierarchy ch', 'c.id = ch.category_id')
               ->leftJoin('categories_gender cg', 'cg.category_id = ch.category_id')
               ->leftJoin('product_categories pc', 'pc.category_id = c.id')
               ->leftJoin('product_gender pg', 'pg.product_id = pc.product_id')
               ->where(['=', 'pg.gender_id', $gender])
               ->andWhere(['=', 'ch.parent_category', $parent])
               ->groupBy('c.category')
               ->having(['>', 'count(pc.product_id)', 0])
               ->orderBy('c.category')
               ->all();

      if (empty($categories_data)) {
        return [];
      }

      $relations_categories = $this->get_formated_relations_categories();

      $data = [];
      foreach ($categories_data as $key => $value) {
        if (isset($relations_categories[ $value['id'] ])) continue;
        $data[] = $value;
      }

      return $data;
    }

    public function get_brands_for_category_menu($gender) {

      $q = (new Query())->select('*, brand as category_name')->from("categories_brands as t1")
            ->leftJoin('brands as t2', 't2.id = t1.brand_id')
            ->where(["=", "gender", $gender])
            ->all();

      if (empty($q))
        return [];

      $data = [];
      foreach ($q as $key => $value)
        $data[] = $value;

      return $data;
    }


    public function getMainCategoriesIDs() {
      $categories_data = (new Query())->select('c.*, ch.parent_category, cg.gender_id as gender, ch.order')
                           ->from('categories c')
                           ->leftJoin('categories_hierarchy as ch', 'c.id = ch.category_id')
                           ->leftJoin('categories_gender as cg', 'c.id = cg.category_id')
                           ->andWhere(['=', 'ch.parent_category', '0'])
                           ->orderBy('ch.order')->all();

      if (empty($categories_data)) {
        return [];
      }

      $categoriesID = [];
      foreach ($categories_data as $key => $value) {
        $categoriesID[$value['category']] = $value;
      }

      return $categoriesID;
    }

    public function getAllCategories() {
      $keys = array_keys($this->categories_hierarhy);
      $sql = "SELECT * FROM `categories`  WHERE `category_name` NOT IN ('" . implode("', '", $keys) . "')";
      $categories_data = Yii::$app->db->createCommand($sql)->queryAll();
      if (empty($categories_data)) {
        return [];
      }

      $categoriesID = [];
      foreach ($categories_data as $key => $value) {
        $categoriesID[$value['category_name']] = $value['id'];
      }

      return $categoriesID;

    }

      /**
      * @inheritdoc
      */
     public function rules()
     {
         return [
             [['category_name', 'slug'], 'required'],
             [['category_name', 'slug'], 'string', 'max' => 255],
         ];
     }

     /**
      * @inheritdoc
      */
     public function attributeLabels()
     {
         return [
             'id' => 'ID',
             'category_name' => 'Category Name',
             'slug' => 'Slug',
         ];
     }

    /**
	* Get categories for filters if we follow to the designers page
    *
    * @param array $params Data for conditions
    * @param int $gender_id
    * @param int $category_id
    * @param int $brand_id
    * @param int $sub_category
    * @param int $color
    * @param int $size
    * @param int $min_price
    * @param int $max_price
	*
	* @return array Category data for filter
	*/
    public function get_categories_for_filters($params = array()) {
        if (!empty($params['designers_flag']) or !empty($params['is_designers'])) {
            $gender = $params['gender_id'];
    		$category_id = $params['category_id'];

            $data = $this->category_for_filter_by_flag($gender, $category_id);
        } else {
            $data = $this->category_for_filter_by_hierarchy($params);
        }

		return $data;
    }

    public function category_for_filter_by_hierarchy($params) {


        $format_cats = $this->get_formated_category_for_filter($params);

        $relations_cnt = $this->get_relations_prod_count_for_filter($format_cats);


        foreach ($format_cats as $k => $fc) {
            if (isset($relations_cnt['relations'][ $fc['id'] ]) && !empty($format_cats[ $relations_cnt['relations'][ $fc['id'] ] ])) {
                unset($format_cats[$k]);
            }
            if (isset($relations_cnt['count'][ $fc['id'] ]) AND !empty($format_cats[$k]['cnt'])) {
                $format_cats[$k]['cnt'] += $relations_cnt['count'][ $fc['id'] ];
            }
        }

        return $format_cats;
    }

    public function get_formated_category_for_filter($params) {
        $query = (new Query())->select(['c.id', 'c.category as category_name', 'c.slug', 'count(pc.product_id) as cnt', 'ch.parent_category'])->from('categories c')
            ->leftJoin('categories_hierarchy ch', 'ch.category_id = c.id')
            ->leftJoin('product_categories pc', 'pc.category_id = ch.category_id')
            ->leftJoin('product_slug pv', 'pv.product_id = pc.product_id')
            ->leftJoin('product_gender pg', 'pg.product_id = pc.product_id');

        //Joins
        if (!empty($params['brand_id']))
            $query->leftJoin('product_brand pb', 'pb.product_id = pc.product_id');
        if (!empty($params['size']))
            $query->leftJoin('product_size ps', 'ps.product_id = pc.product_id');
        if (!empty($params['color']))
            $query->leftJoin('product_colors pcolor', 'pcolor.product_id = pc.product_id');

        $params['gender_id'][] = 1;
        $query = $query->where(['pv.is_visible' => -1])
            //->andWhere([ 'ch.parent_category' => $params['category_id'] ])
            ->andWhere([ 'pg.gender_id' => [$params['gender_id'],1] ]);

        //Where
        if (!empty($params['category_id']))
            $query->andWhere([ 'ch.parent_category' => $params['category_id'] ]);
        if (!empty($params['brand_id']))
            $query->andWhere([ 'pb.brand_id' => $params['brand_id'] ]);
        if (!empty($params['size']))
            $query->andWhere([ 'ps.size_id' => $params['size'] ]);
        if (!empty($params['color']))
            $query->andWhere([ 'pcolor.color_id' => $params['color'] ]);

        $query = $query->groupBy(['c.id'])
            ->orderBy(['cnt' => SORT_DESC])
            ->limit(20)
            ->all();

        $data = array();
        foreach ($query as $q) {
            $data[ $q['id'] ] = $q;
        }

        return $data;
    }

    private function category_for_filter_by_flag($gender, $brand_id) {

        if (is_array($gender))
          $gender = $gender[0];

        $count = $this->get_product_count_by_brand($brand_id, $gender);
        $categories = $this->get_main_sub_categories($gender);
        $data = array();

        $i = 0;
        foreach ($categories as $main => $m_c) {
            if (isset($count[ $m_c['id'] ])) {
                $tmp_i = $i;
                $data[$i] = $count[ $m_c['id'] ];
                $data[$i]['main'] = true;
                $data[$i]['sub'] = array();
                $data[$i]['ids'] = $m_c['id'];
                $data[$i]['common_cnt'] = 0;
                ++$i;
            }

            foreach ($m_c['sub_category'] as $k => $s_c) {
                if (isset($count[ $s_c['id'] ])) {
                    $data[$tmp_i]['ids'] .= ';' . $s_c['id'];
                    $data[$tmp_i]['sub'][] = $count[ $s_c['id'] ];
                    $data[$tmp_i]['common_cnt'] += $count[ $s_c['id'] ]['cnt'];
                }
            }

            if (isset($count[ $m_c['id'] ])) {
                $tmp = $count[ $m_c['id'] ];
                $tmp['category_name'] = 'other';
                $data[$tmp_i]['sub'][] = $tmp;
                $data[$tmp_i]['common_cnt'] += $tmp['cnt'];
            }
        }

        return $data;
    }

    public function get_main_sub_categories($gender) {
        $categories_arr = array();
        $main_categ = $this->getMainCategoriesIDs();
        $related_categ = $this->get_related_data();

        foreach ($main_categ as $m_c) {
            if ($m_c['id'] == 4) continue;
            $categories_arr[ $m_c['id'] ] = array(
                'id' => $m_c['id'],
                'category_name' => $m_c['category'],
                'slug' => $m_c['slug'],
            );

            $categories_arr[ $m_c['id'] ]['sub_category'] = $this->getChildren( $m_c['id'], $gender );
            if (!empty($related_categ[ $m_c['id'] ])) {
                $categories_arr[ $m_c['id'] ]['sub_category'] = array_merge($categories_arr[ $m_c['id'] ]['sub_category'], $related_categ[ $m_c['id'] ]);
            }
        }

        return $categories_arr;
    }

    public function get_product_count_by_brand($brand_id, $gender) {
        $query = (new \yii\db\Query())->select(['c.id', 'c.category as category_name', 'COUNT( pc.product_id ) AS cnt'])->from('categories c')
            ->leftJoin('product_categories pc', 'pc.category_id = c.id')
            ->leftJoin('product_gender pg', 'pg.product_id = pc.product_id')
            ->leftJoin('product_brand pb', 'pb.product_id = pg.product_id')
            ->leftJoin('product_slug pv', 'pv.product_id = pb.product_id')
            ->where(['pg.gender_id' => array(1, $gender)])
            ->andWhere(['pb.brand_id' => $brand_id])
            ->andWhere(['pv.is_visible' => -1])
            ->groupBy(['c.id'])
            ->all();

        $data = array();
        foreach ($query as $q) {
            $data[ $q['id'] ] = $q;
        }

        return $data;
    }

    public function get_related_data() {
        $related = $this->getRelated();

        foreach ($related as $main => $r) {
            $related[$main] = $this->get_related_data_by_ids($r);
        }

        return $related;
    }

    public function get_related_data_by_ids($ids = array()) {
        $data = (new \yii\db\Query())->select('*')->from('categories')->where(['id' => $ids])->all();
        return $data;
    }

    public function get_regulable_categories() {
        $data = (new Query())->select(['rc.*', 'c.category_name'])->from('regulable_categories rc')
            ->leftJoin('categories c', 'c.id = rc.category_id')
            ->all();

        return $data;
    }

    public function get_categories_without_relations() {
        $data = array(
            'categories' => array(),
            'count' => 0,
        );

        $query = (new Query())->select(['c.id', 'c.category_name', 'g.gender', 'ch.show'])->from('categories c')
            ->leftJoin('categories_hierarchy ch', 'ch.category = c.id')
            ->leftJoin('gender g', 'g.id = ch.gender')
            ->where(['ch.parent_category' => 0]);

        $data['count'] = $query->count();

        $data['categories'] = $query->all();

        return $data;
    }

    public function get_categories_with_relations() {
        $data = array(
            'categories' => array(),
            'count' => 0,
        );

        $query = (new Query())->select(['c.id', 'c.category_name', 'ch.parent_category', 'g.gender', 'ch.show'])->from('categories c')
            ->leftJoin('categories_hierarchy ch', 'ch.category = c.id')
            ->leftJoin('gender g', 'g.id = ch.gender')
            ->where(['>', 'ch.parent_category', 0])
            ->orderBy(['ch.parent_category' => SORT_ASC]);

        $data['count'] = $query->count();

        $data['categories'] = $query->all();

        return $data;
    }

    public function get_categories_count() {
        $count = (new Query())->select(['id'])->from('categories')->count();
        return $count;
    }

    public function get_related_categories() {
        $data = array(
            'categories' => array(),
            'count' => 0,
        );

        $query = (new Query())->select(['rc.*', 'c.category_name'])->from('relations_categories rc')
            ->leftJoin('categories c', 'c.id = rc.related')
            ->orderBy(['rc.main_category' => SORT_ASC]);

        $data['count'] = $query->count();

        $data['categories'] = $query->all();

        return $data;
    }

    public function get_formated_relations_categories() {
        $query = (new Query())->select('*')->from('relations_categories')->all();

        $data = array();
        foreach ($query as $q) {
            $data[ $q['related']  ] = $q;
        }

        return $data;
    }

    public function get_relations_prod_count_for_filter($categories) {
        $query = $this->get_formated_relations_categories();

        $data = array();
        foreach ($query as $k => $q) {
            $data['relations'][$k] = $q['main_category'];
        }

        foreach ($categories as $c) {
            if (isset($query[ $c['id'] ])) {
                $main = $query[ $c['id'] ]['main_category'];
                $data['count'][$main] = $c['cnt'];
            }
        }

        return $data;
    }

    /**
    * @param string $params['product_id']
    * @param int $params['gender_id']
    *
    * @return array $data Breadcrumbs entity
    */
    public function get_breadcrumbs(array $params = array()) {
        $gender_m = new Gender();
        $data = array(
            'gender_category' => array(),
            'main_category' => array(),
            'category' => array(),
        );

        $data['gender_category'] = $gender_m->get_gender_by_id($params['gender_id']);
        $data['category'] = $this->get_main_category_by_product_id($params['product_id']);

        return $data;
    }

    public function get_active() {
        return (new Query())->select(['c.*'])->from('categories c')->all();
    }

    public function get_categories_with_brand_url($brand_id = 0) {
        $query = (new Query())->select(['c.*', 'bu.slug as path'])
            ->from('categories c')
            ->leftJoin('categories_hierarchy as ch', 'c.id = ch.category_id')
            ->leftJoin('brand_url bu', 'bu.category_id = c.id')
            ->where(['!=', 'c.id', self::DESIGNER_ID])
            ->andWhere(['ch.parent_category' => 0])
            ->andWhere(['bu.brand_id' => $brand_id])
            ->all();

        return $query;
    }

    /**
    * @param array $params
    * @param int $parent_id
    * @param int $gender_id
    * @param int $brand_id
    *
    * @return array $data
    */
    public function get_children_with_brand_url(array $params = array()) {
        $query = (new Query())->select(['c.*', 'ch.parent_category', 'bu.slug as path'])
            ->from('categories c')
            ->leftJoin('categories_hierarchy ch', 'ch.category_id = c.id')
            ->leftJoin('categories_gender cg', 'cg.category_id = c.id')
            ->leftJoin('brand_url bu', 'bu.category_id = c.id')
            ->where(['cg.gender_id' => $params['gender_id'] ])
            ->andWhere(['=', 'ch.parent_category', $params['parent_id']])
            ->andWhere(['bu.brand_id' => $params['brand_id']])
            ->all();

        return $query;
    }

    public function get_category_id_by_product_id($product_id = '') {
        $id = 0;
        $Sql = new Sql();
        $cats = $Sql->sl("product_categories", "product_id=:pid", array("pid"=>$product_id));
        foreach ($cats as $id)
          $ids[] = $id["category_id"];

        $categories = $this->get_related_categories2($ids);

        return $categories;

    }

    public function get_main_category_by_product_id($product_id = '') {
        $query = (new Query)->select('*')->from('categories c')
            ->leftJoin('product_categories pc', 'pc.category_id = c.id')
            ->where(['pc.product_id' => $product_id])
            ->all();

        if (!empty($query)) $query = current($query);

        return $query;
    }




     public function get_childs($category_id) {
       $Sql = new Sql();
       $r   = [];

       $qd = $Sql->select("SELECT category_id as id FROM categories_hierarchy WHERE parent_category=:field", array("field" => $category_id));

       if (!empty($qd))
        foreach ($qd as $v)
          $r[] = $v["id"];



       return $r;
     }

}


?>
