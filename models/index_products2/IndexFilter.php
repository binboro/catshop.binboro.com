<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;
use app\components\Log;

class IndexFilter
{

  private $sizes;
  private $sku;
  private $core;

  public function __construct() {
    $this->slug = new Slug();
  }

  public function start() {

    $core = $this->get_core();

  //  if ($core === false)
    //  return $this->prepare_cores();

    if ($this->main_index() == false)
      return false;
    else {
      $core = $this->get_core();
      $core++;
      $this->set_core($core);
      $this->start();
    }
/*
    $this->brands_init();
    $this->categories_init();
    $this->size_init();
    $this->gender_init();
    $this->color_init();*/
  //  $this->brand_index();
  //  for ($i = 0; $i< 1; $i++) {
  //    $this->brands();
  //  }

  //  $this->brand_index();
      echo "DA";
  }

  private function main_index() {
    $Lg  = new Log(true);
    $Sql = new Sql();
    $Sql->enable_log = true;
    $Lg->show("-1");

    $core = $this->get_core();


    $limit = 500000;

    $offset = $limit * $core - $limit;

    $q = $Sql->select("
      SELECT color_id, size_id, brand_id, category_id, gender_id, product_slug.product_id FROM product_slug
      LEFT JOIN product_colors      ON product_colors.product_id      = product_slug.product_id
      LEFT JOIN product_categories  ON product_categories.product_id  = product_slug.product_id
      LEFT JOIN product_size        ON product_size.product_id        = product_slug.product_id
      LEFT JOIN product_gender      ON product_gender.product_id      = product_slug.product_id
      LEFT JOIN product_brand       ON product_brand.product_id       = product_slug.product_id
      WHERE product_slug.is_visible=1
      LIMIT " . $offset . ", 500000
    ");


    if (empty($q))
      return false;

    $Lg->show("COUNT:".count($q));
    $fields = array("brand_id", "color_id", "size_id", "gender_id", "category_id");

    $insert = array();
    $Lg->show(1);

    for($i = 0; $i < 32; $i++) {
      $bin = (string) decbin($i);
      $l   = strlen($bin);
      $d   = 5 - $l;

      for ($i2 = 0; $i2 < $d; $i2++)
        $bin = "0" . $bin;


      $variants[] = str_split($bin);
    }


echo count($q);

    foreach($q as $r) {

      $data = array();
      foreach ($variants as $variant) {


            if ($variant[0] == 0)
              $data["brand_id"] = 0;
            else
              $data["brand_id"] = $r["brand_id"];

            if ($variant[1] == 0)
              $data["color_id"] = 0;
            else
              $data["color_id"] = $r["color_id"];

            if ($variant[2] == 0)
              $data["size_id"] = 0;
            else
              $data["size_id"] = $r["size_id"];

            if ($variant[3] == 0)
              $data["gender_id"] = 0;
            else
              $data["gender_id"] = $r["gender_id"];

            if ($variant[4] == 0)
              $data["category_id"] = 0;
            else
              $data["category_id"] = $r["category_id"];

            if (empty($data["category_id"]))
              $data["category_id"] = 0;

            if (empty($data["gender_id"]))
              $data["gender_id"] = 0;

            if (empty($data["size_id"]))
              $data["size_id"] = 0;

            if (empty($data["color_id"]))
              $data["color_id"] = 0;

            if (empty($data["brand_id"]))
              $data["brand_id"] = 0;

            $id = md5($data["brand_id"] . "_" . $data["color_id"] . "_" . $data["size_id"] . "_" . $data["category_id"]);
            $data["match_id"] = $id;
          //  $data2["match_id"] = $id;
            $insert[$id] = $data;
          //  $insert2[$id] = $data2;
            $objects[$id][$r["product_id"]] = 1;
      }

    }

    $Lg->show("EE");



    foreach ($insert as $k => $row) {
     $insert[$k]["cnt"] = count($objects[$k]);
    //  $insert2[$k]["cnt"] = $insert[$k]["cnt"];
    }


    $Lg->show(2);
    echo count($insert);
    $Lg->show(3);
    $Lg->show(1.5);

    $Sql->autoInsertInFile("filter_index", $insert, array("id" => "match_id"));

    return true;
    //$Sql->autoInsert("filter_index2", $insert2, array("id" => $fields));
  }

  public function get_core() {
    if (empty($this->core))
      return false;

    return $this->core;
  }

  public function set_core($core) {
    $this->core = $core;
  }

  public function get_core_data() {
    $Sql = new Sql();
    $r = $Sql->select("SELECT * FROM cores WHERE task='filter_index'");
    return $r[0];
  }

  public function prepare_cores() {
    $Sql = new Sql();
    $Sql->enable_log = true;

    $q = $Sql->select("
      SELECT count(*) as cnt FROM product_slug
      LEFT JOIN product_colors      ON product_colors.product_id      = product_slug.product_id
      LEFT JOIN product_categories  ON product_categories.product_id  = product_slug.product_id
      LEFT JOIN product_size        ON product_size.product_id        = product_slug.product_id
      LEFT JOIN product_gender      ON product_gender.product_id      = product_slug.product_id
      LEFT JOIN product_brand       ON product_brand.product_id       = product_slug.product_id

    ");

    $Sql->update("cores", array(array("task"=>"filter_index", "data" => $q[0]["cnt"])), "task");


  }
}

?>
