<?php
namespace app\models\index_products2;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Log;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexBrandsVisibility extends Model
{

  function start() {
      $Lg = new Log(true);
      $limit = 10000;

      for ($i = 0; $i < 1000000; $i++) {
        $Lg->show("Cicle " . $i);

        $offset = $limit * $i;
        if ($this->index($offset, $limit) == false)
          exit();

        usleep(200);
      }


  }

  function index($offset, $limit) {
    $Sql = new Sql();
    $Sql->enable_log = true;
    $Lg = new Log(true);

    $Lg->show(1);

    $Sql->set_limit($offset, $limit);
    $brands_source= $Sql->sl("brands","is_visible=-1");

    if (empty($brands_source))
      return false;

    foreach ($brands_source as $brand)
      $ids[] = $brand["id"];

    unset($brands_source);

    $q = $Sql->select("SELECT distinct(product_brand.brand_id) FROM product_slug
      LEFT JOIN product_brand ON product_brand.product_id = product_slug.product_id

      WHERE product_slug.is_visible=1 AND product_brand.brand_id IN (in:brands)
      LIMIT ".$offset .",".$limit."


    ", array("in:brands"=>$ids), "brand_id");



    if (empty($q))
      return false;


    foreach ($ids as $id) {
      $brands[ $id ] = array(
        "is_visible" => 1,
        "id" => $id
      );

      if (empty($q[ $id ]))
        $brands[ $id ]["is_visible"] = 0;

    }



    $Sql->update("brands", $brands, "id");


      $Lg->show(4);
    return "yes";
  }


}

?>
