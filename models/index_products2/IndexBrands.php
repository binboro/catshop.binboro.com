<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexBrands
{

  private $brands_slugs;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($brand, $product_id) {

    $slugified_id = $this->slug->slugify($brand);

    $this->brands_slugs[$slugified_id]   = $slugified_id;
    $this->brands[$slugified_id] = array("slugified_id" => $slugified_id, "brand" => $brand);
    $this->sku[$slugified_id][] = $product_id;

  }

  function index() {
    if (empty($this->brands))
      return false;

    $Sql = new Sql();
    $Slug = new Slug();
    $start_time = microtime(true);
    $skus = $this->sku;
    $start_time2 = microtime(true);
    $this->brands = $Slug->slugArray($this->brands, array("field" => "brand"), ['brands' => 'slug'], false);
    $slugs = $this->brands_slugs;
    echo "<br>Brand slug: " . (microtime(true) - $start_time2);
    $Sql->autoInsertInFile("brands", $this->brands, array("id"=>"slug"));


    echo "<br>Brand insert: " . (microtime(true) - $start_time);

    $brands_ids = $Sql->select("SELECT id, slugified_id FROM brands WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id" => $this->brands_slugs));
    $brands_ids_cnt = count($brands_ids);

    for ($i = 0; $i < $brands_ids_cnt; $i++ ) {
      $brand = $brands_ids[$i];
      $brands[$brand["slugified_id"]] = $brand["id"];
    }

    $insert = array();

    foreach ($skus as $brand_slug => $v)
      if (!empty($v))
        foreach ($v as $sku)
          $insert[] = array("product_id" => $sku, "brand_id" => $brands[$brand_slug]);



    $Sql->autoInsertInFile("product_brand", $insert, array("id"=>array("brand_id", "product_id")));

    echo "<br>Brand Select: " . (microtime(true) - $start_time);
  }


}
