<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;

class IndexSizes
{

  private $sizes;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($size, $product_id) {

    $slugified_id = $this->slug->slugify($size);

    $this->sizes[] = array("size" => $size, "slugified_id" => $slugified_id);
    $this->sizes_ids[$slugified_id] = $slugified_id;
    $this->sku[$slugified_id][] = $product_id;

  }

  function index() {
    if (empty($this->sizes_ids))
      return true;

    $sku = $this->sku;
    $Sql = new Sql();
    $start_time = microtime(true);

    $Sql->autoInsertInFile("size", $this->sizes, array("id"=>"slugified_id"));

    echo "<br>Size insert: " . (microtime(true) - $start_time);

    if (!empty($this->sizes_ids))
    $g_ids = $Sql->select("SELECT id, slugified_id FROM size WHERE BINARY slugified_id IN (in:size)", array("in:size" => $this->sizes_ids));
    $g_ids_cnt = count($g_ids);

    foreach ($g_ids as $g)
      $gs[ $g["slugified_id"] ] = $g;

    foreach ($sku as $k => $v) {
      foreach ($v as $v2) {
        $skus[] = array(
          "size_id" => $gs[$k]["id"],
          "product_id" => $v2
        );
      }
    }


    if (empty($this->sku))
      return false;

    $Sql->autoInsertInFile("product_size", $skus, array("id"=>array("size_id", "product_id")));

    echo "<br>Size Select: " . (microtime(true) - $start_time);
  }




}
