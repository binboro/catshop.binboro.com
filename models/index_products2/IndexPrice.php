<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexPrice
{
  private $prices;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($price, $product_id, $type) {

    $this->prices[] = array("product_id" => $product_id, "price" => $price, "type" => $type);

  }

  function index() {
    $Sql = new Sql();
    $start_time = microtime(true);


    $Sql->autoInsertInFile("product_price", $this->prices, array("id"=>array("product_id", "type")));


    echo "<br>Price index: " . (microtime(true) - $start_time);
  }


}
