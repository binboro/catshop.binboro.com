<?php
namespace app\models\index_products2;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexProductSlug extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 3000;
  private $skus;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->skus;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }




    public function add($image, $name, $sku) {
      $this->skus[ ] =
        array(
          "name"       =>  $name,
          "product_id"  => $sku
          );;
    }




    public function index() {


  		$steps = ceil( count($this->skus)/ $this->get_items_per_step() );

  		for ($step = 0; $step < $steps; $step ++)
        $this->indexProducts($step);
  	}




  public function indexProducts($step) {
    $startime = microtime(true);
    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();



    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;

    $slugs_array = array();

    for ($i = $start; $i < $end; $i++)
      if (!empty($this->skus[$i]))
        $slugs[] =$this->skus[$i];



    echo "<br> Slug adter loop:" . (microtime(true)-$startime);

    $slugs = $Slug->slugArray($slugs, array("field" => "name"), ['product_slug' => 'slug'], false, "product_id");

    if (!empty($slugs)) {

      foreach ($slugs as &$slug)
        unset($slug["name"]);

    //  echo "LOOKS";
      $Sql->autoInsertInFile("product_slug", $slugs, array("id" => "product_id"));
    }

    echo "<br> Slug adter insert:" . (microtime(true)-$startime);

  }

}
