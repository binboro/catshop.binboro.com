<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexCategories
{

  private $brands_slugs;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($cat, $product_id) {


    $slugified_id = $this->slug->slugify($cat);

    $this->cats_slugs[$slugified_id]   = $slugified_id;
    $this->cats[$slugified_id] = array("slugified_id" => $slugified_id, "category" => $cat);
    $this->sku[$slugified_id][] = $product_id;

  }

  function index() {

    if (empty($this->cats))
      return false;

    $Sql = new Sql();
    $Slug = new Slug();
    $start_time = microtime(true);
    $cats  = $this->cats;
    $sku  = $this->sku;
    $cats_slugs = $this->cats_slugs;
    $skus = $this->sku;

    $cats = $Slug->slugArray($cats, array("field" => "category"), ['categories' => 'slug'], false);


    $Sql->autoInsertInFile("categories", $cats, array("id"=>"slug"));

    echo "<br>Cat insert: " . (microtime(true) - $start_time);


    $start_time = microtime(true);
    $cats_ids = $Sql->select("SELECT id, slugified_id FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id" => $cats_slugs));
    $cats_ids_cnt = count($cats_ids);
    for ($i = 0; $i < $cats_ids_cnt; $i++ ) {
      $cat = &$cats_ids[$i];
      $cats[$cat["slugified_id"]] = $cat["id"];
    }


    foreach ($skus as $slug => $v)
    if (!empty($v))
      foreach ($v as $sku)
        $insert[] = array("category_id" => $cats[$slug], "product_id" => $sku);
/*
    for ($i = 0; $i < $cats_ids_cnt; $i++ ) {
      $cat = &$cats_ids[$i];

      foreach ($this->sku[$cat["slugified_id"]] as $sku)
        $skus[$sku] = array("product_id" => $sku, "category_id" => $cat["id"]);
    }*/
    echo "<br>Cat Sju Num: " . (microtime(true) - $start_time);



    $Sql->autoInsertInFile("product_categories", $insert, array("id"=>array("category_id", "product_id")));

    echo "<br>Categories Index: " . (microtime(true) - $start_time);
  }


}
