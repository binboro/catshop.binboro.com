<?php
namespace app\models\index_products2;

use Yii;
use app\components\Sql;
use app\components\Slug;
use app\components\Log;
use app\components\Mcurl;
use yii\helpers\Url;

class GroupProducts {

  function __construct() {
    $this->sql = new Sql();
    $this->sql->enable_log = false;

    $this->lg = new Log(true);
  }


  public function get_data() {

    $Sql = new Sql();

    $limit  = $_REQUEST["limit"];
    $offset = $limit * ($_REQUEST["core"]-1);
    $Sql->set_limit($offset,$limit);
    $q = $Sql->sl("products");

    foreach ($q as $r) {
      $id1 = $r["sku_number"];
      $id2 = md5($r["name"] . "_" . $r["merchant_id"]);

      if (empty($keys[$id1]))
        $keys[$id1] = $r["id"];

      if (empty($keys[$id2]))
        $keys[$id2] = $r["id"];
    }

    if (empty($q)) {
      echo json_encode(array());
      exit();
    }

    foreach ($q as $r) {
      $data[$r["id"]] = array("id" => $r["id"], "key1" => $r["sku_number"], "key2" => md5($r["name"] . "_" . $r["merchant_id"]));
    }

    echo  json_encode(array("keys" => $keys, "q" => $data));
    exit();

  }


  function save($same) {
    $Sql = new Sql();
    $Sql->enable_log = false;
    $Lg = new Log(true);

    $same_iter = 0;


    foreach ($same as $k => $v) {
      if (count($v) == 1) {
        foreach($v as $r)
          $parent_zero[] = $r["id"];
      }
      else {
          $ids = array();
          foreach($v as $r) {
            $ids[] = $r["id"];
            $same_iter++;
          }

         $Sql->updateIn("products", array("parent_id" => $k), array("id" => $ids));
      }

    }

    $Lg->show("Same: " . $same_iter);
    $Lg->show("Unique: " . count($parent_zero));

    if (!empty($parent_zero))
     $Sql->query("UPDATE products set parent_id=id WHERE id IN ('" . implode("','", $parent_zero) . "')");

  }
  function start() {
    $Mcurl = new Mcurl();
    $base  = Url::base(true);
    $Sql = $this->sql;
    $Sql->enable_log = true;
    $Lg  = new Log(true);
    $Lg->show("Start");

    $cnt = $Sql->select("SELECT count(id) as cnt FROM products WHERE parent_id=-1");
    $cnt = $cnt[0]["cnt"];

    if ($cnt == 0)
      return false;

    if ($cnt > 100)
      $cores = 3;
    else
      $cores = 1;

  //  $cnt = 10000;
    $limit = ceil($cnt/$cores);
  $Lg->show("Start Mcurl");
    $Mcurl->cores($cores);
    $Mcurl->add_same($base .'/shop-cron/group-products-get-data','limit='. $limit);
    $result = $Mcurl->launch(true);


  $Lg->show("Resilt loop s");

    if (!empty($result))
      foreach ($result as $v) {

        if (!empty($v["q"]))
          foreach ($v["q"] as $k => $r)
            $data[$k] = $r;

        if (!empty($v["keys"]))
          foreach ($v["keys"] as $k => $r)
            $keys[$k] = $r;
      }

    unset($result);


    $Lg->show("Resilt" . count($data));

    $Lg->show("Start Sku Loop");
      // Check sku numbers
      foreach ($data as $k=>$r) {
        if (!empty($keys[$r["key1"]]))
          $data[$k]["parent_id"] = $keys[$r["key1"]];
      }
     $Lg->show("End  Sku Loop");


     $Lg->show("Start K2 Loop");
    // Check Key2
    foreach ($data as $k=>$r) {

      if (!empty($keys[$r["key2"]]) AND empty($r["parent_id"]))
        $data[$k]["parent_id"] = $keys[$r["key2"]];
      else if (!empty($keys[$r["key2"]]) AND $r["parent_id"] == $r["id"]) {

        $data[$k]["parent_id"] = $keys[$r["key2"]];
      }
      else if (!empty($keys[$r["key2"]]) AND $r["parent_id"] != $r["id"] AND $r["parent_id"] != $keys[$r["key2"]] ) {
        $keys[$r["key2"]] = $r["parent_id"];
      }

      unset($data[$k]["key1"]);
      unset($data[$k]["key2"]);
    }
    $Lg->show("End  K2 Loop");

    foreach ($data as $r)
      $same[$r["parent_id"]][] = $r;

    $chunks = ceil(count($same)/$cores);

    $same = array_chunk($same, $chunks, true);

    $Mcurl2 = new Mcurl();
    $Mcurl2->cores($cores);
    $Mcurl2->post_for_core($same);
    $Mcurl2->add_same($base .'/shop-cron/group-products-save','');
    $result2 = $Mcurl2->launch(false);

    print_r($result2);
    $Lg->show("The FIN");

    return true;


    
    $data   = array();


    $Lg  = new Log(true);
    $Lg->show("Start");

    $cnt = $Sql->select("SELECT count(id) as cnt FROM products");
    $cnt = $cnt[0]["cnt"];

  //  $cnt = 100;
    $limit = ceil($cnt/4);


    $Mcurl->cores(4);
    $Mcurl->add_same($base .'/shop-cron/group-products','limit='. $limit);
    //$Mcurl->add_post(array("test" => "DA"));
    $result = $Mcurl->launch(true);


    $Lg->show("END");

    $Lg->show("JSIN");


    // Check sku numbers
    foreach ($data as $k=>$r) {


      if (!empty($keys[$r["key1"]]))
        $data[$k]["parent_id"] = $keys[$r["key1"]];
    }


    // Check Key2
    foreach ($data as $k=>$r) {

      if (!empty($keys[$r["key2"]]) AND empty($r["parent_id"]))
        $data[$k]["parent_id"] = $keys[$r["key2"]];
      else if (!empty($keys[$r["key2"]]) AND $r["parent_id"] == $r["id"]) {

        $data[$k]["parent_id"] = $keys[$r["key2"]];
      }
      else if (!empty($keys[$r["key2"]]) AND $r["parent_id"] != $r["id"] AND $r["parent_id"] != $keys[$r["key2"]] ) {



        foreach ($data as $k2 => $r2) {
          if ($r2['parent_id'] == $keys[$r["key2"]])
            $data[$k2]["parent_id"] = $r["parent_id"];
        }

        $keys[$r["key2"]] = $r["parent_id"];


      }

      unset($data[$k]["key1"]);
      unset($data[$k]["key2"]);
    }

    $same = array();

    exit();
    foreach ($data as $r)
      $same[$r["parent_id"]][] = $r;

    $same_iter = 0;


    foreach ($same as $k => $v) {
      if (count($v) == 1) {
        foreach($v as $r)
          $parent_zero[] = $r["id"];
      }
      else {
          $ids = array();
          foreach($v as $r) {
            $ids[] = $r["id"];
            $same_iter++;
          }

          $Sql->updateIn("products", array("parent_id" => $k), array("id" => $ids));
      }

    }

    if (!empty($parent_zero))
      $Sql->query("UPDATE products set parent_id=id WHERE id IN ('" . implode("','", $parent_zero) . "')");


    $Lg->show("Zero: " . count($parent_zero));
    $Lg->show("same: " . $same_iter);

    $Lg->show("JSIN");

  }


  function check_item($items, $arr) {
      $Sql = $this->sql;
      $this->lg->show("S l");

      //  key, product id
      $keys = array();



      foreach ($items as $r) {
        $id1 = $r["sku_number"];
        if (empty($keys[$id1]))
            $keys[$id1] = $r["id"];

        $id2 = $r["name"] . "_" . $r["merchant_id"];
        if (empty($keys[$id2]))
            $keys[$id2] = $r["id"];
      }

      foreach ($arr as $r) {
        $id1 = $r["sku_number"];
        if (empty($keys[$id1]) OR $r["parent_id"] > 0)
            $keys[$id1] = $r["id"];

        $id2 = $r["name"] . "_" . $r["merchant_id"];
        if (empty($keys[$id2]) OR $r["parent_id"] > 0)
            $keys[$id2] = $r["id"];
      }


      $same_keys = array();
      foreach ($arr as $r) {

        $id = $r["sku_number"];

        if (empty($keys[$id])) continue;
        $k1 = $keys[$id];

        $id = $r["name"] . "_" . $r["merchant_id"];
        if (empty($keys[$id])) continue;
        $k2 = $keys[$id];

        $same_keys[$k1][$r["id"]] = $r["id"];
        $same_keys[$k2][$r["id"]] = $r["id"];

      }
      $this->lg->show("S E");

      $this->lg->show("S L2");

      $same = array();
      foreach ($items as $r) {

        $id = $r["sku_number"];
        $k1 = $keys[$id];

        $id = $r["name"] . "_" . $r["merchant_id"];
        $k2 = $keys[$id];

        $same_keys[$k1][$r["id"]] = $r["id"];
        $same_keys[$k2][$r["id"]] = $r["id"];


        if (!empty($same_keys[$k1])) {
          $same[$k1][$r["id"]] = $r["id"];
          $same[$k2][$r["id"]] = $r["id"];

          if (!empty($same_keys[$k1]))
              foreach($same_keys[$k1] as $r2)
                $same[$k1][$r2] = $r2;

          if (!empty($same_keys[$k2]))
              foreach($same_keys[$k2] as $r2)
                $same[$k2][$r2] = $r2;
        }


      }

      // Union by old_name
      echo count($same_keys);
      foreach ($same as $k => $v)
        if (count($v) == 1)
          foreach($v as $r)
            $parent_zero[] = $r;
        else
          $Sql->updateIn("products", array("parent_id" => $k), array("id" => $v));

        if (!empty($parent_zero))
          $Sql->query("UPDATE products set parent_id=id WHERE id IN ('" . implode("','", $parent_zero) . "')");




      $this->lg->show("E L2");
      /*
      foreach ($arr as $r) {
        $id = $r["sku_number"];
        $same_keys[$id][] = $r;

        $id = $r["name"] . "_" . $r["merchant_id"];
        $same_keys[$id][] = $r;
      }

      $this->lg->show("E S");
      foreach ($items as $item) {
        $parent_id = $item["id"];
        $same      = array();

        $id1 = $item["sku_number"];
        $id2 = $item["name"] . "_" . $item["merchant_id"];

        if ( !empty($same_keys[ $id1 ]))
          foreach ($same_keys[ $id1 ] as $r) {
            $same[$r["id"]] = $r["id"];

            if (!empty($same[$r["id"]] ) AND $r["parent_id"] > 0)
              $parent_id = $r["parent_id"];
          }

        else
        if ( !empty($same_keys[ $id2 ]))
          foreach ($same_keys[ $id2 ] as $r) {
            $same[$r["id"]] = $r["id"];

            if (!empty($same[$r["id"]] ) AND $r["parent_id"] > 0)
              $parent_id = $r["parent_id"];
          }

        if (!empty($same))
          $Sql->updateIn("products", array("parent_id" => $parent_id), array("id" => $same));
      }
      */


      $this->lg->show("E l");

  }

}

?>
