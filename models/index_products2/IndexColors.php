<?php
namespace app\models\index_products2;

use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexColors
{

  private $color_slugs;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($color, $product_id) {

    $slugified_id = $this->slug->slugify($color);

    $this->color_slugs[$slugified_id]   = $slugified_id;
    $this->colors[] = array("slugified_id" => $slugified_id, "color" => $color);
    $this->sku[$slugified_id][] = $product_id;

  }

  function index() {
    if (empty($this->colors))
      return false;

    $Sql = new Sql();
    $start_time = microtime(true);

    $Sql->insert("colors", $this->colors, array("id"=>"slugified_id"));

    echo "<br>Colors insert: " . (microtime(true) - $start_time);

    $color_ids = $Sql->select("SELECT id, slugified_id FROM colors WHERE slugified_id IN (in:slugs)", array("in:slugs" => $this->color_slugs));
    $color_ids_cnt = count($color_ids);

    for ($i = 0; $i < $color_ids_cnt; $i++ ) {
      $color = &$color_ids[$i];

      foreach ($this->sku[$color["slugified_id"]] as $sku)
        $skus[$sku] = array("product_id" => $sku, "color_id" => $color["id"]);
    }


    $Sql->autoInsertInFile("product_colors", $skus, array("id"=>array("color_id", "product_id")));
    echo "<br>Coubt:".count($color_ids);
    echo "<br>Colors Select: " . (microtime(true) - $start_time);
  }



}
