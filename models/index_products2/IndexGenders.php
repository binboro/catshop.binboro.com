<?php
namespace app\models\index_products2;
use Yii;
use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexGenders
{

  private $color_slugs;
  private $sku;
  public function __construct() {
    $this->slug = new Slug();
  }
  public function add($gender, $product_id) {
    $slugified_id = $this->slug->slugify($gender);

    $this->gender_slugs[$slugified_id]   = $slugified_id;
    $this->genders[$slugified_id] = array("slugified_id" => $slugified_id, "gender" => $gender);
    $this->sku[$slugified_id][] = $product_id;

  }

  function index() {
    if (empty($this->genders))
      return false;

    $Sql = new Sql();
    $start_time = microtime(true);

    $Sql->autoInsertInFile("gender", $this->genders, array("id"=>"slugified_id"));

    echo "<br>Gender insert: " . (microtime(true) - $start_time);

    if (!empty($this->gender_slugs))
    $g_ids = $Sql->select("SELECT id, slugified_id FROM gender WHERE slugified_id IN (in:slugs)", array("in:slugs" => $this->gender_slugs));
    $g_ids_cnt = count($g_ids);


    for ($i = 0; $i < $g_ids_cnt; $i++ ) {
      $g = &$g_ids[$i];

      foreach ($this->sku[$g["slugified_id"]] as $sku)
        $skus[$sku] = array("product_id" => $sku, "gender_id" => $g["id"]);
    }

    if (empty($skus))
      return false;

    $Sql->autoInsertInFile("product_gender", $skus, array("id"=>array("gender_id", "product_id")));

    echo "<br>Gender Select: " . (microtime(true) - $start_time);
  }




}
