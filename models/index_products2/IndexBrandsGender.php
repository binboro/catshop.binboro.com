<?php
namespace app\models\index_products2;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexBrandsGender extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 3000;
  private $skus;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->skus;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }




    public function add($sku) {
      $this->skus[] = $sku;
    }


    public function index() {


  		$steps = ceil( count($this->skus)/ $this->get_items_per_step() );

  		for ($step = 0; $step < $steps; $step ++)
        $this->indexProducts($step);
  	}

  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();


    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++)
      if (!empty($this->skus[$i]))
        $skus[ $this->skus[$i] ] = $this->skus[$i];



    $q_genders = $Sql->select("SELECT * FROM product_gender     WHERE product_id IN (in:skus)", array("in:skus" => $skus), false);
    $brands    = $Sql->select("SELECT * FROM product_brand WHERE product_id IN (in:skus)", array("in:skus" => $skus), false);


    foreach ($q_genders as $gender)
      $genders[$gender["product_id"]][$gender["gender_id"]] = $gender["gender_id"];

    foreach ($brands as $brand) {
      if (!empty($genders[$brand["product_id"]]) )
        foreach ($genders[$brand["product_id"]] as $gender)  {
          $hash = $brand["brand_id"] ."__" . $gender;

          $brands_genders[$hash] = array(
            "brand_id" => $brand["brand_id"],
            "gender_id"   => $gender,
          );
        }


    }




    if (!empty($brands_genders))
      $Sql->autoInsertInFile("brands_gender", $brands_genders, array("id"=> array("brand_id", "gender_id"), "no_update" => true));

    usleep(150);
  }



    public function update_categories($slugified, $normal_names) {
      $Sql  = new Sql();

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      // Insert new colors
      $insert = array();
      foreach ($slugified as $v)
        if (!empty($normal_names[$v]))
          if (empty($cats[$v]))
            $insert[$v] = array(
              "category" => $normal_names[$v],
              "slugified_id" => $v
            );


      $Sql->autoInsertInFile("categories", $insert, array("id" => "slugified_id"));

      usleep(15000);

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      return $cats;

    }




}
