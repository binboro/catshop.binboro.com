<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\Categories;

class Colors extends Model
{

	/*
	Deprecated
	*/
	public function get_colors_for_filters() {
		$gender = $_REQUEST['gender'];
		$category_id = $_REQUEST["category_id"];

		$data = (new Query())->select('colors.*, pg.gender_id as gender, p_cat.category_id as category, count(pc.product_id) as cnt')->from('colors')
			->leftJoin('product_colors pc', 'pc.color_id = colors.id')
			->leftJoin('product_categories as p_cat', 'p_cat.product_id = pc.product_id')
			->leftJoin('product_gender pg', 'pg.product_id = pc.product_id')
			->where(['=', 'pg.gender_id', $gender])
			->andWhere(['=', 'p_cat.category_id', $category_id])
			->groupBy(['colors.id'])
			->orderBy(['count(pc.product_id)' => SORT_DESC])
			->limit(20);

		$data = $data->all();
		return $data;
	}

	public function get_colors_for_filter($params = array()) {
		$Categories = new Categories();

		$query = (new Query())
			->select(['c.*', 'pc.color_id', 'pg.gender_id as gender', /*'p_cat.category_id as category',*/ 'count(pc.product_id) as cnt'])
			->from('colors c')
			->leftJoin('product_colors pc', 'pc.color_id = c.id')
			->leftJoin('product_gender pg', 'pg.product_id = pc.product_id');

		//Joins
		if (!empty($params['sub_category']) || !empty($params['category_id']))
			$query->leftJoin('product_categories p_cat', 'p_cat.product_id = pc.product_id');
		if (!empty($params['brand_id']))
			$query->leftJoin('product_brand pb', 'pb.product_id = pc.product_id');
		if (!empty($params['size']))
			$query->leftJoin('product_size ps', 'ps.product_id = pc.product_id');
		if (!empty($params['min_price']) || !empty($params['max_price']))
			$query->leftJoin('product_price pp', 'pp.product_id = pc.product_id');

		//Conditions
		$params['gender_id'][] = 1;
		$query->where(['pg.gender_id' => $params['gender_id'] ]);
		if (!empty($params['sub_category']))
			$query->andWhere(['p_cat.category_id' => $params['sub_category']]);
		elseif (!empty($params['category_id']))  {

			if (!is_array($params["category_id"])) {
				$childs = $Categories->get_childs($params["category_id"]);
				$params['category_id'] = array_merge(array($params['category_id']), $childs);
			}

			$query->andWhere(['p_cat.category_id' => $params['category_id'] ]);
		}
		if (!empty($params['brand_id']))
			$query->andWhere(['pb.brand_id' => $params['brand_id']]);
		if (!empty($params['size']))
			$query->andWhere(['ps.size_id' => $params['size']]);
		if (!empty($params['min_price']))
			$query->andWhere([ '>=', 'pp.product_price', $params['min_price'] ]);
		if (!empty($params['max_price']))
			$query->andWhere([ '<=', 'pp.product_price', $params['max_price'] ]);

		$query->groupBy(['c.id'])
			->orderBy(['cnt' => SORT_DESC])
			->limit(20);
		$data = $query->all();

		return $data;
	}

	public function get_colors_by_product_id($product_id) {
		$query = (new Query())->select('*')->from('colors c')
			->leftJoin('product_colors pc', 'pc.color_id = c.id')
			->where(['pc.product_id' => $product_id])
			->all();

		return $query;
	}

}
