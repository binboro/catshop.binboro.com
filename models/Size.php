<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\Categories;
class Size extends Model
{

	/*
	Deprecated
	*/
	public function get_size_for_filters() {
		$gender = $_REQUEST['gender'];
		$category_id = $_REQUEST["category_id"];

		$data = (new Query())->select('size.*, pg.gender_id as gender, pc.category_id as category, count(ps.sku_number) as cnt')->from('size')
			->leftJoin('product_size ps', 'ps.size_id = size.id')
			->leftJoin('product_categories as pc', 'pc.sku_number = ps.sku_number')
			->leftJoin('product_gender pg', 'pg.sku_number = ps.sku_number')
			->where(['=', 'pg.gender_id', $gender])
			->andWhere(['=', 'pc.category_id', $category_id])
			->groupBy(['size.id'])
			->orderBy(['cnt' => SORT_DESC])
			->limit(20);

		$data = $data->all();
		return $data;
	}

	public function get_size_for_filter($params) {
		$Categories = new Categories();

		$query = (new Query())
			->select(['s.*', 'ps.size_id', 'pg.gender_id as gender', 'pc.category_id as category', 'count(ps.sku_number) as cnt'])
			->from('size s')
			->leftJoin('product_size ps', 'ps.size_id = s.id')
			->leftJoin('product_categories pc', 'pc.sku_number = ps.sku_number')
			->leftJoin('product_gender pg', 'pg.sku_number = ps.sku_number');

		//Joins
		if (!empty($params['brand_id']))
			$query->leftJoin('product_brand pb', 'pb.sku_number = pc.sku_number');
		if (!empty($params['color']))
			$query->leftJoin('product_colors pcolor', 'pcolor.sku_number = pc.sku_number');

		//Conditions
		$params['gender_id'][] = 1;
		$query->where([ 'pg.gender_id' => $params['gender_id'] ]);
		if (!empty($params["category_id"])) {
			if (!is_array($params["category_id"])) {
				$childs = $Categories->get_childs($params["category_id"]);
				$params['category_id'] = array_merge(array($params['category_id']), $childs);
			}


			$query->andWhere(['pc.category_id' => $params['category_id'] ]);
		}

		if (!empty($params['brand_id']))
			$query->andWhere(['pb.brand_id' => $params['brand_id']]);
		if (!empty($params['color']))
			$query->andWhere(['pcolor.color_id' => $params['color']]);

		$query->groupBy(['s.id'])
			->orderBy(['cnt' => SORT_DESC])
			->limit(20);

		$data = $query->all();

		return $data;
	}

	public function get_size_by_product_id($product_id) {
		$query = (new Query())->select('*')->from('size s')
			->leftJoin('product_size ps', 'ps.size_id = s.id')
			->where(['ps.product_id' => $product_id])
			->all();

		return $query;
	}

}
