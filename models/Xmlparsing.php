<?php

namespace app\models;

use yii\db\Query;
use Yii;
use app\components\Slug;
use app\components\Sql;
use app\components\SqlHeavy;
use app\components\Arrays;
use app\components\XmlParser;
use app\components\Log;
use app\components\Firewall;
use app\components\Mcurl;
use yii\helpers\Url;
class Xmlparsing {

  function __construct() {
    $this->lg = new Log(false, "work_with_files");
    $this->lg->show("Xmlparsing constructed");
  }


  function parse_products($xml, $source, $rules) {

    $this->lg->show("parse_products");

    $lg = new Log();
    $XmlParser = new XmlParser();
    $Firewall = new Firewall(false);
    $Firewall->set_rules($rules);
    $start = microtime(true);

    $Sql   = new Sql();
    $Array = new Arrays();
    $cnt = count($xml->product);

    $products = array();
    $productArr   = array();
    $groupCount = 0;
    $count      = 0;
    $lg->show("parse_products ".$cnt);

    //for ($xml_index = 0; $xml_index < $cnt; $xml_index++) {
    $this->lg->show("foreach S");

    foreach ($xml->product as $p) {

      $product = $XmlParser->xml2array($p);


      $productArr  = array();
      $productArr2 = array();

      $this->lg->show("obj_to_key_value S");
      foreach ($product as $k => $v)
        $productArr = $Array->obj_to_key_value($k, $v, $productArr);

      $this->lg->show("productArr as k => v");
      foreach ($productArr as $k => $v) {
        $newK = str_replace("attributeClass_", "", $k);
        $newK = str_replace("@attributes_", "", $newK);
        $newK = str_replace("@", "", $newK);

        $productArr2[$newK] = $v;
      }
$this->lg->show("productArr as k => v END");

        $productArr2["md5"] = md5(implode("", $productArr2));

      if (empty($productArr2["upc"])) {
      $unique_id = uniqid(). microtime();
        $productArr2["no_upc"]    = 1;

      }
      else  {
        $unique_id = $productArr2["upc"];
        $productArr2["no_upc"]    = 0;
      }

      //  $productArr2["source"] = json_encode($source);
      $productArr2["update_time"] = time();
      $productArr2["update_date_time"] = date("Y-m-d H:i:s");


      $productArr2["merchant_id"] = (string)$source["merchant_id"];
      $productArr2["merchant_name"] = (string)$source["merchant_name"];

      $productArr2["parent"] = -1;

      $products[$unique_id] = $productArr2;

      $count++;
    }
    $this->lg->show("foreach END");

    $lg->show("Firewall S");
  //   $products = $this->firewall_protect($products);
    $Firewall->protect($products);
    $lg->show("Firewall E");
    return $products;
    $lg->show("autoInsertInFile S");
    //$Sql->autoInsertInFile("products", $products, array("id" => "sku_number", "id_hash" => "md5"));
    $lg->show("autoInsertInFile E");
    return $count;

  }


  function firewall_protect($products) {

    if (empty($products)) return false;

    $Firewall = new Firewall(false);
    $rules = $Firewall->get_rules();



    $Mcurl = new Mcurl();

    $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/firewall",10);
    $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/firewall",5);
    $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/firewall",5);

    $Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    $Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    $Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    $Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);


    $result = $Mcurl->loop($products, $rules);
  }

  function check_in_firewall($data, $rules = false) {
    $Firewall = new Firewall(false);

    $Firewall->set_rules($rules);
    $p = $data;
    $Firewall->protect($p);

    return $p;
  }


}

?>
