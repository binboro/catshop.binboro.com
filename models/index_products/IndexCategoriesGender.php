<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexCategoriesGender extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );

		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;
              
      $Product->set_product($products[$i]);

      $skus[ $Product->get_skunumber() ] = $Product->get_skunumber();

    }

    $q_genders = $Sql->select("SELECT * FROM product_gender     WHERE sku_number IN (in:skus)", array("in:skus" => $skus), false);
    $cats    = $Sql->select("SELECT * FROM product_categories WHERE sku_number IN (in:skus)", array("in:skus" => $skus), false);


    foreach ($q_genders as $gender)
      $genders[$gender["sku_number"]][$gender["gender_id"]] = $gender["gender_id"];

    foreach ($cats as $cat) {
      if (!empty($genders[$cat["sku_number"]]) )
        foreach ($genders[$cat["sku_number"]] as $gender)  {
          $hash = $cat["category_id"] ."__" . $gender;

          $categories_genders[$hash] = array(
            "category_id" => $cat["category_id"],
            "gender_id"   => $gender,
          );
        }


    }

    if (!empty($categories_genders))
      $Sql->autoInsert("categories_gender", $categories_genders, array("id"=> array("category_id", "gender_id")));

    usleep(15000);
  }



    public function update_categories($slugified, $normal_names) {
      $Sql  = new Sql();

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      // Insert new colors
      $insert = array();
      foreach ($slugified as $v)
        if (!empty($normal_names[$v]))
          if (empty($cats[$v]))
            $insert[$v] = array(
              "category" => $normal_names[$v],
              "slugified_id" => $v
            );


      $Sql->autoInsert("categories", $insert, array("id" => "slugified_id"));

      usleep(15000);

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      return $cats;

    }




}
