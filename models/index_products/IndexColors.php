<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\models\Products;

class IndexColors extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );
    echo $this->getProductsNumber() ."/ ". $this->get_items_per_step() ;
		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql = new Sql();
    $Product = new Products();
    $Slug = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;      
      $Product->set_product($products[$i]);
      $slugified_colors = $Slug->slugify($Product->get_color());

      $colors_to_check[ $slugified_colors ] = $slugified_colors;
      $colors[ $slugified_colors]           = $Product->get_color();
    }

    $colors = $this->update_colors($colors_to_check, $colors);


    $color_skunumber = array();
    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;

      $Product->set_product($products[$i]);

      $slugified = $Slug->slugify(  $Product->get_color()  );

      if (empty($colors[ $slugified ]))
        continue;

      $color_skunumber[] =
        array(
          "color_id" =>   $colors[ $slugified ]["id"],
          "sku_number" => $Product->get_skunumber()
        );

    }

    if (!empty($color_skunumber))
      $Sql->autoInsert("product_colors", $color_skunumber, false);

    usleep(15000);
  }


  public function update_colors($slugified_colors, $normal_names) {
    $Sql  = new Sql();
    $Slug = new Slug();
    $colors = $Sql->select("SELECT * FROM colors WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified_colors), "slugified_id");

    // Insert new colors
    $insert = array();
    foreach ($slugified_colors as $v)
      if (!empty($normal_names[$v]))
        if (empty($colors[$v]))
          $insert[$v] = array(
            "color" => $normal_names[$v],
            "slugified_id" => $v
          );


    $Sql->autoInsert("colors", $insert, array("id" => "slugified_id"));

    usleep(15000);

    $colors = $Sql->select("SELECT * FROM colors WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified_colors), "slugified_id");

    return $colors;

  }




}
