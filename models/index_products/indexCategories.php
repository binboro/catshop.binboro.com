<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexCategories extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );
    echo $this->getProductsNumber() ."/ ". $this->get_items_per_step() ;
		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;
              
      $Product->set_product($products[$i]);

      // Product type
      if (!empty($Product->get_product_type() )) {
        $slugified = $Slug->slugify(  $Product->get_product_type()  );

        $cats_to_check[ $slugified] = $slugified;
        $cats[$slugified] = $Product->get_product_type();
      }

      // Primary Category type
      if (!empty($Product->get_cat_primary() )) {
        $slugified = $Slug->slugify(  $Product->get_cat_primary()  );

        $cats_to_check[ $slugified] = $slugified;
        $cats[$slugified] = $Product->get_cat_primary();
      }

      // Secondary Category type
      if (!empty($Product->get_cat_secondary() )) {
        $slugified = $Slug->slugify(  $Product->get_cat_secondary()  );

        $cats_to_check[ $slugified] = $slugified;
        $cats[$slugified] = $Product->get_cat_secondary();
      }
    }

    $cats = $this->update_categories($cats_to_check,$cats);


    $cats_skunumber = array();
    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;

      $Product->set_product($products[$i]);

      $slugified = $Slug->slugify(  $Product->get_brandName()  );

      if (empty($cats[  $slugified ]))
        continue;

      $cats_skunumber[] =
        array(
          "category_id" =>   $cats[ $slugified]["id"],
          "sku_number" => $Product->get_skunumber()
        );

    }

    if (!empty($cats_skunumber))
      $Sql->autoInsert("product_categories", $cats_skunumber, false);

    usleep(15000);
  }



    public function update_categories($slugified, $normal_names) {
      $Sql  = new Sql();

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      // Insert new colors
      $insert = array();
      foreach ($slugified as $v)
        if (!empty($normal_names[$v]))
          if (empty($cats[$v]))
            $insert[$v] = array(
              "category" => $normal_names[$v],
              "slugified_id" => $v
            );


      $Sql->autoInsert("categories", $insert, array("id" => "slugified_id"));

      usleep(15000);

      $cats = $Sql->select("SELECT * FROM categories WHERE slugified_id IN (in:slugified_id)", array("in:slugified_id"=> $slugified), "slugified_id");

      return $cats;

    }




}
