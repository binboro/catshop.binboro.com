<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexProductSlug extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );
    echo $this->getProductsNumber() ."/ ". $this->get_items_per_step() ;
		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;
              
      $Product->set_product($products[$i]);

      $slugs[$Product->get_skunumber()] =
        array(
          "slug"       =>  $Slug->genereteSlug($Product->get_name(), ['product_slug' => 'slug']) ,
          "image"      =>  $Product->get_image(),
          "name"       =>  $Product->get_name(),
          "sku_number"  => $Product->get_skunumber(),
        );

    }



    if (!empty($slugs)) {
      echo "LOOKS";
      $Sql->autoInsert("product_slug", $slugs, array("id" => "sku_number"));
    }

    usleep(15000);
  }

}
