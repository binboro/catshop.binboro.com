<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexSizes extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );
    echo $this->getProductsNumber() ."/ ". $this->get_items_per_step() ;
		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;
      $Product->set_product($products[$i]);

      $sizes[$Product->get_size()] = $Product->get_size();
    }

    $sizes = $this->update_size($sizes);


    $size_skunumber = array();
    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;
              
      $Product->set_product($products[$i]);


      if (empty($sizes[  $Product->get_size()  ]))
        continue;

      $size_skunumber[] =
        array(
          "size_id" =>   $sizes[ $Product->get_size() ]["id"],
          "sku_number" => $Product->get_skunumber()
        );

    }

    $Sql->autoInsert("product_size", $size_skunumber, false);

    usleep(15000);
  }



    public function update_size($sizes_to_check) {
      $Sql  = new Sql();

      $found = $Sql->select("SELECT * FROM size WHERE size IN (in:size)", array("in:size"=> $sizes_to_check), "size");

      // Insert new colors
      $insert = array();
      foreach ($sizes_to_check as $v)
        if (!empty($v))
          if (empty($found[$v]))
            $insert[$v] = array(
              "size" => $sizes_to_check[$v]
            );


      $Sql->autoInsert("size", $insert, array("id" => "size"));

      usleep(15000);

      $found = $Sql->select("SELECT * FROM size WHERE size IN (in:size)", array("in:size"=> $sizes_to_check), "size");

      return $found;

    }




}
