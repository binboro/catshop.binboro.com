<?php
namespace app\models\index_products;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Sql;
use app\components\Slug;
use app\components\Strings;
use app\models\Products;

class IndexPrice extends Model
{

  private $products_number  = 0;
  private $items_per_step   = 500;

  public function setProductsNumber($n) {
    $this->products_number = $n;
  }

  public function getProductsNumber() {
    return $this->products_number;
  }

  public function setProducts($products) {
    $this->products = $products;
  }

  public function getProducts() {
    return $this->products;
  }

  public function get_items_per_step() {
    return $this->items_per_step;
  }



  public function index($products) {

    $this->setProducts($products);
    $this->setProductsNumber(count($products));

		$steps = ceil($this->getProductsNumber() / $this->get_items_per_step() );
    echo $this->getProductsNumber() ."/ ". $this->get_items_per_step() ;
		for ($step = 0; $step < $steps; $step ++)
      $this->indexProducts($step);
	}



  public function indexProducts($step) {

    $Sql      = new Sql();
    $Product  = new Products();
    $Slug     = new Slug();

    usleep(15000);
    $products = $this->getProducts();

    $limit  = $this->get_items_per_step();
    $start  = $step * $limit;
    $end    = $start + $limit;


    for ($i = $start; $i < $end; $i++) {
      if (empty($products[$i]))
        continue;

      $Product->set_product($products[$i]);

      $deleteSkuNumbers[$Product->get_skunumber()] = $Product->get_skunumber();

      $price_skunumber[] =
        array(
          "price"       => $Product->get_retail_price() ,
          "sku_number"  => $Product->get_skunumber(),
          "type"        => "retail"
        );

      if (!empty($Product->get_sale_price())) {
        $price_skunumber[] =
          array(
            "price"       => $Product->get_sale_price() ,
            "sku_number"  => $Product->get_skunumber(),
            "type"        => "sale"
          );
      }
    }

    if (!empty($deleteSkuNumbers))
      $Sql->delete("DELETE  FROM product_price WHERE sku_number IN (in:ids)", array("in:ids"=>$deleteSkuNumbers));

    if (!empty($price_skunumber))
      $Sql->autoInsert("product_price", $price_skunumber, false);

    usleep(15000);
  }

}
