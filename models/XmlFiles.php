<?php

namespace app\models;

use yii\db\Query;
use Yii;
use app\components\Slug;
use app\components\Sql;
use app\components\SqlHeavy;
use app\models\Xmlparsing;
use app\components\XmlParser;
use app\components\Arrays;
use app\components\Log;
use app\components\Mcurl;
use app\components\Firewall;
use yii\helpers\Url;

class Xmlfiles {

  public $path;
  private $lg = false;
  private $loop_protect = 0;
  function __construct() {
    ignore_user_abort(true);
    set_time_limit(0);

    $this->lg = new Log(true, "xml_parser");
    $this->path = dirname(__DIR__) . '/xml';

    $this->lg->show("XmlFiles constructed");
  }






  function parse_file() {
    $Sql = new Sql();
    $Mcurl = new Mcurl();

    if ($this->loop_protect > 1000000) {
      $this->lg->show("Infinite loop stopped by protection");
      return false;
    }


    $file = $this->get_file();

    if (empty($file)) {
      $this->lg->show("No files to parse");
      return false;
    }

    $this->file_ftell = 0;
    for ($i = 0; $i > -1; $i++)
      if ($this->work_with_file($file) == false)
        break;

    $this->lg->show("File read E");

    usleep(20000);
    // Take next file to parse
    $this->loop_protect++;
    $this->parse_file();
  }


  function parse_file2() {
    $Sql = new Sql();


    if ($this->loop_protect > 1000000) {
      $this->lg->show("Infinite loop stopped by protection");
      return false;
    }

    $file = $this->get_file();

    if ($file === false) {
      $this->lg->show("No files to parse");
      return false;
    }

    $this->lg->show("File read S");
    for ($i = 0; $i > -1; $i++) {
      if ($this->work_with_file($file) == false)
        break;

      usleep(1000);
    }
    $this->lg->show("File read E");

    // Take next file to parse
    $this->loop_protect++;
  //  $this->parse_file();
  }


  function work_with_file($file) {

    $Sql = new Sql();
    $Mcurl = new Mcurl();
    $Firewall = new Firewall();
    $firewall_rules = $Firewall->get_rules();
    $allDone = false;
    $handle = fopen($this->path . "/" . $file["file_name"], "r");
    fseek($handle,   0 , SEEK_SET);


    $lines_limit = 20000;


    $first_line = fgets($handle);
    $this->lg->show("First line of file: " . $first_line);

    $lines = array();

    if ($this->file_ftell != 0)
      fseek($handle,   $this->file_ftell , SEEK_SET);
    for ($i = 0; $i <  $lines_limit; $i++ ) {
      $line = fgets($handle);


      if ($line === false) {
        $allDone = true;
        break;
      }

      $lines[] = $line;
    }


    $this->file_ftell = (int)ftell($handle);

    $this->lg->show("Lines to XML S " . count($lines));
    $result = [];

    if (!empty($lines)) {
        $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/load-xml-string",10);

        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",5);

        $Mcurl->add_core("http://catwalker_api.web2/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web2/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web2/xmlparsing/load-xml-string",5);

        $Mcurl->add_core("http://catwalker_api.web3/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web3/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web3/xmlparsing/load-xml-string",10);



        $Mcurl->add_core(Url::base(true)."/xmlparsing/load-xml-string", 5);
        $Mcurl->add_core(Url::base(true)."/xmlparsing/load-xml-string", 5);

    		$result = $Mcurl->loop($lines, array("first_line" => $first_line, "file" => $file, "rules" => $firewall_rules));

    }
    $this->lg->show("Lines to Mcurl E");
    if (!empty($result))
      $Sql->autoInsertInFile("products", $result, array("id" => "sku_number", "id_hash" => "md5"));



    if (empty($lines) OR count($lines) < $lines_limit) {

      $file["file_busy"] = 2;
      $Sql->autoUpdate("xml_files", array($file), "id");
      $this->lg->show("All is done from Mcurl " .  count($lines));
      $allDone = true;
    }

    $this->lg->show("Lines to XML E");

    $this->file_ftell = (int)ftell($handle);
    $this->lg->show("Read of 100000 lines, actually read: " . count($lines));

    $this->file_ftell = (int)ftell($handle);

    if ($allDone === true) {
      $this->lg->show("allDone for file");
      $file["file_busy"] = 2;
      $Sql->autoUpdate("xml_files", array($file), "id");
      $allDone = true;
      return false;
    }
    else
      return true;

  }

  function work_with_file2($file) {

    $this->lg->show("Lines to XML E");
    $Sql = new Sql();
    $Mcurl = new Mcurl();
    $Firewall = new Firewall();
    $firewall_rules = $Firewall->get_rules();
    $allDone = false;
    $handle = fopen($this->path . "/" . $file["file_name"], "r");
    fseek($handle,   0 , SEEK_SET);



    $first_line = fgets($handle);
    $this->lg->show("First line of file: " . $first_line);

    $lines = array();

    if ($this->file_ftell != 0)
      fseek($handle,   $this->file_ftell , SEEK_SET);
    for ($i = 0; $i <  300000; $i++ ) {
      $line = fgets($handle);


      if ($line === false) {
        $allDone = true;
        break;
      }

      $lines[] = $line;
    }


    $this->file_ftell = (int)ftell($handle);

    $this->lg->show("Lines to XML S " . count($lines));
    $result = [];
    if (!empty($lines)) {
        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",10);
        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",15);
        $Mcurl->add_core("http://catwalker_api.web/xmlparsing/load-xml-string",15);

      //  $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/load-xml-string",30);

        $Mcurl->add_core(Url::base(true)."/xmlparsing/load-xml-string", 20);
    		$Mcurl->add_core(Url::base(true)."/xmlparsing/load-xml-string", 20);
    		$Mcurl->add_core(Url::base(true)."/xmlparsing/load-xml-string", 20);

    		$result = $Mcurl->loop($lines, array("first_line" => $first_line, "file" => $file, "rules" => $firewall_rules));

    }
    $this->lg->show("Lines to Mcurl E");
    if (!empty($result))
      $Sql->autoInsertInFile("products", $result, array("id" => "sku_number", "id_hash" => "md5"));


    if (empty($result) OR empty($lines) OR count($lines) < 100000) {
      $file["file_busy"] = 2;
      $Sql->autoUpdate("xml_files", array($file), "id");
      $allDone = true;
    }

    $this->lg->show("Lines to XML E");

    $this->file_ftell = (int)ftell($handle);
    $this->lg->show("Read of 100000 lines, actually read: " . count($lines));

    $this->file_ftell = (int)ftell($handle);

    if ($allDone === true) {
      $file["file_busy"] = 2;
      $Sql->autoUpdate("xml_files", array($file), "id");
      $allDone = true;
      return false;
    }
    else
      return true;

  }

  function get_file() {
    // This function will take free file to parse and set it to busy mode

    $Sql = new Sql();
    //$q = $Sql->sl("xml_files", "file_busy=0  ORDER BY file_size DESC limit 0,1");
    $q = $Sql->sl("xml_files", "file_busy=0 ORDER BY file_size DESC limit 0,1");


    $q = $q[0];

    $this->lg->show("Got file: " . json_encode($q));

    $this->file_ftell = 0;
    return $q;
  }

  function get_files($limit) {
    // This function will take free file to parse and set it to busy mode

    $Sql = new Sql();
    //$q = $Sql->sl("xml_files", "file_busy=0  ORDER BY file_size DESC limit 0,1");
    $q = $Sql->sl("xml_files", "id=87 ORDER BY file_size DESC limit 0," . $limit);




    $this->lg->show("Got file: " . json_encode($q));

    $this->file_ftell = 0;
    return $q;
  }

  function find_new_files() {

    $Sql = new Sql();

    $path = $this->path;
    $scan = scandir($path);

    $scan_count = count($scan);

    $files = array();

    for ($i = 0; $i < $scan_count; $i ++ ) {
      $file = $scan[$i];

      if ($file == "." OR $file == "..")
        continue;

      $fSize = filesize($path . "/" . $file);

      $files[] = array(
        "file_name"     => $file,
        "file_size"     => $fSize,
        "create_time"   => time(),
        "file_parsed"   => 0,
        "items_parsed"  => 0,
        "file_busy"     => 0,
        "id_hash"       => md5($file . $fSize)
      );

     }

     $Sql->autoInsert("xml_files", $files, array("id" => "file_name", "id_hash" => "id_hash"));
  }

  function parse_file_old() {

    $lg = new Log();

    $ar = array(
        array("name" => "Dima"),
        array("name" => "bob")
      );


    if (empty($_REQUEST["core"])) {
      echo "NO CORES";
      exit();
    }


    $start = microtime(true);
    $Sql = new Sql();

    $file = $this->get_free_file_to_parse();


    if ($file == false) {
      $lg->show("No files");
      exit();
    }
    $lg->show("parse file " . $file["file_name"]);


    $fData = $this->get_xmlfile_array($file);
    $lg->show("get_xmlfile_array ");




    if (!empty($file) AND !empty($file["file_size"]) AND empty($_REQUEST["reload"])){
      usleep(2000);

      $lg->show("Next file ");
      $this->parse_file();
    }
    else if (!empty($_REQUEST["reload"]))
      echo '<script>alert("Look!");setTimeout(function() {location.href=location.href;}, 9);</script>';

  }


  function get_free_file_to_parse() {
      $lg = new Log();
      $Sql = new Sql();
      $lg->show("get_free_file_to_parse S ");


      $core = $_REQUEST["core"];



      $cores = $Sql->select("SELECT * FROM cores WHERE task='xml_files'");
      $cores = $cores[0];
      $cores_data = json_decode(stripslashes($cores["data"]),1);




      $files = $cores_data["cores"][$core];


  //exit();
      if (empty($files)) return false;

      foreach ($files as $k => $f) {
        $fk = $k;
        $file = $f;
        break;
      }






      unset($cores_data["cores"][$core][ $fk]);

      $cores["data"] = json_encode($cores_data);
      $Sql->update("cores", array($cores), "id");


      //  $file = $Sql->select("SELECT * FROM xml_files WHERE id = 10  ORDER BY file_size ASC limit 1");
      $lg->show("get_free_file_to_parse E ");

      if (empty($file))
        return false;

      return $file;

  }

  function get_xmlfile_array($file) {
    $lg = new Log();
    $Sql = new Sql();
    $XmlParsing = new Xmlparsing();

    $start = microtime(true);
    // Open file

    $file["file_busy"] = 1;
    $file["core"] = $_REQUEST["core"];
    $file["parse_status"] = "Opening file (step 1)";

    $Sql->autoUpdate("xml_files", array($file), "id");



    $lg->show("get_xmlfile_array S");




    $lg->show("Open file S");
    $handle = fopen($this->path . "/" . $file["file_name"], "r");
    $linesCount = 0;

    $lg->show("Open file E");

    if (empty($file["step_start"]))
      $file["step_start"] = 0;


    $file["parse_status"] = "File is opened (step 2)";



    $file["parsed_in_seconds"] = 0;

    $Sql->autoUpdate("xml_files", array($file), "id");



    $data = fgets($handle);


  //  if (!empty($file["ftell"]))
    //  fseek($handle,   $file["ftell"] , SEEK_SET);


    $lg->show("fgets loop S");
    $end = $file["start"] + $file["end"];
    $lines = 0;
    for ($i = 0; $i <  $file["lines"]; $i++ ) {



      if ($i > $end)
        break;

      $line = fgets($handle);

      if ($i < $file["start"])
        continue;

      $lines++;

      if ($line === false)
        break;
      else {
        $data .= $line;
      }
    }

    $lg->show("fgets loop E" . $lines);

    $data .= '</merchandiser>';



    $data = str_replace("</merchandiser></merchandiser>", "</merchandiser>", $data);

    $lg->show("simplexml_load_string S");
    $file["parsed_in_seconds"] = microtime(true);
  //  echo $data;
    $xml  = @simplexml_load_string($data);
    $lg->show("simplexml_load_string E");

    if (is_object($xml))
      $lg->show("Count = ".count($xml->product));


    $file["parsed_in_seconds"] =  microtime(true) -  $file["parsed_in_seconds"];

    if (!is_object($xml) OR empty($xml->product)) {

$lg->show("empty(xml->product) S");
      $file["file_busy"] = 2;
      $file["parse_status"] = "File is closed (step 3)";

      if (empty($file["parsed_items"]))
        $file["parsed_items"]  = 0;


      $Sql->autoUpdate("xml_files", array($file), "id");
$lg->show("empty(xml->product) E");

      return true;


    }


    $file["ftell"]          = (int)ftell($handle);
    $file["merchant_id"]    = $xml->header->merchantId;
    $file["merchant_name"]  = $xml->header->merchantName;



    // Start product parsing
    $lg->show("parse_products S");
    $products_count = $XmlParsing->parse_products($xml, $file);
    $lg->show("products_count:" . $products_count);
    // Log product parsing result
    if (empty($file["parsed_items"]))
      $file["parsed_items"] = $products_count;
    else
      $file["parsed_items"] += $products_count;

    $file["parse_status"] = "Products parsed (step 3)";
    $file["parsed_in_seconds"] = (microtime(true) - $start);

    $file["file_busy"] = "1";


    $Sql->autoUpdate("xml_files", array($file), "id");

    $lg->show("File is done");
  }


  public function prepare_core_tasks() {
		$Sql = new Sql();
		$cores  = $Sql->select("SELECT * FROM cores WHERE task='xml_files' ");
		$cores = $cores[0];

	//	print_r(json_decode(stripslashes($cores["data"]),1));

/*
    if (!empty($file["ftell"]))
      fseek($handle,   $file["ftell"] , SEEK_SET);
(int)ftell($handle);
    $lg->show("fgets loop S");

    for ($i = $file["step_start"]; $i < ($file["step_start"] + 50000); $i++ ) {
      $line = fgets($handle);
*/
// file_busy = 0 OR file_busy = 1
    $files = $Sql->select("SELECT * FROM xml_files  WHERE file_busy=0  ORDER BY  file_size DESC limit 400");

    foreach ($files as $k => $file) {

      $handle = fopen($this->path . "/" . $file["file_name"], "r");
      fseek($handle,   0 , SEEK_SET);

      for ($i = 0; $i <  50000000; $i++ ) {
        $line = fgets($handle);
        if (empty($line)) {

          break;
        }
      }

      $files[$k]["lines"] =  $i;
    }

    $Sql->update("xml_files", $files, "id");

    $core = 1;
    $i = 0;
    foreach($files as $k => $f) {

      if ($f["lines"] < 60000) {
        $f["start"] = 0;
        $f["end"] = $f["lines"];
        $cores_data[$core][$f["start"]."_".$f["end"]."_".$f["id"]] = $f;
      }
      else {
        $core = 1;
        $start = 0;
        $end   = ceil($f["lines"]/$cores["cores"]);
        $limit = 60000;
        $cicles = ceil($end/$limit);
        $pull  = $f["lines"];

        for ($cicle = 0; $cicle < $cicles; $cicle++) {
          for ($j = 1; $j <= $cores["cores"]; $j++) {

            $f["start"] = $start;
            $f["end"] = $limit;
            $cores_data[$j][$f["start"]."_".$f["end"]."_".$f["id"]] = $f;


            $start = $start + $limit;

            $core = $j;
          }
        }

      }

      $core++;

      if ($core > $cores["cores"])
            $core = 1;
    }

    $cores["data"] = json_encode(array("cores" => $cores_data));


		$Sql->update("cores", array($cores), "id");

    /*
		$is_indexed = $Sql->select("SELECT  count(id) as cnt FROM xml_files WHERE file_busy = 0 OR file_busy = 1  ORDER BY file_busy ASC, file_size ASC");
echo $is_indexed[0]["cnt"]. "<br>";
		$items_per_core = ceil($is_indexed[0]["cnt"]/$cores["cores"]);
		echo $items_per_core. "<br>";

		if ($items_per_core > 25000)
			$items_per_core = 25000;

			$pool = $is_indexed[0]["cnt"];
		$offset = 0;
		$limit = $items_per_core;
		for($i = 1; $i <= $cores["cores"]; $i++) {


			if ($pool < $items_per_core) $limit = $pool;

			$core_value[$i] = $offset . ", " . $limit;

			$offset+= $limit;
			$pool = $pool - $items_per_core;
		}
		$cores["data"] =json_encode(array("cores" => $core_value));

		$Sql->update("cores", array($cores), "id");
		echo $items_per_core;
    */

	}
}

?>
