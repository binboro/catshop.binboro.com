<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

class SizeCron extends Model
{

	/**
	* Fills size/product_size table and size_id field in products table
	*
	* @return bool
	*/
	public function fill_size_table() {
		// New type of size saving

		$products = (new Query())->select(['id', 'size', 'size_id'])->from('products')->where(['size_id' => 0])->limit(100000)->all();

		if (empty($products)) {
			return false;
		}

		$tmp_size = array();
		foreach ($products as $p) {
			$tmp_size[$p['size']] = $p;
		}
		$size = $this->getNewSizeByName($tmp_size);
		$db_ins = array();


		foreach ($size as $s)
			if (empty($s['size_id']))
				$db_ins[][] = $s['size'];


		Yii::$app->db->createCommand()->batchInsert('size', ['size'], $db_ins)->execute();

		//Bound product table with size table and insert data to relation table product_size
		$size = $this->getNewSizeByName($tmp_size);
		$db_ins = array();
		$db_upd = array();
		foreach ($products as $p) {
			if (isset($size[$p['size']])) {
				$db_ins[] = array(
					'product_id' => $p['id'],
					'size_id' => $size[$p['size']]['size_id'],
				);
				$db_upd[$size[$p['size']]['size_id']][] = $p['id'];
			}
		}


		Yii::$app->db->createCommand()->batchInsert('product_size', ['product_id', 'size_id'], $db_ins)->execute();
		foreach ($db_upd as $s_id => $pr_ids) {
			echo "<br>".$s_id ."--" . print_r($pr_ids,1);
			Yii::$app->db->createCommand()->update('products', ['size_id' => $s_id], ['id' => $pr_ids])->execute();
		}

		echo count($products);
		exit();
	}

	/**
	* Select size by names
	*
	* @param array $names Size names
	*
	* @return array
	*/
	private function getNewSizeByName($names) {
		$return = $names;

		$query = (new Query())->select('*')->from('size')->where(['in', 'size', $return])->all();
		if (!empty($query)) {
			foreach ($query as $q) {
				$return[$q['size']]['size_id'] = $q['id'];
			}
		}

		return $return;
	}
}
