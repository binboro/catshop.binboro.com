<?php



namespace app\models;

use Yii;


use app\components\Slug;
use app\components\Sql;
use app\components\Log;

use app\models\Products;

/*
use app\models\index_products\IndexColors;
use app\models\index_products\IndexBrands;
use app\models\index_products\IndexGenders;
use app\models\index_products\IndexSizes;
use app\models\index_products\IndexPrice;
use app\models\index_products\IndexCategories;
use app\models\index_products\IndexCategoriesGender;
use app\models\index_products\IndexBrandsGender;
use app\models\index_products\IndexProductSlug;

*/

use app\models\index_products2\IndexColors;
use app\models\index_products2\IndexGenders;
use app\models\index_products2\IndexSizes;
use app\models\index_products2\IndexBrands;
use app\models\index_products2\IndexPrice;
use app\models\index_products2\IndexCategories;
use app\models\index_products2\IndexCategoriesGender;
use app\models\index_products2\IndexBrandsGender;
use app\models\index_products2\IndexProductSlug;


/*

use app\models\index_products2\IndexBrandsGender;
use app\models\index_products2\IndexProductSlug;
*/

class ProductsCron
{
	public $get_products_limit = 5000;
	public $gender = [
		0 => 'undefined',
		1 => 'Unisex',
		2 => 'Women',
		3 => 'Men',
		4 => 'Boys',
	];
	public $designers_cats = false;

	public function is_index_allowed() {
		$Sql = new Sql();
		$Sql->set_limit(0,1);
		$q = $Sql->sl("products", "parent_id=-1");

		if (empty($q))
			return true;
		else
			return false;
	}
	public function indexProduct($cicle) {

		if ($this->is_index_allowed() === false)
			return false;

		$Sql 			= new Sql(); 	$Sql->enable_log = true;
		$lg 			= new Log(true);
		$Product  = new Products();

		$IndexGenders = new IndexGenders();
		$IndexColors 	= new IndexColors();
		$IndexSizes 	= new IndexSizes();
		$IndexBrands 	= new IndexBrands();
		$IndexPrices 	= new IndexPrice();
		$IndexCategories = new IndexCategories();
		$IndexCategoriesGender 	= new IndexCategoriesGender();
		$IndexBrandsGender 			= new IndexBrandsGender();
		$IndexProductSlug 			= new IndexProductSlug();

		$core = $this->get_core();
		$offset = 200000 * $cicle;

		$limit = " limit " . $offset . ",200000 ";


		$sql = "
			SELECT id, Gender, Color, name, brand, price_retail, price_sale, parent_id,
			URL_productImage, category_primary, manufacturer_name, Age, Size, Product_Type, category_secondary, upc
			FROM products WHERE is_indexed<1 ORDER BY id ASC  " . $limit;
		$products = $Sql->select($sql);
		$products_cnt = count($products);

		if ($products_cnt == 0)
			return false;

		$ids = array();
		for ($i = 0; $i < $products_cnt; $i++) {
			$id = $products[$i]["id"];
			$ids[$id] = $id;
		}

		$Sql->updateIn("products", array("is_indexed" => 1), array("id" => $ids));


		for ($i = 0; $i < $products_cnt; $i++) {

			$product = &$products[$i];

			$Product->set_product($product);
			$sku = $Product->get_parent_id();
			$id  = $Product->get_parent_id();


			$color = $Product->get_color();

			if (!empty($color)) {
				foreach ($color as $c) {

						if (empty($c))
							continue;

						$IndexColors->add(trim($c), $id);
				}

			}

			$gender = $Product->get_gender();
			if (!empty($gender))
				$IndexGenders->add($gender, $id);

			$size = $Product->get_size();

			if (!empty($size)) {
				foreach ($size as $s) {
						$stop = false;

						if (!empty($color)) {
							foreach ($color as $c) {
								if ($c == $s) {
									$stop = true;
									break;
								}
							}
						}

						if (empty(trim($s)) OR $stop == true)
							continue;

						if ($s == " ")
							continue;

						$IndexSizes->add(trim($s), $id);
				}

			}


			$brand = $Product->get_brandName();
			if (!empty($brand))
				$IndexBrands->add($brand, $sku);


			$price_retail = $Product->get_retail_price();
			if (!empty($price_retail))
				$IndexPrices->add($price_retail, $id, "retail");

			$price_sale = $Product->get_sale_price();
			if (!empty($price_sale))
				$IndexPrices->add($price_sale, $id, "sale");

			$categories = array();
			$categories[] 	= $Product->get_product_type();
			$categories[] 	= $Product->get_cat_primary();
			$categories[] 	= $Product->get_cat_secondary();


			if (!empty($categories)) {

				foreach ($categories as $category) {

					if (empty($category))
						continue;

					if (is_array($category))
						foreach($category as $cat)
							$IndexCategories->add(trim($cat), $id);
					else
						$IndexCategories->add(trim($category), $id);
				}
			}




			$IndexCategoriesGender->add($id);
			$IndexBrandsGender->add($id);
			$IndexProductSlug->add($Product->get_image(), $Product->get_name(), $id);



		}

		$lg->show("indexProduct loop E");
		/**/

		$start_time2 = microtime(true);
		$start_time3 = microtime(true);
		$IndexColors->index();
		$lg->show("IndexColors->index");


		$start_time3 = microtime(true);
		$IndexGenders->index();
		$lg->show("IndexGenders->index");


		$start_time3 = microtime(true);
		$IndexSizes->index();
		$lg->show("IndexSizes->index");

		$start_time3 = microtime(true);
		$IndexBrands->index();
		$lg->show("IndexBrands->index");

		$start_time3 = microtime(true);
		$IndexPrices->index();
		$lg->show("IndexPrices->index");

		$start_time3 = microtime(true);
		$IndexCategories->index();
		$lg->show("IndexCategories->index");

		$start_time3 = microtime(true);
		$IndexCategoriesGender->index();
		$lg->show("IndexCategories->index");

		$start_time3 = microtime(true);
		$IndexBrandsGender->index();
		$lg->show("IndexBrandsGender->index");

		$start_time3 = microtime(true);
		$IndexProductSlug->index();
		$lg->show("IndexProductSlug->index");

		$lg->show("Indexes Done");


		if (!empty($ids))
			$Sql->updateIn("products", array("is_indexed" => 2), array("id" => $ids));

		$lg->show("Done");

		return true;
	}

	public function get_core() {
		if (empty($_REQUEST["core"]))
			return 1;

		return $_REQUEST["core"];
	}


	public function indexVisibility() {
		$Sql = new Sql();
		$Sql->enable_log=true;
		$lg  = new Log(true);

		//$Products = new Products();

		//$Products->get_related_categories(false);


		$cats = $Sql->select("
		SELECT
			main_category, related
		FROM relations_categories

		");

		$active_cats = array();
		foreach ($cats as $cat)  {
			$active_cats[ $cat["main_category"] ] = $cat["main_category"];
			$active_cats[ $cat["related"] ] 			= $cat["related"];
		}


		$cats = $Sql->select("
		SELECT
			category_id
		FROM categories_hierarchy

		");


		foreach ($cats as $cat)
			$active_cats[ $cat["category_id"] ] = $cat["category_id"];





/*
		$products = $Sql->select("
			 SELECT s.product_id  FROM product_slug as s

			 WHERE s.is_visible=-1 LIMIT 20000");

		foreach ($products as $r)
			$products_by_id[$r["product_id"]] = $r;
*/

		$products = array();

		$cats = $Sql->select(" SELECT *  FROM product_categories");
		foreach ($cats as $r) {


			$product =  array(
				"product_id" 	=> $r["product_id"],
				"category_id" => $r["category_id"]);

			$products[] = $product;
		}


		$lg->show("Loop S");
		$cnt = count($products);

		$visible_slugs = array();
		$InVisible_slugs = array();
		$lg->show("Loop E" . $cnt);
		foreach ($products as $product)
			if (!empty($active_cats[ $product["category_id"] ]))
				$visible_slugs[ $product["product_id"] ] = $product["product_id"];

		foreach ($products as $product)
			if (empty($active_cats[ $product["category_id"] ]))
				if (empty($visible_slugs[ $product["product_id"] ]))
					$InVisible_slugs[ $product["product_id"] ] = $product["product_id"];




		$lg->show("Loop E");

		$Sql->updateIn("product_slug", array("is_visible" => 1), array("product_id" => $visible_slugs));
		$Sql->updateIn("product_slug", array("is_visible" => 0), array("product_id" => $InVisible_slugs));

		$lg->show("Updates, visisble: " . count($visible_slugs) . " inVisible: " . count($InVisible_slugs));

		return false;

	}

/* Somecing OLD */






	public function getInvisibleProduct() {
		$Sql = new Sql();

		$d= $Sql->select("SELECT * FROM product_slug
				LEFT JOIN product_categories ON product_categories.product_id = product_slug.product_id
				LEFT JOIN categories ON product_categories.category_id = categories.id
				WHERE product_slug.is_visible=0 AND product_categories.category_id=4916
				LIMIT 0,500
		");


		foreach ($d as $v) {

			echo "<br><br>";
			var_dump($v);
		}


	}
}
