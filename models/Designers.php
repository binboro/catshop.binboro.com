<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\components\Slug;
use app\components\Sql;
use app\models\Categories;

class Designers extends Model
{

	public $gender = [
		0 => 'undefined',
		1 => 'Unisex',
		2 => 'women',
		3 => 'men',
		4 => 'Boys',
	];
	public $designers_cats = false;


	public function get_designers_for_filters() {

		/*
			ATTENTIONS
			IT LOOOKS LIKE FUNCTION NOT IN USE, THERE IS get_designers_for_filter
		*/
		$gender = $_REQUEST['gender'];
		$category_id = $_REQUEST["category_id"];

		$gender = $_REQUEST['gender'];
		$data = (new Query())->select('*,count(product_brand.product_id) as cnt')->from('brands')
		->leftJoin("product_brand", "product_brand.brand_id=brands.id")
		->leftJoin("product_categories", "product_categories.product_id=product_brand.product_id ")
		->leftJoin("product_gender", "product_gender.product_id=product_brand.product_id ")
		->where(["=", "product_categories.category_id", $category_id])
		->andWhere(["=", "product_gender.gender_id", $gender])
		->andWhere(["=", "brands.is_visible", 1])
		->groupBy(["brands.id"])
		->orderBy(["count(product_brand.product_id)" =>SORT_DESC])
		->limit(20);


		$q_brandID= $data->all();


		return $q_brandID;
	}


	// Not sure if it is in use, but it is done wrong
	public function count_products_in_designers() {



		//Yii::$app->db->createCommand("UPDATE brands SET men_count=(SELECT count(*) FROM product_brand WHERE product_brand.brand_id=brands.id AND 3=(SELECT gender_id FROM product_gender WHERE product_gender.product_id=product_brand.product_id) )")->execute();
		$data = (new Query())->select('*')->from('brands')->where(["=", "brand", "Fendi"])->limit(2000);
		$data = $data->all();

		foreach ($data as $r) {

				// Men
				$q = (new Query())->select('count(products.id) as cnt')->from('products')->
				leftJoin("product_gender", "product_gender.product_id=products.product_id ")->where(["=", "products.brand_id", $r["id"]])->andWhere(["=", "product_gender.gender_id", 3])->all();


			 // Women
			 $q = (new Query())->select('count(products.id) as cnt')->from('products')->
			 leftJoin("product_gender", "product_gender.product_id=products.product_id ")->where(["=", "products.brand_id", $r["id"]])->andWhere(["=", "product_gender.gender_id", 2])->all();

			 echo $r['brand'];
			 echo $q[0]["cnt"];
			 echo "<br>";

		}

		echo count($data);
	}

	public function designers_list($gender = 0) {
		$data = (new Query())->select('brands.id, brands.brand, brands.slug')->from('brands')
			->leftJoin('product_brand pb', 'pb.brand_id = brands.id')
			->leftJoin('product_gender pg', 'pg.product_id = pb.product_id')
			->where(['=', 'pg.gender_id', $gender])
			->andWhere(['=', 'brands.is_visible', 1])

			->groupBy('brands.id')
			->all();
		return $data;
	}



	public function get_all_designers_in_alphabet() {
		$Sql = new Sql();
		$Sql->enable_log = false;
		$leftJoin = '';
		$where = '';
		$whereArr = array();

		if (!empty($_REQUEST['category_id']) && $_REQUEST['category_id'] != 'all') {

			$leftJoin .= "
				LEFT JOIN brands_categories ON brands_categories.brand_id=brands.id
			";

			$where .= " AND brands_categories.category_id=:cat_id AND brands_categories.brand_id IS NOT NULL";
			$whereArr["cat_id"] = $_REQUEST["category_id"];
		}

		if (!empty($_REQUEST['search'])) {
			$where .= " AND brands.brand like :search ";
			$whereArr["search"] = '%'.$_REQUEST["search"].'%';
		}

		if (empty($whereArr)) $whereArr = false;


			$data = $Sql->select("
				SELECT brands.id, brands.brand, brands.slug, brands_gender.gender_id as gender_id FROM brands
				LEFT JOIN brands_gender ON brands_gender.brand_id=brands.id
				" . $leftJoin . "
				WHERE brands.is_visible= 1 AND brands_gender.gender_id IS NOT NULL
				" . $where . "
				ORDER by brands.brand ASC
			", $whereArr);

			return $data;

	}

	public function get_designers_for_filter2($params = array()) {
		$Sql = new Sql();
		$Sql->enable_log = true;


		$gender_id = $params['gender_id'][0];

		$q = $Sql->select(
		"SELECT * FROM brands
		LEFT JOIN brands_gender ON brands_gender.brand_id = brands.id
		WHERE brands_gender.gender_id=:gender_id
		ORDER BY brands.brand ASC

		", array("gender_id" => $gender_id)
		);
		/*
		$Categories = new Categories();

		$query = (new Query())->select(['b.*', 'pb.brand_id'])->from('brands b')
			->leftJoin('product_brand pb', 'pb.brand_id = b.id')
			->leftJoin('product_categories pc', 'pc.product_id = pb.product_id')
			->leftJoin('product_gender pg', 'pg.product_id = pb.product_id');

		//Joins
		if (!empty($params['color']))
			$query->leftJoin('product_colors pcolor', 'pcolor.product_id = pb.product_id');
		if (!empty($params['size']))
			$query->leftJoin('product_size ps', 'ps.product_id = pb.product_id');

		//Conditions
		$params['gender_id'][] = 3;
		$query->where([ 'pg.gender_id' => $params['gender_id'] ]);
		if (!empty($params['sub_category']))
			$query->andWhere(['pc.category_id' => $params['sub_category']]);
		elseif (!empty($params['category_id'])) {

			if (!is_array($params["category_id"])) {
				$childs = $Categories->get_childs($params["category_id"]);
				$params['category_id'] = array_merge(array($params['category_id']), $childs);
			}


			$query->andWhere(['pc.category_id' => $params['category_id'] ]);
		}
		if (!empty($params['color']))
			$query->andWhere(['pcolor.color_id' => $params['color']]);
		if (!empty($params['size']))
			$query->andWhere(['ps.size_id' => $params['size']]);

		$query->groupBy(['b.id'])
			->orderBy(['b.brand' => SORT_DESC]);

		$data = $query->all();
		*/
		exit();
		return $data;
	}


	public function get_designers_for_filter($params = array()) {

		$Categories = new Categories();

		$query = (new Query())->select(['b.*', 'pb.brand_id',  'count(pb.product_id) as cnt'])->from('brands b')
			->leftJoin('product_brand pb', 'pb.brand_id = b.id')
			->leftJoin('product_categories pc', 'pc.product_id = pb.product_id')
			->leftJoin('product_gender pg', 'pg.product_id = pb.product_id');

		//Joins
		if (!empty($params['color']))
			$query->leftJoin('product_colors pcolor', 'pcolor.product_id = pb.product_id');
		if (!empty($params['size']))
			$query->leftJoin('product_size ps', 'ps.product_id = pb.product_id');

		//Conditions
		$params['gender_id'][] = 3;
		$query->where([ 'pg.gender_id' => $params['gender_id'] ]);
		if (!empty($params['sub_category']))
			$query->andWhere(['pc.category_id' => $params['sub_category']]);
		elseif (!empty($params['category_id'])) {

			if (!is_array($params["category_id"])) {
				$childs = $Categories->get_childs($params["category_id"]);
				$params['category_id'] = array_merge(array($params['category_id']), $childs);
			}


			$query->andWhere(['pc.category_id' => $params['category_id'] ]);
		}
		if (!empty($params['color']))
			$query->andWhere(['pcolor.color_id' => $params['color']]);
		if (!empty($params['size']))
			$query->andWhere(['ps.size_id' => $params['size']]);

		$query->andWhere(['b.is_visible' => 1]);

		$query->groupBy(['b.id'])
			->orderBy(['cnt' => SORT_DESC])
			->limit(20);

		$data = $query->all();

		return $data;
	}

	public function get_all($fields = '*') {
		return (new Query())->select($fields)->from('brands')->all();
	}

	public function get_by_id($id = 0) {
		return (new Query())->select('*')->from('brands')->where(['id' => $id])->all();
	}

}
