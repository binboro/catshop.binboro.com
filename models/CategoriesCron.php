<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

class CategoriesCron extends Model
{
    /**
	* Fills product_id field in product_categories table
	*/
	public function fill_product_id() {
        $products = (new Query())->select(['id', 'product_id'])->from('products')->where(['product_id_id' => 0])->limit(100000)->all();

        $db_upd = array();
        foreach ($products as $p) {
            $db_upd[$p['product_id']][] = $p['id'];
        }

        // Update product_id_id field in products/product_categories table
        foreach ($db_upd as $sku => $p_ids) {
            Yii::$app->db->createCommand()->update('products', ['product_id_id' => $sku], ['id' => $p_ids])->execute();
            Yii::$app->db->createCommand()->update('product_categories', ['product_id_id' => $sku], ['product_id' => $p_ids])->execute();
        }

        echo count($products);
        exit();
    }
}
