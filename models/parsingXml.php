<?php

namespace app\models;

use yii\db\Query;
use Yii;
use app\components\Slug;

class parsingXml {
  public $offset = 0;
  public $limit = 100;

  public $products = [];
  public $categories = [];
  public $colors = [];
  public $gender = [];

  public function startParse($file) {
      $path = dirname(__DIR__) . '/xml/';
      $xml = simplexml_load_file($path . '/' . $file);
      if ($xml === FALSE) {
        return false;
      }
      $this->colors = [];


      $productsNum = count($xml->product);
      $xml = (array)$xml; //json_decode(json_encode($xml), true);

      if(!empty($xml['header'])) {
        $headers = json_decode(json_encode($xml['header']), true);
        $merchantId = !empty($headers['merchantId']) ? $headers['merchantId'] : '';
        $merchantName = !empty($headers['merchantName']) ? $headers['merchantName'] : '';
      }


      if(!$this->checkFileExsistDB($file))
        $this->insertToFilesTable($file, $productsNum);

      if(empty($productsNum)) {
        return;
      }

      if($productsNum == 1) {
        $xml['product'] = [$xml['product']];
      }


      $this->selectColors();
      $this->selectGender();
      $elementsLeft = $productsNum;


      //insert all categories and colors
      $colorsToInsert = [];
      for($i = $this->offset; $i < $productsNum; $i++) {
        $prod = json_decode(json_encode($xml['product'][$i]), true);
        $prodType = false;
        if(!empty($prod['attributeClass']) && !empty($prod['attributeClass']['Product_Type'])){
          $prodType = $prod['attributeClass']['Product_Type'];
        }

        if(!empty($prod['attributeClass']) && !empty($prod['attributeClass']['Color'])) {
          $color = $prod['attributeClass']['Color'];
          if(!in_array($color, $this->colors) && !in_array($color, $colorsToInsert)){
            $colorsToInsert[] = $prod['attributeClass']['Color'];
          }
        }

        if(!in_array($prodType, $this->categories)) {
          $this->categories[] = $prodType;
        }

        $this->getCategories($prod);
      }
      $this->insertCategories();
      $this->insertColors($colorsToInsert);


      for( $i = $this->offset; $i < $productsNum; $i++ ) {
        $prod = json_decode(json_encode($xml['product'][$i]), true);

        if($i > 0 && $i % $this->limit == 0) {
            $this->insertData();
            $this->updateFilesTable($file, count($this->products), ($this->limit - count($this->products) ));
            $this->clearProducts();
            $elementsLeft -= 100;
        }

        $this->setItemParams($i, $prod); //$xml['product'][$i]
        $this->products[$i]['merchantId'] = $merchantId;
        $this->products[$i]['filename'] = $file;
        $this->products[$i]['merchantName'] = $merchantName;
      }

      $this->insertData();
      $this->updateFilesTable( $file, count($this->products), ($elementsLeft - count($this->products)) );
      $this->clearProducts();
  }


  public function selectGender() {
    $data = (new Query())->select('*')->from('gender')->all();

    foreach ($data as $value) {
      $this->gender[$value['gender']] = $value['id'];
    }
  }



  public function selectColors($limit = false) {
    $data = (new Query())->select('*')->from('colors');

    if($limit) {
      $data = $data->limit($limit);
    }

    $data = $data->orderBy(['id' => SORT_DESC])->all();

    foreach ($data as $value) {
      $this->colors[$value['id']] = $value['color'];
    }
  }


  public function insertCategories() {
    $slugify = new Slug();
    list($categoryBySlug, $slugs, $categs) = $this->getAllCategories();

    $intersect = array_intersect($this->categories, $categoryBySlug);
    $diff = array_diff($this->categories, $intersect);

    $toInsert = [];
    foreach ($diff as $key => $value) {
      //if($value == 0) { continue; }
      $slug =  $slugify->genereteSlug($value, [], $slugs);
      $slugs[] = $slug;
      $toInsert[] = [
        'category_name' => $value,
        'slug' => $slug,
      ];
    }

    if(count($toInsert))
      Yii::$app->db->createCommand()->batchInsert('categories', ['category_name', 'slug'], $toInsert)->execute();


    $this->insertHierarhy();

    return;
  }

  public function insertHierarhy() {
    list($categoryBySlug, $slugs, $categs) = $this->getAllCategories();

    $ids = array_keys($categs);
    $exsistIDs = [];
    $exsisting = (new Query)->select('category')->from('categories_hierarchy')->where(['in', 'category', $ids])->all();
    foreach ($exsisting as $key => $value) {
      $exsistIDs[] = $value['category'];
    }

    $intersect = array_intersect($ids, $exsistIDs);
    $diff = array_diff($ids, $intersect);

    $toInsert = [];
    foreach ($diff as $key => $value) {
      if(in_array($value, [1, 2, 0])){ continue; }
      $toInsert[] = [
        'category' => $value,
        'parent_category' => 0,
        'gender' => 0,
        'order' => 1,
        'show' => 0,
      ];
    }

    if(count($toInsert))
      Yii::$app->db->createCommand()->batchInsert('categories_hierarchy', ['category', 'parent_category', 'gender', 'order', 'show'], $toInsert)->execute();

    $this->categories = $categs;

    return;
  }

  public function getAllCategories() {
    $categories = (new Query)->select('*')->from('categories')->all();
    $categoryesBySlug = [];
    $slugs = [];
    $categs = [];
    foreach ($categories as $key => $value) {
      $categoryesBySlug[$value['slug']] = $value['category_name'];
      $slugs[] = $value['slug'];
      $categs[] = $value['category_name'];
    }

    return [$categoryesBySlug, $slugs, $categs];
  }


  public function getLastInsertedProducts($limit) {
    $products = (new Query)
              ->select('id, product_type, color, price_retail, gender')
              ->from('products')
              ->limit($limit)
              ->orderBy(['id' => SORT_DESC])
              ->all();

    $productsIds = [];
    $ids = [];
    $colors = [];
    $prices = [];
    $gender = [];

    foreach ($products as $key => $value) {
      $productsIds[$value['id']] = $value['product_type'];
      $ids[] = $value['id'];
      $colors[$value['id']] = $value['color'];
      $prices[$value['id']] = $value['price_retail'];
      $gender[$value['id']] = $value['gender'];
    }

    return [$productsIds, $ids, $colors, $prices, $gender];
  }

  public function generateCategoriesRelations($productsIds, $ids) {

    $product_categories = (new Query)
      ->select('*')
      ->from('product_categories')
      ->where(['in', 'product_id', $ids])
      ->all();
      foreach ($product_categories as $key => $value) {
        if(!empty($productsIds[$value['product_id']]) && $productsIds[$value['product_id']] == $value['categories_id']) {
          unset($productsIds[$value['product_id']]);
        }
      }

      $toInsert = [];
      foreach ($productsIds as $prod_id => $category_id) {
        $toInsert[] = [
          'product_id' => $prod_id,
          'categories_id' => $category_id,
        ];
      }

      if(!empty($toInsert)) {
        Yii::$app->db->createCommand()->batchInsert('product_categories', ['product_id', 'categories_id'], $toInsert)->execute();
      }
  }

  public function insertColors($colors) {
    $toInsert = [];
    foreach ($colors as $color) {
      $toInsert[] = [$color];
    }

    if(!empty($toInsert)) {
      Yii::$app->db->createCommand()->batchInsert('colors', ['color'], $toInsert)->execute();
      $this->selectColors(count($toInsert));
    }

  }


  public function generateColorRelations($colors, $ids) {
    $data = (new Query())->select('*')->from('product_colors')->where(['in', 'product_id', $ids])->all();
    $exsistIDs = [];
    foreach ($data as $key => $value) {
      $exsistIDs[] = $value['product_id'];
    }

    $diff = array_diff($ids, $exsistIDs);

    $toInsert = [];
    $colorsToInsert = [];
    $notInsert = [];
    foreach ($diff as $id) {
      if(in_array($colors[$id], $this->colors)) {
        $toInsert[$id] = [array_search($colors[$id], $this->colors), $id];
      } else {
        $colorsToInsert[] = [ $colors[$id] ];
        $notInsert[] = $id;
      }
    }

    if(!empty($colorsToInsert)) {
      Yii::$app->db->createCommand()->batchInsert('colors', ['color'], $colorsToInsert)->execute();
      $this->selectColors(count($colorsToInsert));
      foreach ($notInsert as $id) {
        if(in_array($colors[$id], $this->colors)) {
          $toInsert[$id] = [array_search($colors[$id], $this->colors), $id];
        }
      }
    }

    if(!empty($toInsert)) {
      Yii::$app->db->createCommand()->batchInsert('product_colors', ['color_id', 'product_id'], $toInsert)->execute();
    }
  }

  public function generatePriseRelations($prices, $ids)
  {
    $data = (new Query())->select('*')->from('product_price')->where(['in', 'product_id', $ids])->all();
    $exsistIDs = [];
    $exsistPrices = [];
    foreach ($data as $value) {
      $exsistIDs[] = $value['product_id'];
      $exsistPrices[$value['product_id']] = $value['product_price'];
    }


    $diff      = array_diff($ids, $exsistIDs);
    $intersect = array_intersect($ids, $exsistIDs);

    $toInsert = [];
    foreach ($diff as $id) {
      $toInsert[] = [ $id, $prices[$id] ];
    }

    if(!empty($toInsert)) {
      Yii::$app->db->createCommand()->batchInsert('product_price', ['product_id', 'product_price'], $toInsert)->execute();
    }

    foreach ($intersect as $id) {
      if($prices[$id] != $exsistPrices[$id]) {
        Yii::$app->db->createCommand()
           ->update(
              'product_price', [ 'product_price' => $prices[$id] ],
              'product_id = :product_id', ['product_id' => $id]
            )->execute();
      }
    }

  }

  public function generateGenderRelations($gender, $ids)
  {
    $data = (new Query())->select('*')->from('product_gender')->where(['in', 'product_id', $ids])->all();
    $exsistIDs = [];
    $exsistGender = [];
    foreach ($data as $value) {
      $exsistIDs[] = $value['product_id'];
      $exsistGender[$value['product_id']] = $value['gender_id'];
    }


    $diff      = array_diff($ids, $exsistIDs);
    $intersect = array_intersect($ids, $exsistIDs);

    $toInsert = [];
    foreach ($diff as $id) {
      if(!empty($this->gender[$gender[$id]]))
        $toInsert[] = [ $id, $this->gender[$gender[$id]] ];
      else
        $toInsert[] = [ $id, 0];
    }

    if(!empty($toInsert)) {
      Yii::$app->db->createCommand()->batchInsert('product_gender', ['product_id', 'gender_id'], $toInsert)->execute();
    }

    foreach ($intersect as $id) {
      if($gender[$id] != $exsistGender[$id]) {
        $genderID = !empty($this->gender[$gender[$id]]) ? $this->gender[$gender[$id]] : 0;
        Yii::$app->db->createCommand()
           ->update(
              'product_gender', [ 'gender_id' => $genderID ],
              'product_id = :product_id', ['product_id' => $id]
            )->execute();
      }
    }

  }

  public function createCategoryProductRelations($limit) {

    list($productsIds, $ids, $colors, $prices, $gender) = $this->getLastInsertedProducts($limit);

    $this->generateCategoriesRelations($productsIds, $ids);
    $this->generateColorRelations($colors, $ids);
    $this->generatePriseRelations($prices, $ids);
    $this->generateGenderRelations($gender, $ids);

  }


  public function updateFilesTable($file, $addNumber, $countChange) {
    $this->createCategoryProductRelations($addNumber);

    $item_processed = (new Query)
              ->select('item_processed, item_count')
              ->from('uploaded_xml_files')
              ->where(['file_name' => $file])
              ->one();

    return Yii::$app->db->createCommand()
            ->update('uploaded_xml_files',
                  [
                    'item_processed' => ($item_processed['item_processed'] + $addNumber),
                    'item_count' => ($item_processed['item_count'] - $countChange),
                  ],
                  'file_name = :file_name',
                  ['file_name' => $file]
                )->execute();
  }

  public function checkFileExsistDB($fileName) {
    return (new Query)->select('file_name')->from('uploaded_xml_files')->where(['file_name' => $fileName])->one();
  }

  public function insertToFilesTable($fileName, $products) {
    $del = Yii::$app->db->createCommand()->delete('uploaded_xml_files', "file_name=:file_name", ['file_name' => $fileName])->execute();
    return Yii::$app->db->createCommand()->insert('uploaded_xml_files', [
        'file_name' => $fileName,
        'item_processed' => 0,
        'item_count' => $products,
        'create_time' => time(),
      ])->execute();
  }

  public function insertData() {
    if(count($this->products) == 0)
      return false;


    $toUpdate = $this->checkExsist();
    foreach ($toUpdate as $key => $values) {
      Yii::$app->db->createCommand()->update('products', $values, 'product_id = ' . $key)->execute();
    }

    $this->generateProductSlug();


    if(count($this->products)) {
      $fields = array_keys(current($this->products));

      foreach ($this->products as $key => $value) {
        foreach ($fields as $k => $field) {
          if(empty($value[$field])) {
            $this->products[$key][$field] = '';
          }
        }
      }

      Yii::$app->db->createCommand()->batchInsert('products', $fields, $this->products)->execute();
    }
  }

  public function generateProductSlug() {
    $slugs = [];
    $slugify = new Slug();

    $newSlugs = [];
    foreach ($this->products as $key => $value) {
      $newSlugs[$key] = $slugify->genereteSlug($value['name'], [], $slugs);
      $slugs[] = $newSlugs[$key];
    }

    $found = $this->getBySlugs($newSlugs);
    $intersect = array_intersect($found, $newSlugs);
    foreach ($intersect as $key => $value) {
      $key = array_search($value, $newSlugs);
      $newSlugs[$key] = $slugify->addNumber($value);
    }


    foreach ($this->products as $key => $value) {
      $this->products[$key]['slug'] = $newSlugs[$key];
    }


    return;

  }

  public function getBySlugs($newSlugs) {
    $slugs = (new Query)->select('slug')->from('products')->where(['in', 'slug', $newSlugs])->all();

    $toReturn = [];
    foreach ($slugs as $key => $value) {
      $toReturn[] = $value['slug'];
    }

    return $toReturn;
  }


  public function checkExsist() {
    $prodId_key = [];
    $prodIds = [];
    $hash = [];
    foreach ($this->products as $key => $value) {
      if(empty($value['product_id'])) {
        unset($this->products[$key]);
        continue;
      }

      $prodId_key[$value['product_id']] = $key;
      $prodIds[$value['product_id']] = $value['product_id'];
      $hash[$value['product_id']] = $value['hash'];
    }

    //check to update
    $hash_data = $this->getDataByHash($hash);
    foreach ($hash_data as $key => $value) {
      if(!empty($prodId_key[$value['product_id']])) {
        unset($this->products[$prodId_key[$value['product_id']]]);
        unset($prodIds[$value['product_id']]);
      }
    }


    //chech exsist products
    $prodID_data = $this->getDataByProdId($prodIds);
    $founded_prodIDs = [];
    foreach ($prodID_data as $value) {
      $founded_prodIDs[$value['id']] = $value['product_id'];
    }

    //diff - products not exsist in database
    $diff = array_diff($prodIds, $founded_prodIDs);

    $toUpdate = [];
    foreach ($prodIds as $key => $value) {
      //generate update list
      if(!in_array($value, $diff)) {
        $toUpdate[$value] = $this->products[$prodId_key[$value]];
        unset($this->products[$prodId_key[$value]]);
      }
    }

    return $toUpdate;
  }

  public function getDataByHash($hash) {
    return (new Query)->select('id, product_id')->from('products')->where(['in', 'hash', $hash])->all();
  }

  public function getDataByProdId($prodIds) {
    return (new Query)->select('id, product_id')->from('products')->where(['in', 'product_id', $prodIds])->all();
  }


  public function clearProducts() {
    $this->products = [];
  }


  public function setItemParams($key, $product) {
    if(empty($product['attributeClass']))
      return false;
    if(empty($product['@attributes']))
      return false;
    if(empty($product['URL']))
      return false;

    $pass = $this->setAttributes($product['attributeClass'], $key);
    if(empty($pass)) {
      unset($this->products[$key]);
      return false;
    }

    $pass = $this->setProdAttributes($product['@attributes'], $key);
    if(empty($pass)) {
      unset($this->products[$key]);
      return false;
    }


    $this->setUrls($product['URL'], $key);
    $this->setDescription($product['description'], $key);
    $this->setDiscount($product['discount'], $key);
    $this->setModification($product, $key);
    $this->setShipping($product['shipping'], $key);
    $this->setPrice($product['price'], $key);

    $this->products[$key]['brand'] = (isset($product['brand'])) ? $product['brand'] : '';
    $this->products[$key]['upc'] = isset($product['upc']) ? $product['upc'] : '';
    $this->products[$key]['pixel'] = isset($product['pixel']) ? $product['pixel'] : '';
    $this->products[$key]['keywords'] = isset($product['keywords']) ? $product['keywords'] : '';
    $this->products[$key]['hash'] = md5(json_encode($this->products[$key]));
    $this->products[$key]['date'] = time();

  }

  public function setModification($product, $key) {
    $this->products[$key]['modification'] =  isset($product['modification']) ? $product['modification'] : '';
  }

  public function setAttributes($product, $key) {
    $prodType = (isset($product['Product_Type'])) ? $product['Product_Type'] : '';
    $categoryID = array_search($prodType, $this->categories) + 1;

    $this->products[$key]['class_id'] = (isset($product['@attributes']['class_id'])) ? $product['@attributes']['class_id'] : '';
    $this->products[$key]['misc'] = (isset($product['Misc'])) ? $product['Misc'] : '';
    $this->products[$key]['product_type'] = $categoryID;
    $this->products[$key]['color'] = (isset($product['Color'])) ? $product['Color'] : '';
    $this->products[$key]['gender'] = (isset($product['Gender'])) ? $product['Gender'] : '';
    $this->products[$key]['age'] = (isset($product['Age'])) ? $product['Age'] : '';
    $this->products[$key]['size'] = (isset($product['Size'])) ? $product['Size'] : '';


    if(isset($product['Gender']) && $product['Gender'] == 'Baby') {
      return false;
    }

    if(isset($product['Age']) && $product['Age'] == 'Kids') {
      return false;
    }


    if(!empty($product['Product_Type']))
      $this->categories[$product['Product_Type']] = $product['Product_Type'];

    return true;
  }
  //
  // public function setPixel($product, $key) {
  //   $this->products[$key]['pixel'] = $product;
  // }
  //
  // public function setUpc($product, $key) {
  //   $this->products[$key]['upc'] = $product;
  // }
  //
  // public function setKeywords($product, $key) {
  //   $this->products[$key]['keywords'] = $product;
  // }


  public function setShipping($product, $key) {
    $this->products[$key]['cost_amount'] = (isset($product['cost']['amount'])) ? $product['cost']['amount'] : '';
    $this->products[$key]['cost_currency'] = (isset($product['cost']['currency'])) ? $product['cost']['currency'] : '';
    $this->products[$key]['availability'] = (isset($product['availability'])) ? $product['availability'] : '';
  }


  public function setPrice($product, $key) {
    $this->products[$key]['price_currency'] = (isset($product['@attributes']['currency'])) ? $product['@attributes']['currency'] : '';
    $this->products[$key]['price_retail'] = (isset($product['retail'])) ? $product['retail'] : '';
  }

  public function setDiscount($product, $key) {
    $this->products[$key]['discount_currency'] = (isset($product['@attributes']['currency'])) ? $product['@attributes']['currency'] : '';
    $this->products[$key]['discount_type'] = (isset($product['type'])) ? $product['type'] : '';
  }

  public function setDescription($product, $key) {
    $this->products[$key]['description_short'] = (isset($product['short'])) ? $product['short'] : '';
    $this->products[$key]['description_long'] = (isset($product['long'])) ? $product['long'] : '';
  }

  public function setUrls($product, $key) {
    $this->products[$key]['product_link'] = (isset($product['product'])) ? $product['product'] : '';
    $this->products[$key]['product_image'] = (isset($product['productImage'])) ? $product['productImage'] : '';
  }

  public function setProdAttributes($product, $key) {
    $prod_id = isset($product['product_id']) ? $product['product_id'] : 0;
    $slug = ''; //(new Slug())->genereteSlug($products[$i]['name'], ['categories' => 'slug', 'products' => 'slug'], $existing_slug);

    if(empty($prod_id)) {
      unset($this->products[$key]);
      return false;
    }

    $this->products[$key]['product_id'] = $prod_id;
    $this->products[$key]['name'] = (isset($product['name'])) ? $product['name'] : '';
    $this->products[$key]['slug'] = $slug;
    $this->products[$key]['sku_number'] = (isset($product['sku_number'])) ? $product['sku_number'] : '';
    $this->products[$key]['manufacturer_name'] = (isset($product['manufacturer_name'])) ? $product['manufacturer_name'] : '';
    $this->products[$key]['part_number'] = (isset($product['part_number'])) ? $product['part_number'] : '';
    $this->products_ids[$prod_id] = $prod_id;

    return true;
  }

  public function getCategories($product) {
    if(!empty($product['category']) && !empty($product['category']['primary'])) {
      if(!in_array($product['category']['primary'], $this->categories)) {
        $this->categories[] = $product['category']['primary'];
      }
    }

  }


  public function getNextFile() {
    $path = dirname(__DIR__) . '/xml';
    $scan = scandir($path);
    $scan = array_diff($scan, array('.', '..'));
    $scan_count = count($scan);

    if($scan_count == 0){
      return false;
    }

    foreach ($scan as $key => $value) {
      if(substr($value, -3) !== 'xml') {
        unset($scan[$key]);
      }
    }

    $continueFile = false;
    $file = $this->checkCompleteParsing();

    $filesNames = [];
    if(!empty($file['file_name']) && in_array($file['file_name'], $scan)) {
      $this->offset = $file['item_processed'];
      $continueFile = $file['file_name'];
    }

    $files = $this->getParsedFiles();
    if(!empty($files)) {
      foreach ($files as $key => $value) {
        $filesNames[] = $value['file_name'];
      }
    }

    $scan = array_diff($scan, $filesNames);

    if(count($scan)) {
      if(!empty($continueFile))
        array_unshift($scan, $continueFile);

      return $scan; //array_shift($scan);
    }
    else {
      return false;
    }

  }

  public function removeAllFilesData(){
    return Yii::$app->db->createCommand()->delete('uploaded_xml_files')->execute();
  }

  public function getParsedFiles() {
    return (new Query)->select('*')->from('uploaded_xml_files')->orderBy([ 'id' => SORT_DESC])->all();
  }

  public function checkCompleteParsing() {
    return Yii::$app->db->createCommand("SELECT * FROM `uploaded_xml_files` WHERE `item_count` <> `item_processed`")->queryOne();
  }


}



 ?>
