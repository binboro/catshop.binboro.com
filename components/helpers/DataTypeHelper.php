<?php
namespace app\components\helpers;

class DataTypeHelper
{
    /**
	* Scalar to array
    *
    * @param array $data
    * @param array $scalar_fields
    *
    * @return array
	*/
    public function scalar_to_array(array $params = array(), $scalar_fields) {
        if (empty($params) || empty($scalar_fields)) return $params;

        foreach ($scalar_fields as $sf) {
            if (isset($params[$sf])) $params[$sf] = (array) $params[$sf];
        }

        return $params;
    }
}
