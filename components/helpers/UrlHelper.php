<?php
namespace app\components\helpers;

class UrlHelper
{
    /**
	* Clean POST/GET parametrs from ";"
    *
    * @param array $params
    *
    * @return array cleaned params
	*/
    public function clean_params($params = array()) {
        if (empty($params)) return $params;

        foreach ($params as $key => $p) {
            if (strpos($p, ';')) $params[$key] = explode(';', $p);
        }

        return $params;
    }
}
