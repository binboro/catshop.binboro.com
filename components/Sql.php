<?php

namespace app\components;
use Yii;

class SqlPortYii {
  function query($query) {

    Yii::$app->db->createCommand($query)->execute();

    return true;
  }


  function fetchAll($query) {

    return Yii::$app->db->createCommand($query)->queryAll();
  }

  function escapeString($string) {
    if (empty($string))
      return "''";

    if ($string === "")
      return "''";

    $string = Yii::$app->db->quoteValue(addslashes($string));

    return $string;

  }
}

class Sql {
  public $connection;
  private $fields_by_all;

  private $tmp_limit = '';
  public $enable_log = false;

  public function setConfig($config) {
    $this->config = $config;
  }


  public function __construct($sql = false) {
        $this->connection = new SqlPortYii();
        $this->fields_by_all = false;
  }

  public function query($query) {
    $this->connection->query($query);
  }
  public function insert($table, $array) {
    if (empty($array))
      return false;

    foreach ($array as $first) {
      $fields = $this->makeFields($first, $table);
      break;
    }

    if (empty($fields))
      return false;

    $values = $this->makeValues($array, $fields[1]);

    foreach ($values as $value) {
      if (empty($value))
        continue;

      $sql = "INSERT IGNORE INTO `" . $table . "` (" . $fields[0] . ") VALUES " . implode(",", $value);

      $this->connection->query($sql);
    }

    return true;
  }

  public function autoUpdate($table, $array, $equal) {
    $fields = $this->makeFieldsAll($array);
    $this->create_table_if_not_exists($table, $fields);
    $this->create_and_update_fields($table, $fields);

    $this->update($table, $array, $equal);
  }

  public function update($table, $array, $equal) {


    foreach ($array as $item) {

      $set = array();
      foreach ($item as $k=>$v)
        if ($k != "id")
          $set[] = " `".$k."`=" . $this->connection->escapeString($v) . "";

      if (is_array($equal)) {
        foreach($equal as $eq) {
          $sqlWhere[] =  " " . $eq . "='" .$item[$eq]. "' ";
        }

        $sql = "UPDATE  `" . $table . "` set " . implode(",", $set) . " WHERE " . implode(" AND ", $sqlWhere). "";
        $this->connection->query($sql);
        $this->log($sql);
      }
      else {
          $sql = "UPDATE  `" . $table . "` set " . implode(",", $set) . " WHERE " . $equal . "='" .$item[$equal]. "'";
          $this->connection->query($sql);
          $this->log($sql);
      }

    }

    return true;
  }


  public function updateIn($table, $array, $equal) {
    if (empty($array))
      return false;

    foreach ($array as $k=>$v)
      if ($k != "id")
        $set[] = " `".$k."`=" . $this->connection->escapeString($v) . "";


    $group  = 0;
    $wheres = array();
    $count  = 0;
    foreach ($equal as $field => $v) {
      foreach ($v as $v2)
        $values[$field][] = $this->connection->escapeString($v2);

    if (empty($values))
      return false;

      $wheres[$group][] = " " . $field . " IN (" . implode(",", $values[$field]) .") ";

      $count++;

      if ($count == 500) {
        $count = 0;
        $group++;
      }
    }

    foreach ($wheres as $where) {
      $sql = "UPDATE  `" . $table . "` set " . implode(",", $set) . " WHERE " . implode(" AND ", $where). "";
      $this->log("S: " . $sql);

      $this->connection->query($sql);
      $this->log("E: " . $sql);
      usleep(300);
    }


    return true;
  }

  function makeFields($data, $table) {
    if (empty($data))
      return false;

    foreach ($data as $field => $value) {
      $fields[] = '`' . $field . '`';
      $fieldsClean[] = '' . $field . '';
    }


    return array(0 => implode(",", $fields), 1 => $fieldsClean);

  }

  function makeValues($data, $fields) {
    $cnt          = count($data);
    $values       = array(0 => array());
    $queries      = 0;
    $queries_cnt  = 0;

    $i = 0;
    foreach($data as $i => $obj) {

      if ($queries_cnt == 800) {
        $queries_cnt = 0;
        $queries++;
      }

      $tmp_array = array();
      foreach ($fields as $field)
        if (empty($obj[$field]))
          $tmp_array[] = "''";
        else if (is_array($obj[$field]))
          $tmp_array[] = json_encode($obj[$field]);
        else
          $tmp_array[] = $this->connection->escapeString($obj[$field]);



      $values[$queries][$i] = '(' . implode(",", $tmp_array) . ')';

      $queries_cnt++;
    }

    return $values;

  }



  function autoInsert($table, $values, $config) {
    if (empty($values))
      return false;

    $fields = $this->makeFieldsAll($values);
    $this->create_table_if_not_exists($table, $fields);
    $this->create_and_update_fields($table, $fields);

    if (!empty($config["id"]))
      $values = $this->autoUpdate2($table, $values, $config);

    $this->insert($table, $values);
  }



  function autoInsertInFile($table, $values, $config) {
    $Sql = new Sql();

    $this->log("Start auto insert In File");

    if (empty($values)) {
      $this->log("Empty values, going off");
      return false;
    }

      $this->log("Values: " . count($values));

    $fields = $this->makeFieldsAll($values);
    $this->log("makeFieldsAll");

    $this->create_table_if_not_exists($table, $fields);
    $this->log("create_table_if_not_exists");

    $this->create_and_update_fields($table, $fields);
    $this->log("create_and_update_fields");

    if (!empty($config["id"])) {
      $values = $this->autoUpdateInFile($table, $values, $config);

      $this->log("Update instead insert (Left for insert = ".count($values).")");
    }

    // check unique

    if (!empty($config["id"]))
    $values = $this->unique_values_in_array($values, $config["id"]);

    if (!empty($values))
      $this->insert_in_file($values, $table);

    return false;
  }

  public function unique_values_in_array($values, $unique_fields) {

    if (is_array($unique_fields))
      $unique_fields_v = implode(",", $unique_fields);
    else
      $unique_fields_v = $unique_fields;


    if (empty($values))
      return false;

    $newArray = array();
    $exists   = array();

    foreach ($values as $k => $r) {

      $id = '';
      if (is_array($unique_fields))
        foreach ($unique_fields as $field)
          $id .= $r[$field];
      else
        $id = $r[$unique_fields];

      $id = md5($id);

      if (!empty($exists[$id]))
        unset($values[$k]);

      $exists[$id] = true;


    }

    return $values;

  }


  function insert_in_file($values, $table, $skipFieldsSort = false) {


    // You need to put fields same way they are in
    if ($skipFieldsSort == false)
      foreach ($values as $k => $v)
        foreach ($v as $k2 => $v2)
          $fields[$k2] = $k2;
    else
      foreach ($values[0] as $k => $v)
        $fields[$k] = $v;



    ksort($fields);

    $this->log("Sort fields array in ASC");


    $n = array();

    if ($skipFieldsSort == false) {
      foreach ($values as $k => $v) {

        foreach ($fields as $field) {

          if (empty($v[$field]))
            $n[$k][$field] = '';
          else
            $n[$k][$field] = $v[$field];

            ksort($n[$k]);
        }

      }
    }
    else {
      $n = $values;
    }

    $this->log("Be sure each item has fields ");


    $file = str_replace(" ", "_", ($table . count($values) . microtime() . rand(0,1000) . "_sql_.dat"));

    $this->log("file name ");
    if (!empty($n) )
    {
      $this->array_to_csv($n, $file, ";");

      $this->log("array_to_csv ");

      $file = '/tmp_ram/' . $file;
  //$file = '/var/www/bbserver/catwalker_api.bbserver/tmp/' . $file;


      $this->query(
            "LOAD DATA INFILE '".$file."'
              IGNORE INTO TABLE ".$table."
              FIELDS TERMINATED BY ';'
              OPTIONALLY ENCLOSED BY '\"'
              (" . implode(",", $fields) .")
      ", false);
      $this->log("LOAD DATA INFILE '".$file."'
        INTO TABLE products
        FIELDS TERMINATED BY ';'
        (" . implode(",", $fields) .")");


      unlink($file);

      $this->log("unlink " . $file);
    }
  }

  function array_to_csv($input_array, $file, $delimiter, $path = false)
  {
    //  $file = '/var/www/bbserver/catwalker_api.bbserver/tmp/' . $file;

    if ($path != false)
      $file = $path . $file;
    else
      $file = "/tmp_ram/" . $file;

    $temp_memory = fopen($file, 'w+');
  //  $temp_memory = fopen"/var/tmp.csv", 'w+');
    // loop through the array

    foreach ($input_array as $line)
      fputcsv($temp_memory, $line, $delimiter);


    fclose($temp_memory);

  }




  function autoUpdate2($table, $data, $config) {
    // Generate list of IDS
    $this->log("autoUpdate2");
    $ids = array();
    foreach ($data as $value) {
      if (is_array($config["id"])) {
        foreach ($config["id"] as $ids)
          $wheres[$ids][] = $value[$ids];

      }
      else {
        $unique = $value[$config["id"]];
        $ids[] = $unique;
      }
    }


    if (!empty($wheres)) {
      foreach ($wheres as $key => $where)
        $sql[] = " `".$key."` IN ('" . implode("','", $where) . "') ";

      $q = $this->connection->fetchAll("SELECT * FROM `" . $table . "` WHERE " . implode(" AND ", $sql) . "");
    }
    else
      $q = $this->connection->fetchAll("SELECT * FROM `" . $table . "` WHERE `".$config["id"]."` IN ('" . implode("','", $ids) . "')");

    foreach ($q as $r) {

      $id = $this->make_select_id($config, $r);
      $existent[$id] = $r;
    }
    $this->log("Fetch");


    $update = array();
    foreach ($data as $k => $r) {
      $id = $this->make_select_id($config, $r);

      if (empty($existent[$id]))
        continue;

      $found = $existent[$id];

      if (empty($config["id_hash"]))
        $update[] = $r;
      else {
        if (!empty($r[$config["id_hash"]]) AND !empty($found[$config["id_hash"]]) AND $found[$config["id_hash"]] != $r[$config["id_hash"]])
          $update[] = $r;

      }

      unset($data[$k]);
    }

    $this->log("Unset after hash checks");

    if(!empty($config["no_update"]))
      return $data;

    if (!empty($update)) {
        $this->update($table, $update, $config["id"]);
        $this->log("Update " . count($update));

    }


    return $data;
  }


  function updateInFile($table, $data, $id_field, $skipFieldsSort = false) {
      $ids_to_delete = array();
      foreach ($data as $r) {
        $ids_to_delete[] = $r[$id_field];
      }

      if (!empty($ids_to_delete))
        $this->delete("DELETE FROM " . $table . " WHERE `id` IN (in:id)",array("in:id" =>  $ids_to_delete));

      $this->insert_in_file($data, $table, $skipFieldsSort);
  }

  function autoUpdateInFile($table, $data, $config) {
    // Generate list of IDS
    $this->log("autoUpdate InFile");
    $ids = array();
    foreach ($data as $value) {
      if (is_array($config["id"])) {
        foreach ($config["id"] as $ids)
          $wheres[$ids][] = $value[$ids];

      }
      else {
        $unique = $value[$config["id"]];
        $ids[] = $unique;
      }
    }


    if (!empty($wheres)) {
      foreach ($wheres as $key => $where)
        $sql[] = " `".$key."` IN ('" . implode("','", $where) . "') ";

      $q = $this->connection->fetchAll("SELECT * FROM `" . $table . "` WHERE " . implode(" AND ", $sql) . "");
    }
    else
      $q = $this->connection->fetchAll("SELECT * FROM `" . $table . "` WHERE `".$config["id"]."` IN ('" . implode("','", $ids) . "')");

    foreach ($q as $r) {

      $id = $this->make_select_id($config, $r);
      $existent[$id] = $r;

    }
    $this->log("Fetch");


    $update = array();
    $i = 0;
    foreach ($data as $k => $r) {
      $id = $this->make_select_id($config, $r);

      if (empty($existent[$id]))
        continue;

      $found = $existent[$id];

      if (empty($config["id_hash"])) {

        $update[$found["id"]] = $r;
        $update[$found["id"]]["id"] = $found["id"];

        $ids_to_delete[] = $found["id"];
        $i++;
      }
      else
      {
        if (!empty($r[$config["id_hash"]]) AND !empty($found[$config["id_hash"]]) AND $found[$config["id_hash"]] != $r[$config["id_hash"]])
        {

          $update[$found["id"]] = $r;
          $update[$found["id"]]["id"] = $found["id"];
          $ids_to_delete[] = $found["id"];
          $i++;
        }
      }


      unset($data[$k]);

    }

    if(!empty($config["no_update"]))
      return $data;

    if (!empty($ids_to_delete)) {

      $this->delete("DELETE FROM " . $table . " WHERE `id` IN (in:id)",array("in:id" =>  $ids_to_delete));
    }

    $this->log("Unset after hash checks");


    if (!empty($update)) {
    //    $this->update($table, $update, $config["id"]);

      $this->insert_in_file($update, $table);
      $this->log("Update In File " . count($update));

    }



    return $data;
  }

  function make_select_id($config, $r) {
    if (is_array($config["id"])) {
        foreach ($config["id"] as $c)
          $id[] = $r[$c];

        $id = implode("_", $id);

    }
    else
      $id = $r[$config["id"]];

    return $id;
  }

  function get_type_level($type) {

    if ($type == "int(11)")
      return 1;

    if ($type == "bigint(20)")
      return 2;


    if ($type == "varchar(255)")
      return 3;

    if ($type == "text")
      return 4;


    return 0;
  }

  function makeFieldsAll($values) {

    $fields = array();
    foreach ($values as $r) {
      foreach ($r as $field =>  $value) {
        $fields[$field]["name"] = $field;

        if (empty($fields[$field]["typeLevel"])) $fields[$field]["typeLevel"] = 0;

        if (is_numeric($value) && $fields[$field]["typeLevel"] < 3 && !is_float($value)) {
            $str = "" . $value;
            $cnt = iconv_strlen($str);

            if ($cnt > 11) {
              $fields[$field]["type"] = "bigint(20)";
              $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
            }
            else if (  $fields[$field]["typeLevel"] < 2) {
              $fields[$field]["type"] = "int(11)";
              $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
            }


        }
        else {
          if (is_array($value))
            $cnt = 500;
          else {
            $str = "" . $value;
            $cnt = iconv_strlen($str);
          }

          if ($cnt > 255) {
            $fields[$field]["type"] = "text";
            $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
          }
          else if (  $fields[$field]["typeLevel"] < 4) {
            $fields[$field]["type"] = "varchar(255)";
            $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
          }
        }


      }

      if ($this->fields_by_all == false)
        break;
    }




    // Update fields data
    return $fields;

  }


  function create_table_if_not_exists($table, $fields) {

    $q = $this->connection->fetchAll("SHOW TABLES LIKE '" . $table . "'");

    $fieldsSql = array('`id` int(11) NOT NULL AUTO_INCREMENT');
    foreach ($fields as $field)
      $fieldsSql[] = ' `'.$field["name"].'` '.$field["type"].' NULL';

    $fieldsSql[] = ' PRIMARY KEY (`id`)';

    // Create new table if doesn ot exists

    if (empty($q))
      $this->connection->query("CREATE TABLE IF NOT EXISTS `" . $table . "`(".implode(", ", $fieldsSql).") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");


    return true;
  }


  function create_and_update_fields($table, $fields) {

    $q = $this->connection->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $table. "'");

    $columns = array();
    foreach ($q as $r) {
      $columns[$r["COLUMN_NAME"]] = $r;
      $columns[$r["COLUMN_NAME"]]["typeLevel"] = $this->get_type_level($r["COLUMN_TYPE"]);
    }

    foreach ($fields as $key => $field)
      if (empty($columns[$field["name"]]))
        $create_columns[$field["name"]] = $field;
      else if ($field["typeLevel"] > $columns[$field["name"]]["typeLevel"]) {
        $field["old_name"] = $columns[$field["name"]]["COLUMN_NAME"];
        $update_columns[$field["name"]] = $field;
      }


    if (!empty($create_columns))
      foreach ($create_columns as $column)
        $this->connection->query("ALTER TABLE `" . $table . "` ADD `" . $column["name"]  . "` " . $column["type"] . "");

    if (!empty($update_columns))
      foreach ($update_columns as $column) {

        $encoding  = '';
        if ($column["typeLevel"] > 2)
          $encoding = ' CHARACTER SET utf8 COLLATE utf8_general_ci ';

        $this->connection->query("ALTER TABLE `" . $table . "` CHANGE `" . $column["old_name"]  . "` `" . $column["name"]  . "` " . $column["type"] . $encoding);
      }


  }

  function set_limit($offset = 0 , $limit = 20) {
    $this->tmp_limit = " limit " . $offset . ", " . $limit . " ";
  }

  function get_limit() {
    return $this->tmp_limit;
  }

  function del_limit() {
    $this->tmp_limit = "";
  }

  function sl($table, $where = false, $params = false, $byField = false) {

    if ($where == false)
      $q = $this->select("SELECT * FROM " . $table . $this->get_limit());
    else
      $q = $this->select("SELECT * FROM " . $table ." WHERE " . $where . $this->get_limit(), $params, $byField);


    $this->del_limit();
    return $q;

  }


  function select($query, $params = false, $byField = false) {


    if ($params != false)
      foreach ($params as $k=>$v) {
        $ex = explode("in:", $k);

        if (empty($ex[1]))
          $query = str_replace(":" . $k, "" . $this->connection->escapeString($v) . "", $query);
        else {

          $values = array();

          foreach ($v as $v2)
            $values[] = $this->connection->escapeString($v2);

          $values = "". implode(",", $values) ."";


          $query = str_replace($k, "" . $values . "", $query);
        }
      }

    $this->log("Start: " . $query);

    $q = $this->connection->fetchAll($query);

    if ($byField == true) {
      $out = array();

      foreach ($q as $r)
        $out[$r[$byField]] = $r;
    }
    else
      $out = $q;

    $this->log("End S");
    return $out;

  }



  function delete($query, $params = false) {


    if ($params !== false) {
      foreach ($params as $k=>$v) {
        $ex = explode("in:", $k);

        if (empty($ex[1]))
          $query = str_replace(":" . $k, "" . $this->connection->escapeString($v) . "", $query);
        else {

          $values = array();

          foreach ($v as $v2)
            $values[] = $this->connection->escapeString($v2);

          $values = "". implode(",", $values) ."";


          $query = str_replace($k, "" . $values . "", $query);
        }
      }
    }


    $this->log("Make delete SQL");

    $this->log("Delete query start " . $query);

    $this->connection->query($query);

    $this->log("Delete query end");


  }



  public function log($msg) {

    if (empty($this->enable_log))
      return false;

    if (empty($this->start_time)) {
      $this->start_time = microtime(true);
      $this->last_time  = 0;
    }
    $difTime = 0;

    $time = microtime(true) - $this->start_time;

    if ($time < 0.0002) $time = "Uncountable fast";
    else
      $difTime = $time - $this->last_time;


    $this->last_time = $time;
    echo "<div style='padding:6px; color:rgba(0, 0, 0, 0.87); margin-top:6px; background:#FFECB3;font-size:12px; font-family:Arial;'><span style='font-size:10px'>(Sql.php)</span> - <b style='display:inline-block;min-width:100px; max-height:200px; max-width:700px; overflow:hidden;'>" . $msg . "</b>: " . $time." (".$difTime.")</div>";
    $this->last_time = $time;
  }

  public function export_database_data($tables = false, $path = false, $prefix = "sql") {
    $q = $this->select("SHOW TABLES");

    $data = false;

    foreach ($q as $r) {
      foreach ($r as $r2 )
      $table = $r2;

      for ($cicle = 0; $cicle < 100000; $cicle++) {
        $offset = $cicle * 250000;
        $this->set_limit($offset, 250000);
        $data = $this->sl($table);

        if (empty($data))
          break;

        $fields = array();

        foreach ($data[0] as $k=>$v)
          $fields[] = $k;

        array_unshift($data, $fields);

        $this->array_to_csv($data, $prefix."_".$table."--".$offset . ".csv", ";", $path);
      }
    }

  }

  public function import_database($path = "/tmp_ram/", $file_prefix = "sql") {
    // Path is not in use Yaf_Controller_Abstract
    $scan = scandir($path);
    $scan_count = count($scan);

    $files = array();

    for ($i = 0; $i < $scan_count; $i ++ ) {
      $file = $scan[$i];

      if ($file == "." OR $file == "..")
        continue;

      if ($file_prefix != "") {
        $x = explode("_", $file);

        if ($x[0] != $file_prefix)
          continue;


      }

      $x = explode("--", $file);
      $table = $x[0];

      $table = str_replace($file_prefix."_", "", $table);
      $table = str_replace(".sql", "", $table);


      $handle = fopen($path . $file, "r");
      $line = fgets($handle);
      $fields = str_getcsv($line, ';', '\"');

      $this->query(
            "LOAD DATA INFILE '".$path . $file."'
              IGNORE INTO TABLE ".$table."
              FIELDS TERMINATED BY ';'
              OPTIONALLY ENCLOSED BY '\"'
              IGNORE 1 LINES
              (`" . implode("`,`", $fields) ."`)
      ", false);

      $this->log("LOAD DATA INFILE '".$path . $file."'
        INTO TABLE products
        FIELDS TERMINATED BY ';'
        (" . implode(",", $fields) .")");

    }

  }
}

?>
