<?php

namespace app\components;
use Yii;

class Arrays {

  function obj_to_key_value($k, $v, $rArray, $parent = false) {

    $array = array();
    if ($parent === false)
      $parent = $k;

    if ( is_array($v) )
        foreach ($v as $k2 => $v2)
          $array = $this->obj_to_key_value($k2, $v2, $array, ($parent ."_" . $k2));

    else
      $array[$parent] = $v;


    return array_merge($array, $rArray);

  }


  function array_to_csv($input_array, $output_file_name, $delimiter)
  {
    $temp_memory = fopen(dirname(__DIR__) . "/mysql_csv/tmp.csv", 'w+');
  //  $temp_memory = fopen"/var/tmp.csv", 'w+');
    // loop through the array

    foreach ($input_array as $line) {
      fputcsv($temp_memory, $line, $delimiter);
    }

    fclose($temp_memory);

  }



  function herachly($array, $id, $parent) {
    $newArray = array();

    foreach ($array as $row) {

      if (empty($row[$parent]))
        $row["children"] = $this->herachly_children($array, $id, $parent, $row);
      else
        continue;


      $newArray[] = $row;
    }

    return $newArray;
  }

  function herachly_children($array, $id, $parent, $parent_obj) {
    $children = array();

    foreach ($array as $row)
      if ($row[$parent] == $parent_obj[$id])
        $children[] = $row;

    return $children;
  }

  function groupBy(&$array, $field) {

    $newArray = array();
    foreach ($array as $k => $r) {
      $newArray[$r[$field]] = $r;
      unset($array[$k]);
    }

    $array = $newArray;
    
  }
}
?>
