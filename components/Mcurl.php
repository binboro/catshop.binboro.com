<?php

namespace app\components;
use Yii;
use app\components\Log;

class Mcurl {

  public $curl_inits;
  public $url_arr;
  public $cores = 1;
  public $self_keys = false;
  public $posts = false;
  function add($url) {
    $this->url_arr[md5($url)] = $url;
  }

  function cores($cores) {
    $this->cores = $cores;
  }

  function get_cores() {
    return $this->cores;
  }

  function add_post($data, $core = false) {
    if ($core == false)
      $this->posts = $data;
    else
      $this->core_posts[$core] = array("data"=>$data);
  }


  function post_for_core($data) {
    foreach ($data as $core => $v) {

      $this->core_posts[$core]["data"] = json_encode($v);

    }
  }



  function add_same($url, $params) {
    $cores = $this->get_cores();

    for($i = 1; $i <= $cores; $i++) {
       $url2 = $url ."?core=" .$i. "&" . $params;

      $this->url_arr[md5($url2)] = $url2;
    }
  }

  public $core_iter = 0;
  function add_core($url, $performance = false) {
      $this->core_iter++;
      $url = $url ."?core=" .$this->core_iter. "&time=".time();
      $this->url_arr[md5($url)] = $url;
      $this->url_arr_prf[md5($url)] = $performance;
  }

  function loop($data, $params) {
    $Lg = new Log(true);
      $Lg->show("loop s");
    $size = count($data);
  $Lg->show("loop s" . $size);

  //  $chuncked = array_chunk ($data , $size, true);
    //print_r($chuncked);
    $i = 0;
    $last_chunk = 0;
    foreach ($this->url_arr as $key => $url)  {

      $chunk = ceil($size * ($this->url_arr_prf[$key]/100));
      $output = array_slice($data, $last_chunk, $chunk);

      $this->core_posts[$i] = array("data" => $output);


      $Lg->show($i . " == " . count($this->core_posts[$i]["data"]));


      $this->core_posts[$i]["data"] = json_encode($this->core_posts[$i]["data"]);


      if (!empty($params))
          $this->core_posts[$i]["params"] = json_encode($params);

      $last_chunk += $chunk;


      $i++;
    }


    $result = $this->launch(true);




$Lg->show("loop 2 i:".   $i);

    $out = [];

    foreach ($result as $r){
      if (!empty($r["data"]))
        foreach ($r["data"] as $k => $v)
          if ($this->self_keys)
            $out[$this->self_keys] = $v;
          else
            $out[] = $v;
    }


    $Lg->show("loop merged");


    return $out;
  }
  function launch($json = true) {

    $Lg = new Log(true);

    $core_iter = -1;
    $Lg->show("launch s");
    foreach ($this->url_arr as $key => $url)  {
        $core_iter++;

        $curls[$key] = curl_init($url);
        curl_setopt($curls[$key], CURLOPT_RETURNTRANSFER, true);

        if (!empty($this->posts)) {
          curl_setopt($curls[$key], CURLOPT_POST, 1);
          curl_setopt($curls[$key], CURLOPT_POSTFIELDS, $this->posts);
        }

        if (!empty($this->core_posts[$core_iter])) {
          curl_setopt($curls[$key], CURLOPT_POST, 1);

          curl_setopt($curls[$key], CURLOPT_POSTFIELDS, $this->core_posts[$core_iter]);
        }
    }

    $mh = curl_multi_init();

    foreach ($curls as $key=>$url)
      curl_multi_add_handle($mh, $curls[$key]);

    $running = null;
    do {
      curl_multi_exec($mh, $running);
    } while ($running);


    foreach ($curls as $key=>$url)
      curl_multi_remove_handle($mh, $curls[$key]);

    curl_multi_close($mh);

    if ($json == true)
      foreach ($curls as $key=>$url) {
        $results[] = json_decode(curl_multi_getcontent($curls[$key]), 1);

      }
    else
      foreach ($curls as $key=>$url)
        $results[] = curl_multi_getcontent($curls[$key]);

$Lg->show("launch E");
    return $results;
  }
}
