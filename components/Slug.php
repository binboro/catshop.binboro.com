<?php
namespace app\components;
use Yii;

use app\components\Sql;

class Slug {

  public function genereteSlug($title, $tables = [], $slugs = []) {
    $slug = $this->slugify($title);
    $slug = $this->checkSlug($slug, $tables, $slugs);
    return $slug;
  }


  public function slugArray($array, $config, $tables, $slugs_to_check, $unique_field = false) {
    $Sql = new Sql();

    if (empty($config))
      return false;

    if (empty($config["field"]))
      return false;
    else
      $sourceField = $config["field"];

    $slugField = "slug";
    if (!empty($config["slugField"]))
      $slugField = $config["slugField"];

    if (empty($array))
      return false;

    $slugs = array();

    foreach ($array as $key => $obj) {

      $slug = $this->slugify($obj[ $sourceField ]);
      $array[ $key ][ $slugField ] = $slug;

      $slugs[$slug] = $slug;
    }

    if (!empty($tables))
      foreach ($tables as $table => $field) {
        $q = $Sql->select("SELECT slug FROM ". $table . " WHERE " . $field . " IN (in:slug)", array("in:slug" => $slugs));

        foreach ($q as $r)
          $slugs[$r[$field]] = $r[$field];
      }

      foreach ($array as $k => $v) {

        if (!empty($slugs_to_check))
          if(in_array($v[$slugField], $slugs_to_check)) {

            if ($unique_field != false)
              $array[$k][ $slugField ] = $v[$slugField] . "_" . $array[$k][ $unique_field ];
            else
              $array[$k][ $slugField ] = $v[$slugField] . "_" .md5(microtime().rand(0,2000));
          }
      }


    foreach ($array as $k => $v)
      foreach ($array as $k2 => $v2)
        if ( $v2[$slugField] == $v[$slugField] && $k != $k2) {
          if ($unique_field != false)
            $array[$k][ $slugField ] = $v[$slugField] . "_" . $array[$k][ $unique_field ];
          else
            $array[$k][ $slugField ] = $v[$slugField] . "_" .md5(microtime().rand(0,2000));
        }



    return $array;

    // Check clugs
  }

  public function checkSlug($slug, $tables, $slugs = []) {
    foreach ($tables as $table => $field) {
      $sql = "SELECT * FROM " . $table . " WHERE `" . $field . "` = '" . $slug . "'";
      $categories_data = Yii::$app->db->createCommand($sql)->queryAll();
      if (!empty($categories_data)) {
        $slug = $this->addNumber($slug);
        return $this->checkSlug($slug, $tables);
      }
    }

    if(in_array($slug, $slugs)) {
      $slug = $this->addNumber($slug);
      return $this->checkSlug($slug, $tables);
    }

    return $slug;
  }

  public function slugify($text) {
    if (empty($text)) {
      return uniqid();
    }

    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    //$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);

    if (empty($text)) {
      return uniqid();
    }

    return $text;
  }



  public function randomNumber() {
    return rand(0, 15);
  }

  public function addNumber($slug) {
    $slugs = explode('_', $slug);
    $slug = $slugs[0] . '_' .  time()  . $this->randomNumber();
    return $slug;
  }


}


?>
