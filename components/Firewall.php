<?php

namespace app\components;
use Yii;
use app\components\Sql;

class Firewall {

  public $enable_log = false;

  function __construct($enable_log = false){
    $this->enable_log = $enable_log;
  }
  public function protect(&$data, $fields_to_check = false) {
      $this->log("check S:");

      if (empty($data))
        return false;

      foreach ($data as $key => $row) {

        if (!empty($match = $this->check_row($key, $row, $fields_to_check))) {

          unset($data[$key]);
        }


      }


      $this->log("check E:");
    //  return $result;


  }


  function check_row($key, $row, $fields_to_check) {
    $rules = $this->get_rules();
    $fields_to_check_arg = $fields_to_check;
    $match = array();

    $str_for_like = false;
    if ($fields_to_check == false) {
      foreach ($row as $k => $v)
        $fields_to_check[] = $k;
    }



    foreach ($rules as $rule) {

      $str_for_like = '';
      foreach ($fields_to_check as $field) {

        if (!empty($rule["field"]) AND ((string)$field != (string)$rule["field"]))
          continue;

        $value = $row[$field];


        if ($rule["match"] == "equal")
          if (strtolower($rule["rule_value"]) == strtolower($value))
            $match[$field][] = array("rule" => $rule, "value" => $row);

        if ($rule["match"] == "like")
          if ( strstr (strtolower($value), strtolower($rule["rule_value"])) )
            $match[$field][] = array("rule" => $rule, "value" => $value);
      }



    }

    if (empty($match))
      return false;

    return array("match" => $match);
  }

  function set_rules($data) {
    $this->rules = $data;
  }
  function get_rules() {

    

    if (!empty($this->rules))
      return $this->rules;


    $Sql = new Sql();

    $this->rules = $Sql->select("SELECT * FROM firewall_data");

    return $this->rules;
  }



  public function log($msg) {

    if (empty($this->enable_log))
      return false;

    if (empty($this->start_time)) {
      $this->start_time = microtime(true);
      $this->last_time  = 0;
    }
    $difTime = 0;

    $time = microtime(true) - $this->start_time;

    if ($time < 0.0002) $time = "Uncountable fast";
    else
      $difTime = $time - $this->last_time;


    $this->last_time = $time;
    echo "<div style='padding:6px; color:rgba(0, 0, 0, 0.87); margin-top:6px; background:#FFCDD2;font-size:12px; font-family:Arial;'><span style='font-size:10px'>(Firewall.php)</span> - <b style='display:inline-block;min-width:100px;'>" . $msg . "</b>: " . $time." (".$difTime.")</div>";

    $this->last_time = $time;
  }
}
?>
