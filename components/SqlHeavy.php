<?php

namespace app\components;
use Yii;

class SqlPortYiiHeavy {
  function query($query) {
    //Yii::$app->db->createCommand($query)->execute();

    $d = shell_exec('export PATH=$PATH:/usr/local/mysql/bin; mysql -u root -proot -e  "use catwalker_api;'.$query.'\G"');
    //$d = shell_exec('export PATH=$PATH:/usr/local/mysql/bin; mysql -u root -proot -e  "use catwalker_api;INSERT INTO products (Age) VALUES(\'31\')"\G');

    return true;
  }


  function fetchAll($query) {
    return Yii::$app->db->createCommand($query)->queryAll();
  }

  function escapeString($string) {

    $string = "'" . addslashes($string) . "'";
    //Yii::$app->db->quoteValue($string);

    return $string;

  }
}

class SqlHeavy {
  public $connection;

  public function setConfig($config) {
    $this->config = $config;
  }


  public function __construct($sql = false) {
        $this->connection = new SqlPortYiiHeavy();
  }

  public function insert($table, $array) {

    foreach ($array as $first) {
      $fields = $this->makeFields($first, $table);
      break;
    }

    if (empty($fields))
      return false;

    $values = $this->makeValues($array, $fields[1]);

    foreach ($values as $value) {
      if (empty($value))
        continue;

      $sql = "INSERT INTO " . $table . " (" . $fields[0] . ") VALUES " . implode(",", $value) . "";
      $this->connection->query($sql);
    }

    return true;
  }

  public function autoUpdate($table, $array, $equal) {
    $fields = $this->makeFieldsAll($array);
    $this->create_table_if_not_exists($table, $fields);
    $this->create_and_update_fields($table, $fields);

    $this->update($table, $array, $equal);
  }

  public function update($table, $array, $equal) {


    foreach ($array as $item) {

      $set = array();
      foreach ($item as $k=>$v)
        if ($k != "id")
          $set[] = " ".$k."=" . $this->connection->escapeString($v) . "";

      if (is_array($equal)) {
        foreach($equal as $eq) {
          $sqlWhere[] =  " " . $eq . "='" .$item[$eq]. "' ";
        }

        $sql = "UPDATE  " . $table . " set " . implode(",", $set) . " WHERE " . implode(" AND ", $sqlWhere). "";
        $this->connection->query($sql);
      }
      else {
          $sql = "UPDATE  " . $table . " set " . implode(",", $set) . " WHERE " . $equal . "='" .$item[$equal]. "'";
          $this->connection->query($sql);
      }

      usleep(1500);

    }

    return true;
  }

  function makeFields($data, $table) {
    if (empty($data))
      return false;

    foreach ($data as $field => $value) {
      $fields[] = '' . $field . '';
      $fieldsClean[] = '' . $field . '';
    }


    return array(0 => implode(",", $fields), 1 => $fieldsClean);

  }

  function makeValues($data, $fields) {
    $cnt          = count($data);
    $values       = array(0 => array());
    $queries      = 0;
    $queries_cnt  = 0;

    $i = 0;
    foreach($data as $i => $obj) {

      if ($queries_cnt == 500) {
        $queries_cnt = 0;
        $queries++;
      }

      $tmp_array = array();
      foreach ($fields as $field)
        if (empty($obj[$field]))
          $tmp_array[] = "''";
        else if (is_array($obj[$field]))
          $tmp_array[] = $this->connection->escapeString(json_encode($obj[$field]));
        else
          $tmp_array[] = $this->connection->escapeString($obj[$field]);



      $values[$queries][$i] = '(' . implode(",", $tmp_array) . ')';

      $queries_cnt++;
    }

    return $values;

  }



  function autoInsert($table, $values, $config) {
    if (empty($values))
      return false;

    $fields = $this->makeFieldsAll($values);
    $this->create_table_if_not_exists($table, $fields);
    $this->create_and_update_fields($table, $fields);

    if (!empty($config["id"]) AND empty($config["no_update"]))
      $values = $this->autoUpdate2($table, $values, $config);

    $this->insert($table, $values);
  }

  function autoUpdate2($table, $data, $config) {
    // Generate list of IDS
    $ids = array();
    foreach ($data as $value) {
      if (is_array($config["id"])) {
        foreach ($config["id"] as $ids)
          $wheres[$ids][] = $value[$ids];

      }
      else {
        $unique = $value[$config["id"]];
        $ids[] = $unique;
      }
    }

    if (!empty($wheres)) {
      foreach ($wheres as $key => $where)
        $sql[] = " ".$key." IN ('" . implode("','", $where) . "') ";

      $q = $this->connection->fetchAll("SELECT * FROM " . $table . " WHERE " . implode(" AND ", $sql) . "");
    }
    else
      $q = $this->connection->fetchAll("SELECT * FROM " . $table . " WHERE ".$config["id"]." IN ('" . implode("','", $ids) . "')");

    foreach ($q as $r) {

      $id = $this->make_select_id($config, $r);
      $existent[$id] = $r;
    }


    $update = array();
    foreach ($data as $k => $r) {
      $id = $this->make_select_id($config, $r);

      if (empty($existent[$id]))
        continue;

      $found = $existent[$id];

      if (empty($config["id_hash"]))
        $update[] = $r;
      else {
        if (!empty($r[$config["id_hash"]]) AND !empty($found[$config["id_hash"]]) AND $found[$config["id_hash"]] != $r[$config["id_hash"]])
          $update[] = $r;

          usleep(150);
      }

      unset($data[$k]);
    }

    if (!empty($update)) {
        $this->update($table, $update, $config["id"]);
    }


    return $data;
  }

  function make_select_id($config, $r) {
    if (is_array($config["id"])) {
        foreach ($config["id"] as $c)
          $id[] = $r[$c];

        $id = implode("_", $id);

    }
    else
      $id = $r[$config["id"]];

    return $id;
  }

  function get_type_level($type) {

    if ($type == "int(11)")
      return 1;

    if ($type == "bigint(20)")
      return 2;


    if ($type == "varchar(255)")
      return 3;

    if ($type == "text")
      return 4;


    return 0;
  }

  function makeFieldsAll($values) {

    $fields = array();
    foreach ($values as $r)
      foreach ($r as $field =>  $value) {
        $fields[$field]["name"] = $field;

        if (empty($fields[$field]["typeLevel"])) $fields[$field]["typeLevel"] = 0;

        if (is_numeric($value) && $fields[$field]["typeLevel"] < 3 && !is_float($value)) {
            $str = "" . $value;
            $cnt = iconv_strlen($str);

            if ($cnt > 11) {
              $fields[$field]["type"] = "bigint(20)";
              $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
            }
            else if (  $fields[$field]["typeLevel"] < 2) {
              $fields[$field]["type"] = "int(11)";
              $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
            }


        }
        else {
          if (is_array($value))
            $cnt = 500;
          else {
            $str = "" . $value;
            $cnt = iconv_strlen($str);
          }

          if ($cnt > 255) {
            $fields[$field]["type"] = "text";
            $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
          }
          else if (  $fields[$field]["typeLevel"] < 4) {
            $fields[$field]["type"] = "varchar(255)";
            $fields[$field]["typeLevel"] = $this->get_type_level($fields[$field]["type"]);
          }
        }


      }




    // Update fields data
    return $fields;

  }


  function create_table_if_not_exists($table, $fields) {

    $q = $this->connection->fetchAll("SHOW TABLES LIKE '" . $table . "'");

    $fieldsSql = array('id int(11) NOT NULL AUTO_INCREMENT');
    foreach ($fields as $field)
      $fieldsSql[] = ' '.$field["name"].' '.$field["type"].' NULL';

    $fieldsSql[] = ' PRIMARY KEY (id)';

    // Create new table if doesn ot exists

    if (empty($q))
      $this->connection->query("CREATE TABLE IF NOT EXISTS " . $table . " (".implode(", ", $fieldsSql).") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");


    return true;
  }


  function create_and_update_fields($table, $fields) {

    $q = $this->connection->fetchAll("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $table. "'");

    $columns = array();
    foreach ($q as $r) {
      $columns[$r["COLUMN_NAME"]] = $r;
      $columns[$r["COLUMN_NAME"]]["typeLevel"] = $this->get_type_level($r["COLUMN_TYPE"]);
    }

    foreach ($fields as $key => $field)
      if (empty($columns[$field["name"]]))
        $create_columns[$field["name"]] = $field;
      else if ($field["typeLevel"] > $columns[$field["name"]]["typeLevel"]) {
        $field["old_name"] = $columns[$field["name"]]["COLUMN_NAME"];
        $update_columns[$field["name"]] = $field;
      }


    if (!empty($create_columns))
      foreach ($create_columns as $column)
        $this->connection->query("ALTER TABLE " . $table . " ADD " . $column["name"]  . " " . $column["type"] . "");

    if (!empty($update_columns))
      foreach ($update_columns as $column) {

        $encoding  = '';
        if ($column["typeLevel"] > 2)
          $encoding = ' CHARACTER SET utf8 COLLATE utf8_general_ci ';

        $this->connection->query("ALTER TABLE " . $table . " CHANGE " . $column["old_name"]  . " " . $column["name"]  . " " . $column["type"] . $encoding);
      }


  }



  function select($query, $params = false, $byField = false) {
    if ($params != false)
      foreach ($params as $k=>$v) {
        $ex = explode("in:", $k);

        if (empty($ex[1]))
          $query = str_replace(":" . $k, "" . $this->connection->escapeString($v) . "", $query);
        else {

          $values = array();

          foreach ($v as $v2)
            $values[] = $this->connection->escapeString($v2);

          $values = "". implode(",", $values) ."";


          $query = str_replace($k, "" . $values . "", $query);
        }
      }


    $q = $this->connection->fetchAll($query);

    if ($byField == true) {
      $out = array();

      foreach ($q as $r)
        $out[$r[$byField]] = $r;
    }
    else
      $out = $q;

    return $out;

  }



  function delete($query, $params = false) {
    if ($params != false)
      foreach ($params as $k=>$v) {
        $ex = explode("in:", $k);

        if (empty($ex[1]))
          $query = str_replace(":" . $k, "" . $this->connection->escapeString($v) . "", $query);
        else {

          $values = array();

          foreach ($v as $v2)
            $values[] = $this->connection->escapeString($v2);

          $values = "". implode(",", $values) ."";


          $query = str_replace($k, "" . $values . "", $query);
        }
      }


    $this->connection->query($query);



  }



}

?>
