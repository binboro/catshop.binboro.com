<?php

namespace app\components;
use Yii;

class XMLParser  {

  function xml2array($xml)
  {
  

    $arr = [];


      if(!is_array($xml) AND $attributes = $xml->attributes())
        foreach ($attributes as $key => $attr)
          $arr[$key] = (string)($attr);


    foreach ($xml as $k => $v) {


      if (!is_array($v)) {
        if (is_object($v))
          $e = get_object_vars($v);

        if (empty($e)) {
          $arr[$k] = (string)$v;
        }
        else {
          $out = $this->xml2array($e);

          if (!empty($out))
            foreach ($out as $k2 => $v2)
              $arr[$k][$k2] = $v2;
        }
      }
    }

    return $arr;
  }


}

?>
