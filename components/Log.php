<?php
namespace app\components;
use Yii;

use app\components\Log;

class Log {
  public $log_folder = "../runtime/logs/";
  public $show_logs = false;
  public $file_name = false;
  public $file_size = 0;

  function __construct($show_logs = false, $file_name = false) {
    if ($file_name !== false)
      $file_name = $file_name . '.txt';

    $this->show_logs = $show_logs;
    $this->file_name = $file_name;
  }

  function show($msg) {
    if (empty($this->show_logs) AND empty($this->file_name))
      return false;

    if (empty($this->start_time)) {
      $this->start_time = microtime(true);
      $this->last_time  = 0;
    }
    $difTime = 0;

    $time = microtime(true) - $this->start_time;

    if ($time < 0.0002) $time = "Uncountable fast";
    else
      $difTime = $time - $this->last_time;



    $this->last_time = $time;
    if (!empty($this->show_logs))
      echo "<div style='padding:6px; color:rgba(0, 0, 0, 0.87); margin-top:6px; background:#F3E5F5;font-size:12px; font-family:Arial;'><span style='font-size:10px'>(Log.php)</span> - <b style='display:inline-block;min-width:100px;'>" . $msg . "</b>: " . $time." (".$difTime.")</div>";

    if (!empty($this->file_name))
      $this->write_into_file($msg . " : " . $time." (".$difTime.")");

    $this->last_time = $time;
  }

  function get_file_name() {
    if (empty($this->file_name))
      return "log.php";
    else
      return $this->file_name;
  }

  function get_log_folder() {
      return $this->log_folder;
  }

  function write_into_file($msg) {
    $file       = $this->get_file_name();
    $folder     = $this->get_log_folder();

    $path = $folder . $file;
    if (file_exists($path) AND $this->file_size == 0)
      $this->file_size  = filesize($path);

    if ($this->file_size > 5000000)
      unlink($path);

    file_put_contents($path, date("Y-m-d h:i:s") . " " . $msg ."\n", FILE_APPEND);


  }
}
?>
