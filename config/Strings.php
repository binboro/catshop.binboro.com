<?php

namespace app\components;
use Yii;

class Strings {

  function obj_to_key_value($k, $v, $rArray, $parent = false) {

    $array = array();
    if ($parent === false)
      $parent = $k;

    if ( is_array($v) )
        foreach ($v as $k2 => $v2)
          $array = $this->obj_to_key_value($k2, $v2, $array, ($parent ."_" . $k2));

    else
      $array[$parent] = $v;


    return array_merge($array, $rArray);

  }
}
?>
