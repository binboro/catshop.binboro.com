
<div id="mainPage">
<h2>Main page images</h2>


<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    New row <span class="caret"></span>
  </button>

  <ul class="dropdown-menu dropdown-menu-rows">
    <li><a href="#"   class="row_option" data-type="1"><img class="rowIcon1" src="/adminTheme/rowsIcons/1item.png"/></a></li>
    <li><a href="#"  class="row_option"  data-type="2"><img  class="rowIcon1"  src="/adminTheme/rowsIcons/2items.png"/></a></li>
    <li><a href="#"  class="row_option"  data-type="3"><img  class="rowIcon1"  src="/adminTheme/rowsIcons/3items.png"/></a></li>
  </ul>
</div>

<ul id="mainPage_rows">

  <div style="padding:30px;">Loading</div>

</ul>

<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    New row <span class="caret"></span>
  </button>

  <ul class="dropdown-menu dropdown-menu-rows">
    <li><a href="#"   class="row_option" data-type="1"><img class="rowIcon1" src="/adminTheme/rowsIcons/1item.png"/></a></li>
    <li><a href="#"  class="row_option"  data-type="2"><img  class="rowIcon1"  src="/adminTheme/rowsIcons/2items.png"/></a></li>
    <li><a href="#"  class="row_option"  data-type="3"><img  class="rowIcon1"  src="/adminTheme/rowsIcons/3items.png"/></a></li>
  </ul>
</div>
</div>

<div class="modal fade" id='myModal' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <iframe class="hiddenImageframe" onload="mainPage.frameLoad();" id="fileuploadframe" name="fileuploadframe" src=""></iframe>
      <form id="fileupload" action=""  target="fileuploadframe" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

          <div class="form-group">
            <label for="image_title">Image title</label>
            <input type="text" class="form-control" value="" name="image_title" id="image_title" placeholder="Image title">
          </div>
          <div class="form-group">
            <label for="irl_to_go">Url to go</label>
            <input type="text" class="form-control" id="url_to_go" name="url_to_go" placeholder="Url to go">
          </div>

          <div class="form-group">
            <label for="file">Select image to upload</label>
            <input type="file" name="img" class="form-control" id="file">
          </div>


          <input type="hidden" name="row_id" id="row_id"/>
          <input type="hidden" name="row_box_num" id="row_box_num"/>
          <input type="hidden" name="img_id" id="img_id"/>





      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onclick="mainPage.saveImage();">Save changes</button>
        <button type="button" class="btn btn-danger" id="btn_delete-image" onclick="mainPage.deleteImage();">Delete</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id='loadingModal' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">


      <div class="modal-body">
          Loading

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<script>

var mainPage = false;
window.onload = function() {
   $( function() {
     $( "#mainPage_rows" ).sortable({
       update: function(event, ui) {
          console.log("sort ui",ui);

          var order = [];
          $.each($(".row_li"), function(index, value) {
            var row_id = $(this).attr("data-row-id");

            order[index] = row_id;

          });

           var data = {"orders" : order};

           $.ajax({
             url: 'main-page/update-order',
             data: data,
             success : function(data) {
               console.log(data);
             },
             error : function(data) {
                console.error(data);
             }

           });

       }
     });
     $( "#mainPage_rows" ).disableSelection();
   } );


   mainPage = new MainPage();

 }



 function MainPage() {
   this.data = {};
   this.init = function() {
     var self = this;





     $(".hiddenImageframe").attr("src", location.href + "/save-image");
     $("#fileupload").attr("action", "/admin/main-page/save-image"); ///location.href + "/save-image");


     this.loadAllRows();


     $('.row_option').click(function() {

       var v    = $(this).attr("data-type");
       var data = {"row_type" : v};

       $.ajax({
         url: 'main-page/create-row',
         data: data,
         dataType: "json",
         success : function(data) {
           console.log(data);
          self.drawRows(data, true);

          $(window).scrollTop($(".main-container").height());
         },
         error : function(data) {
            console.error(data);
         }

       });
       return false;
     });




   }


   this.loadAllRows = function() {
     var data = {};
     var self = this;
     $.ajax({
       url: 'main-page/get-all-rows',
       data: data,
       dataType :"json",
       success : function(data) {
         console.log("data", data);
         self.drawRows(data);

         setTimeout(function() {

           $('#loadingModal').modal("hide");
         }, 2200);
       },
       error : function(data) {
          console.error(data);
       }

     });
   }

   this.drawRows = function(data, skipExistent) {
     var self = this;
     var html = '';
     var data2 = [];
     console.log("rowData:", data);

     for (var i in data) {
       var row = data[i];

        console.log("skipExistent == "+skipExistent+" && ", this.data[row.id]);
       if (skipExistent == true && this.data[row.id])
         continue;



       this.data[row.id] = row;

       if (row.row_type == 1)
          html += '<li class="image_row1 row_li" data-row-id="'+row.id+'" id="row_id_'+row.id+'"><div data-row-id="'+row.id+'" class="rowDelete glyphicon glyphicon-trash"></div><div class="image_row1_box" data-box="1" id="box_'+row.id+'_1" data-row-id="'+row.id+'"></div><div data-box="2" id="box_'+row.id+'_2" data-row-id="'+row.id+'" class="image_row1_box"></div><div data-box="3" id="box_'+row.id+'_3" data-row-id="'+row.id+'" class="image_row1_box"></div></li>';
       else if (row.row_type == 2)
          html += '<li class="image_row2 row_li" data-row-id="'+row.id+'"  id="row_id_'+row.id+'"><div data-row-id="'+row.id+'" class="rowDelete glyphicon glyphicon-trash"></div><div data-box="1" id="box_'+row.id+'_1"  data-row-id="'+row.id+'" class="image_row2_box"></div><div id="box_'+row.id+'_2" class="image_row2_box" data-box="2" data-row-id="'+row.id+'"></div></li>';
       else if(row.row_type == 3)
          html += '<li class="image_row3 row_li" data-row-id="'+row.id+'"  id="row_id_'+row.id+'"><div data-row-id="'+row.id+'" class="rowDelete glyphicon glyphicon-trash"></div><div class="image_row3_box" id="box_'+row.id+'_1" data-box="1" data-row-id="'+row.id+'"></div></li>';
      else if(row.row_type == 4)
          html += '<li class="image_row4 row_li" data-row-id="'+row.id+'"  id="row_id_'+row.id+'"><div data-row-id="'+row.id+'" class="rowDelete glyphicon glyphicon-trash"></div></li>';

          data2[row.id] = row;
     }


     data = data2;



     console.log("skipExistent",skipExistent);
     if (skipExistent == true)
      $("#mainPage_rows").append(html);
     else
      $("#mainPage_rows").html(html);

     setTimeout(function() {


      $(".image_row2_box, .image_row1_box, .image_row3_box").dblclick(function() {
        self.uploadDataIntobox(this);

      });

      if (skipExistent != true)
        self.drawImages(data, skipExistent);

      self.listenDeleteClicks();
     },500);
   }


   this.drawnImages = {};
   this.drawImages = function(data, skipExistent) {
     for (var i in data) {
        var row = data[i];

        if (!row.data)
          continue;

        for (var box_n in row.data) {


          var img = row.data[box_n];

          if (skipExistent == true &&   this.drawnImages[img.id]) {
            console.log("SKIP");
              continue;


          }

          this.drawnImages[img.id] = img;

          var image = new Image();
          image.src= "/images/main-page/" + img.img_url;

          var html = '<div class="image_details" id="image_details_'+row.id+'_'+box_n+'"><div class="image_details__title">' + img.img_title +'</div><div class="image_details__url">'+img.link_to+'</div></div>';

          $("#box_" + row.id +"_" + box_n).html(image);
          $("#box_" + row.id +"_" + box_n).append(html);
        }



     }


      $(".rowDelete").click(function() {

         var r = confirm("Confirm row delete");

         if (r == false)
          return false;

         var row_id = $(this).attr("data-row-id");

         $("#row_id_" + row_id).remove();

         var data = {"id" : row_id};

         $.ajax({
           url: 'main-page/delete-row',
           data: data,
           dataType: "json",
           success : function(data) {
             console.log(data);

           },
           error : function(data) {
              console.error(data);
           }

         });
      });
   }

   this.uploadDataIntobox = function(clickedObj) {
     var rowID  = $(clickedObj).attr("data-row-id");
     var boxN   = $(clickedObj).attr("data-box");
     var row    = false;
     var img    = false;
     console.log("this.datathis.datathis.data", this.data);
     for (var i in this.data) {
       var d = this.data[i];
       console.log(d.id +"=="+ rowID);
       if (d.id == rowID) {
         row = d;
         break;
       }

     }

     console.log("rowID:", rowID);
     console.log("boxN:", boxN);
     console.log("row:", row);

     if (row && row.data && row.data[boxN])
      img = row.data[boxN];



      console.log("Double clicked", img);

      $('#myModal').modal();
      if (img.id) {
        $("#myModal .modal-title").text("Edit image");
        $("#image_title").val(img.img_title);
        $("#url_to_go").val(img.link_to);
        $("#img_id").val(img.id);
        $("#btn_delete-image").show();

      }
      else {
        $("#myModal .modal-title").text("Add new image");
        $("#image_title").val('');
        $("#url_to_go").val('');
        $("#img_id").val(0);
        $("#btn_delete-image").hide();
      }

      $("#row_id").val(rowID);
      $("#row_box_num").val(boxN);

   }

   this.deleteImage = function() {
    $('#myModal').modal("toggle");
     $('#loadingModal').modal();

      var self = this;
      var data = {"id" :  $("#img_id").val()};

      $.ajax({
        url: 'main-page/delete-image',
        data: data,
        dataType: "json",
        success : function(data) {
         self.drawRows(data);

         setTimeout(function() {

           $('#loadingModal').modal("toggle");
         },500);
        }

      });
   }

   this.frameLoadsIter = 0;
   this.frameLoad = function() {

     this.frameLoadsIter++;

     console.log("this.frameLoadsIte",this.frameLoadsIter);

     if (this.frameLoadsIter  < 2)
      return false;

     this.loadAllRows();

   }

   this.saveImage = function() {
      $("#fileupload").submit();
      $('#myModal').modal("toggle");
      $('#loadingModal').modal();
   }

   this.init();

 }
 </script>
