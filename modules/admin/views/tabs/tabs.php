<label class="control-label" for="articles-status">Categories</label>
<div class="tabs">
<?php
$main_categories = array();
$n = 1;
foreach($categories as $tabs)
{
    if($tabs['parent_id'] == 0) : ?>
        <a href="#" id="<?php echo $n; ?>">
            <?php echo $tabs['category']; ?>
        </a>
    <?php
    $main_categories[] = $tabs['id'];
    $n++;
    endif;
} ?>
</div>

<?php
$array_letters_alphabet = array(
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4',
    '5', '6', '7', '8', '9',
);
?>
