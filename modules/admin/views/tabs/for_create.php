<!-- Tabs -->
<?php require_once('tabs.php'); ?>

<!-- Categories -->
<div class="row categories_list">

<?php for($i = 0; $i < count($main_categories); $i++) : ?>
<div class="tabs_block block_<?php echo $i + 1; ?>">

<?php
foreach($array_letters_alphabet as $letters) :

      foreach($categories as $isset_letter) :
          $select_category_for_letter = false;

          if($isset_letter['parent_id'] == $main_categories[$i]
          && strtolower($isset_letter['category'][0]) == $letters) :
          ?>
              <div class="letter"><?php echo $letters ?></div>
          <?php
          $select_category_for_letter = true;
          break;
          endif;
      endforeach;

      if($select_category_for_letter) : ?>

          <div class="row group_categories_one_letter">
              <?php
              $c = 1;
              foreach($categories as $item) : ?>

                  <?php if($item['parent_id'] == $main_categories[$i]
                        && strtolower($item['category'][0]) == $letters) : ?>
                  <div class="cat_b">
                      <div class="cat_row2 checkbox_for_category">
                          <input type="checkbox" name='category[<?= $c ?>]' class="art_cat_for_banner" value="<?php echo $item['id']; ?>" />
                      </div>
                      <div class="cat_row"><?php echo $item['category']; ?></div>
                  </div>
                  <?php endif;

              $c++;
              endforeach;
              ?>
          </div>
          <div class="line_under_letters"></div>

      <?php
      endif;

 endforeach; ?>

</div>
<?php endfor; ?>

</div>
<!-- > -->
