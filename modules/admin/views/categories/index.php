<?php use app\modules\admin\models\Categories; ?>

<h1>Categories</h1>
<h2>What is what rules</h2>

<div class="alert alert-warning" role="alert">It is BETA area, done fast to speed up routine work, be sure you are know what you are doing, there is no protection from fools!</div>

<ul id="categories_rules">

</ul>
<button class="btn btn-success btn-margine" id="create_new_rule">Create new rule</button>


<div id="create_new_rule_modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create new rule</h4>
      </div>
      <div class="modal-body">
        <div id="create_new_rule_section">
        <p>You may create your own category or pick existent from the list</p>
          <form action="" method="POST">
          <div class="fieldSet">
            <label>Category name:</label>
            <input type="text"  class="inputText" id="rule_categoryName" name="categoryName"/>
          </div>
          <div class="fieldSet">
            <label>Slug name:</label>
            <input type="text" class="inputText" id="rule_categorySlug"  name="categorySlug"/>
          </div>
          <div class="fieldSet"><b>ID:</b><span id="rule_categoryID" class="category_id"></span></div>
          </form>
        </div>

        <h3>Pick category from list below</h3>

        <div id="free_categories">
          <div class="fieldSet">
            <label>Search</label>
            <input class="search" />
          </div>
          <ul class="list"></ul>
        </div>

      </div>
      <div class="modal-footer">
        <div class="buttons">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="create_new_rule_submit" class="btn btn-primary">Save changes</button>
        </div>
        <div class="alert alert-info creating_category" role="alert">Loading, please wait</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class='categories_tree'>
  <h3>Make category herachly</h3>
  <button class="pickRelatedCategories" onclick="category_herachly.create_root();">+</button>
  <div id="categories_tree">

  </div>
</div>


<script>


var whatIsWhat = false;
var categories_tree = false;
var category_herachly = false;
window.onload = function() {
    whatIsWhat = new WhatIsWhat("categories_rules", "free_categories");
    launch_categories_tree();
    category_herachly = new CategoryHerachly();
}

function WhatIsWhat(rules, variants) {
  this.variants_target = $("#" + variants);
  this.variants_list   = variants;
  this.rules_cnt = rules;
  this.cat_list = false;
  this.rules = [];

  this.pickRelatedCategoriesFor = false;
  this.pickCategoryForHerachly  = false;
  this.init = function() {
     var self = this
     this.load_variants(function(data) {
       self.variants = data;
       self.draw();
     });

     this.load_rules(function(data) {
       if (data != false )
        self.rules = data;

       self.drawRules();
     });


     $("#create_new_rule").click(function(){
       self.pickRelatedCategoriesFor = false;

       $("#rule_categoryName").val("");
       $("#rule_categorySlug").val("");
       $("#rule_categoryID").html("");


       $("#create_new_rule_section").show();
       $("#create_new_rule_modal").modal("show");
     });

     $("#create_new_rule_submit").click(function() {

       if(self.pickCategoryForHerachly)
        self.create_new_herachly();
       else
        self.create_new_rule();
     })
  }

  this.create_new_herachly = function() {
    var self = this;
    var name = $("#rule_categoryName").val();
    var slug = $("#rule_categorySlug").val();
    var id   = $("#rule_categoryID").html();

    $("#create_new_rule_modal").modal("hide");

    if (self.pickCategoryForHerachly === "root"){
      $('#categories_tree').jstree(true).create_node("#", {id : id, text : name, slug : slug}, "last", function (new_node) {  category_herachly.save_categories();});
    }
    else {
      self.tree_inst.create_node(self.pickCategoryForHerachly, {id : id, text : name, slug : slug}, "last", function (new_node) {
          category_herachly.save_categories();
      });
    }
  }

  this.create_new_rule = function() {
    var name = $("#rule_categoryName").val();
    var slug = $("#rule_categorySlug").val();
    var id   = $("#rule_categoryID").html();


    if (name == "") return false;

    if (id == "") {
      $(".creating_category").show();
      $(".buttons").hide();

      var self = this;
      $.ajax({
        url : "categories/new-category",
        data : {name : name},
        dataType : "json",
        success : function(data) {
            self.addRule(data);
            $("#create_new_rule_modal").modal("hide");
            $(".creating_category").hide();
            $(".buttons").show();
        }
      });

      return false;
    }

    this.addRule({ id : id, category : name, slug : slug});
    $("#create_new_rule_modal").modal("hide");
  }


  this.rules = [];
  this.addRule = function(obj) {
    console.log("this.rules", this.rules);
    this.rules.push(obj);
    this.drawRules();
  }

  this.drawRules = function() {
    var html = '';

    for ( var i in this.rules) {

      if (this.rules[i].drawn)
        continue;

      this.rules[i].drawn = true;
      var rule = this.rules[i];
      console.log("rulerulerulerule", rule);
      if (!rule.relations)
        var relations = '';
      else {
        var relations = '';
        for (var i2 in rule.relations)
          relations += this.drawRelations(rule.relations[i2], rule.id, true);
      }


      html += '<li class="rule_id_'+rule.id+'" data-id="'+rule.id+'"><div class="category"><div class="name">'+rule.category+'</div><div class="id">Id: '+rule.id+'</div> <div class="slug">Slug: '+rule.slug+'</div></div><div class="relations"><ul class="relations_for_'+rule.id+'">'+relations+'</ul><div style="clear:both;"></div><button class="pickRelatedCategories" onclick="whatIsWhat.pickRelatedCategories('+rule.id+');">+</button></div><div style="clear:both;"></div></li>';
    }

    $("#" + this.rules_cnt).append(html);
  }

 this.drawRelations = function(selected, main_category, returnHtml) {
    var html = '<li class="relation_'+selected.id+'"><div>'+selected.category+' <div class="relations_slug"><div>Id: '+selected.id+'</div><div>Slug: '+selected.slug+'</div></div></div><div class="relations_delete" onclick="whatIsWhat.delete_relation('+selected.id+', '+main_category+');">X</div></li>';

    if (returnHtml)
      return html;

    $(".relations_for_" + main_category).append(html);

 }


 this.delete_relation = function(cat_id, main_id) {
   $(".relation_" + cat_id).remove();
   $.ajax({
     url : "categories/delete-relation",
     data : {main_category: main_id, related : cat_id}
   })

 }

  this.load_variants = function(callback) {


    $.ajax({
      url : "categories/get-categories",
      dataType : "json",
      success : callback
    });

  }


  this.load_rules = function(callback) {


    $.ajax({
      url : "categories/get-categories-rules",
      dataType : "json",
      success : callback
    })

  }


 this.pickRelatedCategories = function(id) {
   this.pickRelatedCategoriesFor = id;
   console.log("this.pickRelatedCategoriesFor ", this.pickRelatedCategoriesFor );
   $("#create_new_rule_section").hide();
   $("#create_new_rule_modal").modal("show");
 }


  this.draw = function() {
    var html = '';
    var self = this;

    for (var i in this.variants) {
      var v = this.variants[i];

      html += '<li class="item_to_select" id="item_to_select_'+v["id"]+'" data-id="'+v["id"]+'"><div class="category">'+v["category"] + '</div><div class="id">'+v["id"]+'</div><div class="slug">'+v["slug"]+'</div></li>';
    }

    $("#" + this.variants_list + " ul").html(html);

    var options = {
      valueNames: [ 'category', 'id', "slug" ]
    };

    var hackerList = new List(this.variants_list, options);

    $(".item_to_select").click(function() {
      console.log('self.pickRelatedCategoriesFor', self.pickRelatedCategoriesFor);
      console.log("pickCategoryForHerachly", self.pickCategoryForHerachly);
      if (self.pickRelatedCategoriesFor != false) {
          var selected = hackerList.get("id", $(this).attr("data-id"));
          selected = selected[0]._values;

          self.drawRelations(selected, self.pickRelatedCategoriesFor);


          $.ajax({
              url : "categories/save-relations",
              data: {parent: self.pickRelatedCategoriesFor, child: selected.id},
          });

          $(this).remove();
          var self2 = this;
          setTimeout(function() {
            hackerList.remove("id", $(self2).attr("data-id"));
          },200);
      }
      else {
        var selected = hackerList.get("id", $(this).attr("data-id"));

        selected = selected[0]._values;
        $("#rule_categoryName").val(selected.category);
        $("#rule_categorySlug").val(selected.slug);
        $("#rule_categoryID").html(selected.id);
        $(this).remove();
        var self2 = this;
        setTimeout(function() {
          hackerList.remove("id", $(self2).attr("data-id"));
        },200);
      }
    })

  }



  this.init();
}

function CategoryHerachly() {

  this.create_root = function() {

      whatIsWhat.pickCategoryForHerachly = "root";

    $("#create_new_rule_modal").modal("show");
  }

  this.save_categories_i = 0;
  this.save_categories = function() {

    this.save_categories_i++;

    if (this.save_categories_i < 2)
      return false;

    var treeData = $('#categories_tree').jstree(true).get_json('#', {flat:false, no_li_attr: true, no_a_attr:true, no_state: true});

    console.log("treeData", treeData);
    $.ajax({
    'url' : 'categories/save-categories-jstree',
    data: {"data" : treeData},
    method: "POST",
    success : function(data) {
        console.log("categories/save-categories-jstree", data);
    }});
  }
}
function launch_categories_tree() {

  $.ajax({
  'url' : 'categories/get-categories-for-jstree',
  dataType : "json",
  success : function(data) {
  categories_tree = $('#categories_tree')
  .on('redraw.jstree', function (e, data) {
    category_herachly.save_categories();
  })
  .jstree({
      "core" : {
        "animation" : 0,
        "check_callback" : true,
        "themes" : { "stripes" : true },
        'data' : data
      },
  "plugins" : [
    "contextmenu", "dnd", "search",
    "state", "types", "wholerow"
  ]
    });

    console.log("AAA", t.defaults.contextmenu.items);
  }
});



}
</script>
