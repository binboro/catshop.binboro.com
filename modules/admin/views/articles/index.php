<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\admin\models\Articles;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row actions_row">

      <div class="col-md-12">
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success btn-margine']) ?>
      </div>
      <div class="col-md-2">
        <select class="form-control" id="article_action">
          <option value="statusEnable">Enable selected</option>
          <option value="statusDisable">Disable selected</option>
          <option value="delete">Delete selected</option>
        </select>
      </div>
      <div class="col-md-10">
        <?= Html::a('OK', ['#'], ['class' => 'btn btn-primary articles-do-btn']) ?>
      </div>

    </div>


    <?php Pjax::begin(['id'=>'articles-pjax']); ?>
    <?= ListView::widget([
        'options' => [
            'tag' => 'div',
        ],
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) {
            return '<div>' . $model['title'] . '</div>';
        },
        'itemOptions' => [
            'tag' => false,
        ],
        'summary' => '',
        'layout'=>"{pager}",
        'pager' => [
            // Use custom pager widget class
            'class' => yii\widgets\demopager\DemoPager::className(),
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'prevPageLabel'=>'<',
            'nextPageLabel'=>'>',
            'maxButtonCount' => 20,
            // Options for <ul> wrapper of default pager buttons are still available
            'options' => [
                'class' => 'pagination',
                'style' => 'display:inline-block;float:left;margin:-7px 11px 20px 0;width:auto;'
            ],
            // Style for page size select
            'sizeListHtmlOptions' => [
                'class' => 'form-control paginationDropdawn',
                'style' => 'display:inline-block;float:left;margin:-12px 10px 21px 3px;;width:auto;'
            ],
            // Style for go to page input
            'goToPageHtmlOptions' => [
                'class' => 'form-control jumpPage ',
                //'style' => 'display:block;width:auto;'
            ],
        ],
    ]);
    ?>
    <?= GridView::widget([
        'id' => 'articles-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'layout'=>"{items}\n{pager}",
        'layout'=>"{summary}\n{items}",

        'columns' => [
          [
            'attribute' => '',
            'header'    => '<input type="checkbox" class="select_all_articles" />',
            'content'   => function($data)
            {
              return '<input type="checkbox" data-id="' . $data['id'] . '" class="articles_checkboxes" />';
            }
          ],
          [
            'attribute'=>'id',
            'contentOptions' => ['class'=>'id_row'],
          ],
          [
            'attribute' => 'image',
            'contentOptions' => ['class'=>'td-img'],
            'content' => function($data)
            {
              return '<a href="/admin/articles/update?id=' . $data['id'] . '" title="Update" aria-label="Update" data-pjax="0"><img src="/web/uploads/articles/' . $data['image'] . '" width="100px"></a>';
            }
          ],

          'title',
          'vendor_code',
          'path',
          [
            'attribute'=>'create_time',
            'filter' => \yii\jui\DatePicker::widget(['model'=>$searchModel,
                                                     'attribute'=>'create_time',
                                                     'dateFormat' => 'yyyy-MM-dd']),
            'content'=>function($data){
                return date('Y-m-d H:i:s', $data['create_time']);
            }
          ],



          [
            'attribute' => 'status',
            'filter' => $searchModel->statuses,
            'content' => function($data)
            {
              if($data['status'] != 0)
                return 'Enabled';
              else
                return 'Disabled';
            }
          ],

          [
            'attribute' => 'edit_status',
            'filter' => Articles::$edit_status,
            'content' => function($data)
            {
              return Articles::$edit_status[$data['edit_status']];
            }
          ],

          [
            'attribute'=>'categories',
            'header'=>'Categories',
            'content'=>function($data){

                return Articles::getCategories($data['id']);
            }
          ],


          [
            'attribute'=>'sources',
            'header'=>'Sources',
            'content'=>function($data){

                return Articles::getSourses($data['id']);
            }
          ],

          [
            'header'=>'Actions',
            'content'=>function($data){

                return  '<a href="/admin/articles/update?id=' . $data['id'] . '" title="Update" aria-label="Update" data-pjax="0">
                      <span class="glyphicon glyphicon-pencil"></span>
                  </a>
                  <a class="del-btn" href="/admin/articles/delete?id=' . $data['id'] . '" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0">
                      <span class="glyphicon glyphicon-trash"></span>
                  </a>';
            }
          ],

        ],
    ]); ?>

    <?= ListView::widget([
        'options' => [
            'tag' => 'div',
        ],
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) {
            return '<div>' . $model['title'] . '</div>';
        },
        'itemOptions' => [
            'tag' => false,
        ],
        'summary' => '',
        'layout'=>"{pager}",
        'pager' => [
            // Use custom pager widget class
            'class' => yii\widgets\demopager\DemoPager::className(),
            'firstPageLabel' => '<<',
            'lastPageLabel' => '>>',
            'prevPageLabel'=>'<',
            'nextPageLabel'=>'>',
            'maxButtonCount' => 20,
            // Options for <ul> wrapper of default pager buttons are still available
            'options' => [
                'class' => 'pagination',
                'style' => 'display:inline-block;float:left;margin:-7px 11px 20px 0;width:auto;'
            ],
            // Style for page size select
            'sizeListHtmlOptions' => [
                'class' => 'form-control paginationDropdawn',
                'style' => 'display:inline-block;float:left;margin:-12px 10px 21px 3px;;width:auto;'
            ],
            // Style for go to page input
            'goToPageHtmlOptions' => [
                'class' => 'form-control jumpPage ',
                //'style' => 'display:block;width:auto;'
            ],
        ],
    ]);
    ?>

    <?php Pjax::end(); ?>

  <?php $this->registerJsFile('@web/js/backend/articles_checkboxes.js',['depends' => [\app\assets\AdminAsset::className()]]); ?>
</div>
