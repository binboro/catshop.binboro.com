<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\controllers\AController;

use app\modules\admin\models\LoginForm;
use app\modules\admin\models\Articles;
use app\modules\admin\models\XML;
use app\modules\admin\models\MainPage;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class MainPageController extends AController
{

	public function behaviors()
	{
			return [
					'access' => [
							'class' => AccessControl::className(),
							'only' => ['index','logout','save-image'],
							'rules' => [
									[
											'actions' => ['index','logout','save-image'],
											'allow' => true,
											'roles' => ['@'],
									],
									[
										'actions' => ['index','logout'],
										'allow' => false,
										'roles' => ['?'],
									]
							],
					],/*
					 'verbs' => [
						'class' => VerbFilter::className(),
					 		'actions' => [
					 				'logout' => ['save-image'],
					 		],
					 ],*/
			];
	}

	    public function beforeAction($action)
	    {
	        $this->enableCsrfValidation = false;
	        return parent::beforeAction($action);
	    }


	    public function actionIndex()
	    {

					$MainPage = new MainPage();

	        return $this->render('index', array(
						"rows" => $MainPage->getRows()
					));
	    }

			public function actionCreateRow() {

				$MainPage = new MainPage();
				$MainPage->createRow($_REQUEST["row_type"]);
				return json_encode($MainPage->getRows());
				exit();
			}

			public function actionGetAllRows() {
					$MainPage = new MainPage();

				return json_encode($MainPage->getRows());
			}

			public function actionDeleteRow() {
				if (empty($_REQUEST["id"]))
					exit();

				$id = $_REQUEST["id"];


				$MainPage = new MainPage();
				$MainPage->deleteRow($id);
			}

			public function actionSaveImage() {
				$MainPage = new MainPage();
			 	$post = Yii::$app->request->post();

				if(!empty($_FILES['img']['name']))
				 {

					 $name =  $_FILES["img"]['name'];
					 if (file_exists("../images/main-page/" .$name))
					 	$name = rand(0,20) . date("Y-m-d")."-". $name;
					 move_uploaded_file($_FILES['img']['tmp_name'], "../images/main-page/" .$name);
					 $post["img_url"] = $name;
				 }


				$MainPage->save($post);

				exit();
			}

			public function actionUpdateOrder() {
				if (empty($_REQUEST["orders"]))
					exit();

				$orders = $_REQUEST["orders"];

				print_r($_REQUEST);
				$MainPage = new MainPage();

				$MainPage->updateOrder($orders);
			}


			public function actionDeleteImage() {

				$id = $_REQUEST["id"];
				$MainPage = new MainPage();
				$MainPage->deleteImage($id);

				return json_encode($MainPage->getRows());
				exit();

			}
}
