<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * BannersSearch represents the model behind the search form about `app\modules\admin\models\Banners`.
 */
class MainPage
{
  function getRows($time = false) {


    if ($time != false)
      $q = Yii::$app->db->createCommand("SELECT * FROM  `page_images_rows` WHERE `create_time`=" . $time . "")->queryAll();
    else
      $q = Yii::$app->db->createCommand("SELECT * FROM  `page_images_rows` ORDER BY  `order` ASC")->queryAll();

    if (empty($q))
      return array();

    foreach ($q as $r)
      $id[$r["id"]] = $r["id"];


    $images = Yii::$app->db->createCommand("SELECT * FROM  `page_images` WHERE row_id IN ('" . implode("','", $id) ."')")->queryAll();


    foreach ($q as $r) {
      $rows[ $r["id"]] = $r;
    }

    foreach ($images as $image)
      $rows[$image["row_id"]]["data"][$image["box_n"]] = $image;

    foreach ($rows as $row)
      $out[$row["order"]] = $row;
    return $out;
  }


  function createRow($type) {
     $insert = array(
       "row_type"     => $type,
       "create_time"  => time(),
     );
     $q = Yii::$app->db->createCommand("SELECT * FROM  `page_images_rows` ORDER BY  `order` DESC limit 0,1")->queryAll();
     $insert["order"] = $q[0]["order"]+1;
     Yii::$app->db->createCommand()->insert("page_images_rows", $insert)->execute();

  }


  public function save($post) {


    $data = array(
      "img_title" => $post["image_title"],
      "link_to"   => $post["url_to_go"],
      "update_time" => time(),
      "row_id" => $post["row_id"],
      "box_n" => $post["row_box_num"],
    );

    if (empty($post["img_id"])) {
      $data["create_time"] = time();
    }

    if (!empty($post["img_url"]))
      $data["img_url"] = $post["img_url"];

    if (!empty($data["create_time"]))
      Yii::$app->db->createCommand()->insert("page_images", $data)->execute();
    else
      Yii::$app->db->createCommand()->update("page_images", $data, "id=:id", array(":id"=>$post["img_id"]))->execute();

    exit();

  }

  public function updateOrder($orders) {
    foreach ($orders as $order => $row_id)
      Yii::$app->db->createCommand("UPDATE `page_images_rows` SET `order`=".$order." WHERE id='".$row_id."'")->execute();


  }

  public function deleteRow($row_id) {
    $images = Yii::$app->db->createCommand("SELECT * FROM  `page_images` WHERE row_id='".$row_id."'")->queryAll();
    Yii::$app->db->createCommand("DELETE FROM  `page_images` WHERE row_id='".$row_id."'")->execute();
    Yii::$app->db->createCommand("DELETE FROM  `page_images_rows` WHERE id='".$row_id."'")->execute();

    foreach ($images as $image)
      @unlink("../images/main-page/" . $image["img_url"]);

  }

  public function deleteImage($id) {
    $images = Yii::$app->db->createCommand("SELECT * FROM  `page_images` WHERE id='".$id."'")->queryAll();
    $images = $images[0];

    Yii::$app->db->createCommand("DELETE FROM  `page_images` WHERE id='".$id."'")->execute();

    @unlink("../images/main-page/" . $images["img_url"]);

  }
}
