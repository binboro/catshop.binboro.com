<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Sources;
use yii\data\SqlDataProvider;

/**
 * SourcesSearch represents the model behind the search form about `app\modules\admin\models\Sources`.
 */
class SourcesSearch extends Sources
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'categories'], 'integer'],
            [['name', 'site_url', 'feed_url', 'update_status', 'status', 'last_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

      $this->load($params);
      $params = (!empty($params['SourcesSearch'])) ? $params['SourcesSearch'] : [];

      $query = (new \yii\db\Query())
        ->select(['s.*'])
        ->from('sources s')
        ->groupBy('s.id');

        if(!empty($params['id']))
          $query->andWhere([ '=', 's.id', $params['id'] ]);

        if(!empty($params['name']))
          $query->andWhere([ 'like', 's.name', $params['name'] ]);

        if(!empty($params['site_url']))
          $query->andWhere([ 'like', 's.site_url', $params['site_url'] ]);

        if(!empty($params['feed_url']))
          $query->andWhere([ 'like', 's.feed_url', $params['feed_url'] ]);

        if(!empty($params['update_status']))
        {
            if($params['update_status'] == 2) {
              $query->andWhere([ '=', 's.update_status', 0 ]);
            } else {
              $query->andWhere('s.update_status = "1" OR s.update_status="Active"');
            }
        }

        if(!empty($params['status']))
        {
            if($params['status'] == 2) {
              $query->andWhere([ '=', 's.status', 0 ]);
            } else {
              $query->andWhere('s.status = "1" OR s.status="Published"');
            }
        }

        if(!empty($params['last_update']))
          $query->andWhere([ '>=', 's.last_update', strtotime($params['last_update']) ]);

        $count = $query->count();

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->getRawSql(),
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'name',
                    'site_url',
                    'feed_url',
                    'update_status',
                    'status',
                    'last_update',
                ],
                'defaultOrder' => ['id'=>SORT_DESC]
            ],
        ]);


        return $dataProvider;

        /*
        $query = Sources::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'categories' => $this->categories,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'site_url', $this->site_url])
            ->andFilterWhere(['like', 'feed_url', $this->feed_url])
            ->andFilterWhere(['like', 'update_status', $this->update_status])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
        */
    }
}
