<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\Mcurl;
use app\models\Xmlparsing;
use app\models\XmlFiles;
use yii\helpers\Url;
use app\components\Firewall;

class XmlparsingController extends Controller {
  public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		ini_set('max_execution_time', 30000);
		return parent::beforeAction($action);
	}
  public function actionIndex() {
      $xmlParsing = new Xmlparsing();

      return "da";
  }

  // For testing
  public function actionFullCicle() {

  }


  public function actionCheckFiles() {
    $XmlFiles = new XmlFiles();
    $XmlFiles->find_new_files();
  }


  public function actionParseFiles() {
    $XmlFiles = new XmlFiles();
    $XmlFiles->parse_file();
  }

  public function actionWorkWithFile() {
    $XmlFiles = new XmlFiles();

    $file = json_decode($_REQUEST['data'],1);

    $XmlFiles->file_ftell = 0;
    for ($i = 0; $i > -1; $i++)
      if ($XmlFiles->work_with_file($file[0]) == false)
        break;
  }

  public function actionPrepareCores() {
    $XmlFiles = new XmlFiles();
    $XmlFiles->prepare_core_tasks();
  }

  public function actionFirewall() {


    if (empty($_REQUEST["data"])) {
      return false;
    }

    $data = json_decode($_REQUEST["data"],1);
    $rules = json_decode($_REQUEST["params"],1);


    $Xmlparsing = new Xmlparsing();
    echo json_encode(array("data" => $Xmlparsing->check_in_firewall($data, $rules)));
  }

  public function actionFirewall2() {

    $Firewall = new Firewall(false);
    $rules = $Firewall->get_rules();

    $products = array(array("field" => "kids"));

    $Mcurl = new Mcurl();

    $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/firewall",10);
    $Mcurl->add_core("http://catwalker_api.bbserver1/xmlparsing/firewall",10);

    //$Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    //$Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    //$Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);
    //$Mcurl->add_core(Url::base(true)."/xmlparsing/firewall", 20);


    $result = $Mcurl->loop($products, $rules);

    //print_r($result);
  }


  public function actionLoadXmlString() {

    $XmlParsing = new Xmlparsing();

    $params = json_decode($_REQUEST["params"],1);


    $lines = json_decode($_REQUEST['data'],1);
    $first_line = $params["first_line"];
    $rules = $params["rules"];

    $end_line = '</merchandiser>';

    $data =  implode("", $lines) . $end_line;

    $data = str_replace("</merchandiser></merchandiser>", "</merchandiser>", $data);
    $data = $first_line . str_replace($first_line, "", $data);

    $xml = simplexml_load_string($data);

    $products_count = $XmlParsing->parse_products($xml, $params["file"], $rules);
    echo json_encode(array("data"=>$products_count));
  }


}
