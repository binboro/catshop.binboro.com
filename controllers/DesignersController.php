<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Designers;
use app\models\Categories;
use app\models\Products;
use app\models\Gender;
use app\components\helpers\UrlHelper;

class DesignersController extends Controller
{
    public function actionIndex() {
        echo 'api.catwalker.com - Designers';
    }

    public function actionGetAllDesignersAlphabet() {
  		$designers = new Designers();
  		$list_by_words = array();

  		$data = $designers->get_all_designers_in_alphabet();

  		foreach ($data as $d) {
  			if ( ($d['gender_id'] == 1 || $d['gender_id'] == 2) && isset($d['brand'][0]) ) {
  				$ind = mb_convert_encoding($d['brand'][0], 'UTF-8');
  				$list_by_words[$ind][$d['gender_id']][] = $d;
  			}
  		}

  		return json_encode($list_by_words);

  	}

    public function actionGetCategoriesForMenu() {
        $categories_m = new Categories();
        $gender_m = new Gender();
        $data = array();

        if (!isset($_REQUEST['brand_id'])) return json_encode($data);
        $brand_id  = $_REQUEST['brand_id'];

        $gender_data = $gender_m->get_women_men_data();

        $main_categories = $categories_m->get_categories_with_brand_url($brand_id);
        unset($main_categories['Designers']);

        foreach ($main_categories as $mc_k => $mc_d) {
            $tmp_prm = array(
                'parent_id' => $mc_d['id'],
                'gender_id' => $gender_data['women']['id'],
                'brand_id' => $brand_id,
            );
            $other = $mc_d;
            $other['category_name'] = 'Other';

            $tmp = $categories_m->get_children_with_brand_url($tmp_prm);
            $main_categories[$mc_k]['children']['women'] = $tmp;
            $main_categories[$mc_k]['children']['women'][] = $other;

            $tmp_prm['gender_id'] = $gender_data['men']['id'];
            $tmp = $categories_m->get_children_with_brand_url($tmp_prm);
            $main_categories[$mc_k]['children']['men'] = $tmp;
            $main_categories[$mc_k]['children']['men'][] = $other;
        }

        return json_encode($main_categories);
    }

    public function actionGetBrandById() {
        $designers_m = new Designers();
        $data = array();

        if (!isset($_REQUEST['id'])) return json_encode($data);

        $data = $designers_m->get_by_id($_REQUEST['id']);
        if (!empty($data)) $data = current($data);

        return json_encode($data);
    }

    public function actionGetProducts() {
        $products_m = new Products();
        $data = array();

        $request = (!empty($_REQUEST)) ? $_REQUEST : array();

        foreach ($request as $k => $r) {
            $products_m->setFilter($k, $r);
        }

        $data = $products_m->get_products_data();

        return json_encode($data);
    }

    // public function actionGetAllDesignersAlphabet() {
	// 	$designers = new Designers();
	// 	$list_by_words = array();
    //
	// 	$data = $designers->get_all_designers_in_alphabet();
    //
	// 	foreach ($data as $d) {
	// 		if ( ($d['gender_id'] == 2 || $d['gender_id'] == 3) && isset($d['brand'][0]) ) {
	// 			$ind = mb_convert_encoding($d['brand'][0], 'UTF-8');
	// 			$list_by_words[$ind][$d['gender_id']][] = $d;
	// 		}
	// 	}
    //
	// 	return json_encode($list_by_words);
    //
	// }

    public function actionGetAllIdsAndNames() {
        $designers_m = new Designers();

        $fields = array(
            'id', 'brand',
        );
        $designers_data = $designers_m->get_all($fields);

        return json_encode($designers_data);
    }

}
