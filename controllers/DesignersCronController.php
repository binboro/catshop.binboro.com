<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Designers;
use app\models\Categories;
use app\models\BrandUrl;

class DesignersCronController extends Controller
{
    public function actionIndex() {
        echo 'api.catwalker.com - DesignersCron';
    }

    public function actionGenerateBrandCategoryUrlRelation() {
        $designers_m = new Designers();
        $categories_m = new Categories();
        $brand_url_m = new BrandUrl();

        $designers = $designers_m->get_all();
        $active_categories = $categories_m->get_active();

        $i = 0;
        $counter = 0;
        $db_ins = array();
        foreach ($designers as $d) {
            foreach ($active_categories as $ac) {
                $db_ins[$counter][$i]['brand_id'] = $d['id'];
                $db_ins[$counter][$i]['category_id'] = $ac['id'];
                $db_ins[$counter][$i]['slug'] = $d['slug'] . '-' . $ac['slug'];
                ++$i;
                if ($i % 25000 == 0) {
                    ++$counter;
                    $i = 0;
                }
            }
        }

        $db_res = 0;
        for ($i = 0; $i <= $counter; $i++) {
            $db_res += $brand_url_m->insert_batch($db_ins[$i]);
        }

        echo $db_res;
    }
}
