<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Gender;

class GenderController extends Controller
{
    public function actionIndex() {
        echo 'api.catwalker.com - Gender';
    }

    public function actionGetGenderCategory() {
        $gender_m = new Gender();

        $data = $gender_m->get_gender_category_relations();

        return json_encode($data);
    }

    public function actionGetGenderDataByString() {
        $gender_m = new Gender();

        $gender_str = (isset($_REQUEST['gender_string'])) ? $_REQUEST['gender_string'] : '';

        $data = $gender_m->get_by_string($gender_str);

        return json_encode($data);
    }

}
