<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Categories;

class CategoriesController extends Controller
{
    public function actionIndex() {
        echo "API Categories";
    }

    public function actionShowLogs() {
        $categories = new Categories();

        $categories_count = $categories->get_categories_count();
        $regulable_categories = $categories->get_regulable_categories();
        $categories_without_relations = $categories->get_categories_without_relations();
        $categories_with_relations = $categories->get_categories_with_relations();
        $related_categories = $categories->get_related_categories();

        return $this->renderPartial('logs', [
            'count' => $categories_count,
            'regulable' => $regulable_categories,
            'without_relations' => $categories_without_relations,
            'with_relations' => $categories_with_relations,
            'related' => $related_categories,
        ]);
    }
}
