<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Filters;
use app\components\helpers\UrlHelper;

class FiltersController extends Controller
{
    public function actionIndex() {
        echo 'api.catwalker.com - Filter';
    }

    public function actionGetFilters() {
        $Filters    = new Filters();
        $UrlHelper  = new UrlHelper();

        if (!empty($_REQUEST))
          $_REQUEST = $UrlHelper->clean_params($_REQUEST);


        if (!empty($_REQUEST["gender_id"]))
          $Filters->set_param("gender_id", $_REQUEST["gender_id"]);

        if (!empty($_REQUEST["color_id"]))
          $Filters->set_param("color_id", $_REQUEST["color_id"]);




      //  if (!empty($_REQUEST["sub_category"]))
        //  $Filters->set_param("category_id", $_REQUEST["sub_category"]);



        if (!empty($_REQUEST["designer_id"]))
          $Filters->set_param("brand_id", $_REQUEST["designer_id"]);

        if (!empty($_REQUEST["designers"]))
          $Filters->set_param("brand_id", $_REQUEST["designers"]);

        if (!empty($_REQUEST["category_id"]) AND empty($_REQUEST["is_designer"]) AND empty($_REQUEST["is_designers"]))
          $Filters->set_param("category_id", $_REQUEST["category_id"]);

        if (!empty($_REQUEST["category_id"]) AND !empty($_REQUEST["is_designer"]) AND !empty($_REQUEST["is_designers"]))
          $Filters->set_param("brand_id", $_REQUEST["category_id"]);

        if (!empty($_REQUEST["category_id"]) AND empty($_REQUEST["is_designers"]) AND empty($_REQUEST["is_designers"]))
          $Filters->set_param("category_id", $_REQUEST["category_id"]);

        if (!empty($_REQUEST["category_id"]) AND !empty($_REQUEST["is_designers"]))
          $Filters->set_param("brand_id", $_REQUEST["category_id"]);

        $filters = $Filters->get_all_filters_data();

        return json_encode($filters);
    }

}
