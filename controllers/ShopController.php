<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Request;
use yii\data\Pagination;
use app\models\Products;
use app\models\Designers;
use app\models\Categories;
use app\models\Colors;
use app\models\Size;
use app\models\parsingXml;
use app\models\BrandUrl;
use app\models\Gender;
use app\components\Firewall;
use app\components\Log;
use app\components\Slug;
use app\components\Sql;
use yii\helpers\Url;
use app\components\helpers\UrlHelper;

class ShopController extends Controller
{

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		echo "Empty page";
	}

	public function actionGetCategoryName() {


		if(empty($_REQUEST['slug']))
			return false;

		$slug = $_REQUEST['slug'];
		$name = (new Categories())->getBySlug($slug);
		return $name['category'];
	}


	/*
		Parsing XML
	*/
	public function actionXmlParse() {
		//set_time_limit(14400);
		ini_set('memory_limit', '3000M');
		echo "startTime : " . date('Y-m-d H:i:s', time());
		$time = time();

		$parsing = new parsingXml();
		$files = $parsing->getNextFile();

		if(empty($files)) {
			$parsing->removeAllFilesData();
			$files = $parsing->getNextFile();
		}

		// if(is_array($file)) {
		// 	$parsing->offset = $file['item_processed'];
		// 	$file = $file['file_name'];
		// }
		foreach ($files as $key => $file) {
			$parsing->offset = 0;
			$parsing->startParse($file);

			$resultTime = time() - $time;
			echo '<br />sec: ' . $resultTime;
		}


		echo "Parsing complete";
		echo "<br />startTime : " . date('Y-m-d H:i:s', time());
		//header('Location: ' . Url::to(['shop/xml-parse'], true));
		exit();

	}

	/**
	 * Checks are categories exist
	 *
	 * @param array $categories Input categories data from XML
	 *
	 * @return array $ret Formated array to be simple in use and able to insert batch
	 */
	private function actionCategoryCheking($categories = array()) {
		$ret = array();

		if (empty($categories)) {
			return $ret;
		}

		$sql = 'SELECT * FROM `categories`;';
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();
		foreach ($categories_data as $c_d) {
			$category = $c_d['category'];
			if (isset($categories[$category])) {
				unset($categories[$category]);
				continue;
			}
		}

		if (empty($categories)) {
			return $ret;
		}

		foreach ($categories as $c) {
			$ret[$c][$c] = $c;
			$tmp_slug = $this->slugify($c);
			$slug = $this->slugifyChecking($tmp_slug);
			$ret[$c]['slug'] = $this->slugify($slug);
		}

		return $ret;
	}

	/**
	 * Checks are products exist
	 *
	 * @param array $products Input products data from XML
	 *
	 * @return array $ret Formated array to be simple in use and able to insert batch
	 */
	private function actionProductCheking($products = array()) {
		$ret = array();
		if (empty($products)) {
			return $ret;
		}
		$sql = 'SELECT `product_id` FROM `products`;';
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		foreach ($products_data as $p_d) {
			$p_id = $p_d['product_id'];
			if (in_array($p_id, $products)) {
				$ret[$p_id] = $p_id;
			}
		}

		return $ret;
	}

	/**
	 * Get all categories for shop
	 *
	 * @return array
	 */
	public function actionGetAllCategories() {

		$categories_m = new Categories();
		$categories = $categories_m->getAll();

		return json_encode($categories);
	}

	/**
	 * Get product data for paginate
	 *
	 * @return array
	 */
	public function actionPaginateData() {
		$ret = array(
			'count' => 0,
			'products' => array(),
		);

		$offset = 0;
		if (!empty($_REQUEST['offset'])) {
			$offset = trim($_REQUEST['offset']);
		}
		$limit = 10;
		if (!empty($_REQUEST['limit'])) {
			$limit = trim($_REQUEST['limit']);
		}
		$where = '';

		if (!empty($_REQUEST['where']) && !empty($_REQUEST['needful'])) {
			$where = "WHERE `" .trim($_REQUEST['where']) . "` = " . "'" . trim($_REQUEST['needful']) . "'";
		}
		$sql = 'SELECT COUNT(*) FROM `products`' . $where . ';';
		$count = Yii::$app->db->createCommand($sql)->queryAll();
		$sql = 'SELECT * FROM `products` ' . $where . ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
		$query = Yii::$app->db->createCommand($sql)->queryAll();

		$ret['count'] = $count[0]['COUNT(*)'];
		$ret['products'] = $query;
		return json_encode($ret);
	}

	/**
	 * Get some random products
	 *
	 * @return array
	 */
	public function actionGetRandomProduct() {
		$rand_data = (new Products())->getRandomRelatedProduct();
		return json_encode($rand_data);
	}

	private function slugify($text) {
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if (empty($text)) {
			return uniqid();
		}

		return $text;
	}

	/**
	* Checks slug existing in categories or products table
	*
	* @param string $slug
	*
	* @return string Checked slug (if is not checked will add suffix)
	*/
	private function slugifyChecking($slug) {
		$checked_slug = '';
		if (empty($slug)) {
			return $checked_slug;
		}

		$sql = "SELECT * FROM `categories` WHERE `slug` LIKE '" . $slug . "'";
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($categories_data)) {
			foreach ($categories_data as $c_d) {
				if ($c_d['slug'] == $slug) {
					$checked_slug = $slug . '-' . time();
				}
			}
		}
		$sql = "SELECT products.*, URL_productImage as image FROM `products` WHERE `slug` LIKE '" . $slug . "'";
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($products_data)) {
			foreach ($products_data as $p_d) {
				if ($p_d['slug'] == $slug) {
					$checked_slug = $slug . '-' . time();
				}
			}
		}
		if (empty($checked_slug)) {
			$checked_slug = $slug;
		}

		return $checked_slug;
	}

	/**
	* Check place of data in DB by slug
	*
	* @param string $slug
	*
	* @return array place with important data
	*/
	public function actionCheckBySlug() {
		$Sql = new Sql();
		$brand_url_m = new BrandUrl();
		$products_m = new Products();
		$gender_m = new Gender();
		$ret = array(
			'place' => '',
			'data' => array(),
		);

		if (empty($_REQUEST['slug']) && empty($_REQUEST['gender'])) {
			return json_encode($ret);
		}



		$gender = !empty($_REQUEST['gender']) ?  $_REQUEST['gender'] : '';
		$slug = !empty($_REQUEST['slug']) ?  $_REQUEST['slug'] : '';


		$products_data = $products_m->get_product_by_slug($slug);
		if (!empty($products_data)) {
			$ret['place'] = 'products';
			$ret['data'] = end($products_data);
			return json_encode($ret);
		}



		$categories = new Categories();
		if(!empty($gender) && empty($slug)) {
			$gender_data = $gender_m->get_by_string($gender);

			$ret['place'] = 'categories';
			$ret['data'] = array(
				'category' => '',
				'slug' => '',
				'id' => 0,
			);

			return json_encode($ret);
		}


		$brand_url_data = $brand_url_m->get_by_slug($slug);
		if (!empty($brand_url_data)) {
			$ret['place'] = 'designers';
			$ret['data'] = end($brand_url_data);
			return json_encode($ret);
		}

		$sql = "SELECT * FROM `categories` WHERE `slug` = '" . $slug . "';";
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();


		// Get parent cateogry
		if (!empty($categories_data[0]))
		$parent_category = $Sql->select("SELECT * FROM categories_hierarchy
			 LEFT JOIN categories ON categories.id=categories_hierarchy.parent_category
			 WHERE category_id=:id", array("id" => $categories_data[0]["id"]));

		if (empty($parent_category)) $parent_category = false;
		else  $parent_category = $parent_category[0];

		if (!empty($categories_data)) {
			$ret['place'] = 'categories';
			$ret['data'] = end($categories_data);
			$ret['parent'] =$parent_category;
			return json_encode($ret);
		}

		$brands_data = Yii::$app->db->createCommand("SELECT *, brand as category_ FROM `brands` WHERE `slug` = '" . $slug . "';")->queryAll();
		if (!empty($brands_data)) {
			$ret['place'] = (!empty($_REQUEST['designers_flag'])) ? 'designers' : 'categories';
			$ret['data'] = end($brands_data);
			return json_encode($ret);
		}



		return json_encode($ret);
	}

	/**
	* Get products (if db has some records with common group that return group)
	*
	* @return array
	*/
	public function actionTestF() {
		$Firewall = new Firewall(true);



		$data = array(
			array("merchant_id" => 1, "name" =>"total~kids~neard"),
			"DAD" => array("merchant_id" => 3, "name" =>"total~k2ids~neard")
		);

		$Firewall->protect($data);

		print_r($data);

	}

	public function actionGetProduct() {
		$products_m = new Products();
		$product = array();

		if (empty($_REQUEST['slug'])) {
			return json_encode($ret);
		}

		$slug = $_REQUEST['slug'];
		$product = $products_m->get_product_by_slug($slug);

		return json_encode($product);
	}

	public function actionGetShopData() {
		$UrlHelper = new UrlHelper();
		$Products = new Products();

		// Filter wrong gender values

		if (!empty($_REQUEST["gender_id"]) AND $_REQUEST["gender_id"] == "women")
			$_REQUEST["gender_id"] = 1;
		else if (!empty($_REQUEST["gender_id"]) AND $_REQUEST["gender_id"] == "men")
			$_REQUEST["gender_id"] = 2;
		else if (!empty($_REQUEST["gender"]) AND $_REQUEST["gender"] == "women")
				$_REQUEST["gender_id"] = 1;
		else if (!empty($_REQUEST["gender"]) AND $_REQUEST["gender"] == "men")
				$_REQUEST["gender_id"] = 2;

		if (!empty($_REQUEST["designers"]) AND $_REQUEST["designers"] != "all")
				$_REQUEST["brand_id"] = $_REQUEST["designers"];

		if (!empty($_REQUEST["color"]) AND $_REQUEST["color"] != "all")
				$_REQUEST["color_id"] = $_REQUEST["color"];

		if (!empty($_REQUEST["size"]) AND $_REQUEST["size"] != "all")
				$_REQUEST["size_id"] = $_REQUEST["size"];

		if (!empty($_REQUEST["sub_category"]) AND $_REQUEST["sub_category"] != "all")
				$_REQUEST["category_id"] = $_REQUEST["sub_category"];

		$filterArrays = array(
			"gender_id",
			"color_id",
			"search",
			"category_id",
			"size_id",
			"brand_id",
			"minPrice", "maxPrice",
			"offset", "limit",
		);

		$params = $UrlHelper->clean_params($_REQUEST);

		foreach ($filterArrays as $field)
			if (!empty($params[ $field ]))
				if ($params[ $field ] != "all")
					$Products->setFilter($field, $params[ $field ]);

		if (!empty($_REQUEST["xml"]))
			return json_encode($Products->get_products_data_xml());
		else
			return json_encode($Products->get_products_data());
	}

	public function actionAllProdColors() {
		$products = new Products();
		$where = $products->getWhere();
		$tmp_colors = $products->getProductColor($where);
		$colors = array();
		foreach ($tmp_colors as $t_c) {
			$color = $t_c['color'];
			$colors[$color] = $color;
		}
		return json_encode($colors);
	}

	public function actionMaxPrice() {
		$products = new Products();
		$where = $products->getWhere();
		$tmp_price = $products->getProductPrice($where);
		$max_pr = array();
		foreach ($tmp_price as $tmp_p) {
			$max_pr[] = $tmp_p['price_retail'];
		}
		$max_pr = max($max_pr);
		return $max_pr;
	}

	/**
	* In order to fill brands table
	*
	* @return bool
	*/
	public function actionFillBrands() {
		(new Products())->fillBrandsTable();
		return true;
	}

	/**
	* In order to fill related table product_brand table
	*
	* @return bool
	*/
	public function actionFillProductBrand() {
		(new Products())->fillProductBrandTable();
		return true;
	}

	public function actionDesignersForFilters() {
		$Designers = new Designers();
		return json_encode($Designers->get_designers_for_filters());
	}

	public function actionGetDesignersList() {
		$designers = new Designers();
		$return = array();

		if (empty($_REQUEST['gender'])) {
			return json_encode($return);
		}
		$gender = $_REQUEST['gender'];
		$designers_list = $designers->designers_list($gender);
		$list_by_words = array();
		foreach ($designers_list as $d_l) {
			$d_name = $d_l['brand'];
			if (isset($d_name[0])) {
				$ind = mb_convert_encoding($d_name[0], 'UTF-8');
				$list_by_words[$ind][] = $d_l;
			}
		}
		$return = json_encode($list_by_words);
		return $return;
	}

	public function actionColorsForFilters() {
		$colors = new Colors();
		$return = $colors->get_colors_for_filters();
		return json_encode($return);
	}

	public function actionSizeForFilters() {
		$colors = new Size();
		$return = $colors->get_size_for_filters();
		return json_encode($return);
	}

	public function actionCategoriesForFilters() {
		$categories = new Categories();
		$return = $categories->get_categories_for_filters();
		return json_encode($return);
	}



	public function actionGetRandomRelatedProductByBrand() {
		$products = new Products();
		$rand_data = $products->actionGetRandomRelatedProductByBrand();
		return json_encode($rand_data);
	}

	public function actionGetProductBreadCrumbs() {
		$category_m = new Categories();
		$data = array();

		if (empty($_REQUEST)) return json_encode($data);

		$data = $category_m->get_breadcrumbs_for_product();

		return json_encode($data);
	}

}
