<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Products;
use app\models\Categories;

class ProductController extends Controller
{
    public function beforeAction($action) {

      $this->enableCsrfValidation = false;
      ini_set('max_execution_time', 30000);
      return parent::beforeAction($action);
    }

    public function actionIndex() {
        echo "API Product";
    }

    public function actionGetColorSize() {
        $products_m = new Products();

        $data = $products_m->get_product_color_size_by_product_id($_REQUEST['product_id']);

        return json_encode($data);
    }

    public function actionGetRandomRelatedProduct() {

        $products_m = new Products();

        $data = $products_m->get_rand_related_product($_REQUEST);

        return json_encode($data);
    }

    public function actionGetProductBreadCrumbs() {
        $category_m = new Categories();

        $data = $category_m->get_breadcrumbs($_REQUEST);

        return json_encode($data);
    }

}
