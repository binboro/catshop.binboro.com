<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\components\Sql;
use app\components\Firewall;
use app\components\Arrays;
use app\components\Log;
use app\components\Mcurl;
use yii\helpers\Url;


class FirewallController extends Controller
{

  function actionCheckProductsInFirewallBycore(){
    if (empty($_REQUEST["core"]))
      exit();

    $Lg = new Log(true);
    $FW = new Firewall();
    $Array = new Arrays();
    $Sql = new Sql();
    $Sql->enable_log = true;
    $limit = $_REQUEST["limit"];
    $offset = $limit * ($_REQUEST["core"]-1);
    $Sql->set_limit($offset, $limit);
    $q = $Sql->sl("products");

    if (empty($q))
      return false;

    $Lg->show("Query is done");

    $checked = $q;
    $FW->protect($checked);
    $Lg->show("Protect is done");

    $Array->groupBy($checked, "id");
    $Lg->show("groupBy is done");



    $delete = array();

    foreach ($q as $r)
      if (empty($checked[$r["id"]])) {
        $delete[] = $r["id"];
      }


    if (!empty($delete))
      print_r($delete);

    $Sql->delete("DELETE FROM products WHERE id IN (in:id)", array("in:id" => $delete));

    echo count($delete);
  }
  function actionCheckProductsInFirewall() {
    $base  = Url::base(true);
    $Lg  = new Log(true);
    $Sql = new Sql();
    $cnt = $Sql->select("SELECT count(id) as cnt FROM products");
    $cnt = $cnt[0]["cnt"];
    $limit = 50000;

    if ($cnt > 4)
      $cores = ceil($cnt/$limit);
    else
      $cores = 1;



    $Lg->show("Start steps");

    $Mcurl = new Mcurl();
    $Mcurl->cores($cores);
    $Mcurl->add_same($base .'/firewall/check-products-in-firewall-bycore','limit='. $limit );
    $result = $Mcurl->launch(false);

    print_r($result);


    $Lg->show("Finish steps");


  }


  function checking_products($offset,$limit) {
    $Lg     = new Log(true);
    $Array  = new Arrays();
    $FW     = new Firewall();
    $Sql    = new Sql();
    $Sql->enable_log = true;

    $Lg->show("Start");
    $Sql->set_limit($offset, $limit);
    $q = $Sql->sl("products");

    if (empty($q))
      return false;

    $Lg->show("Query is done");

    $checked = $q;
    $FW->protect($checked);
    $Lg->show("Protect is done");

    $Array->groupBy($checked, "id");
    $Lg->show("groupBy is done");



    $delete = array();

    foreach ($q as $r)
      if (empty($checked[$r["id"]]))
        $delete[] = $r;

    if (!empty($delete))
      print_r($delete);

    return true;
  }


}
