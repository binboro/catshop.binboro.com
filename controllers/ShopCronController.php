<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\web\Request;
use app\models\Products;
use app\models\Designers;
use app\models\ProductsCron;
use app\models\SizeCron;
use app\models\CategoriesCron;
use app\models\Categories;
use app\components\Slug;
use app\components\Sql;
use app\components\Log;
use app\components\Mcurl;
use app\models\parsingXml;
use app\models\PrepareProducts;
use app\models\index_products\IndexBrands;
use app\models\index_products2\IndexFilter;
use app\models\index_products2\IndexBrandsVisibility;
use app\models\index_products2\GroupProducts;
use yii\helpers\Url;

class ShopCronController extends Controller
{

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		ini_set('max_execution_time', 30000);
		return parent::beforeAction($action);
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */

	public function actionCustomSql() {

		$sql ="ALTER TABLE `xml_products` ADD `is_indexed` INT(11) NOT NULL DEFAULT '0', ADD INDEX (`is_indexed`);";
		Yii::$app->db->createCommand($sql)->execute();

	}

	public function  actionPrepareCores() {
		$Products = new ProductsCron();
		$Products->prepare_core_tasks();
	}


	public function  actionIndexFilter() {
		$IndexFilter = new IndexFilter();
		if (empty($_REQUEST["core"]))
			$core = false;
		else
			$core = $_REQUEST['core'];

		$IndexFilter->set_core($core);

		$IndexFilter->start();
	}

	public function actionTest() {

		$Mcurl = new Mcurl();

		$Lg = new Log(true);
		$Lg->show("Start");
		$data = array();

		//for ($i = 0; $i < 2000000; $i++) {
		for ($i = 0; $i < 200; $i++) {
			$data[] = "Time to go to sleep in iters: " . $i;
		}


		$Lg->show("End");
		$Lg->show("S foreacg");

		//foreach ($data as &$v) {
			//$v = array("text" =>$v);
		//}



/*
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 16);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 16);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 16);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 20);
		$Mcurl->add_core("http://catwalker_api.bbserver1/shop-cron/test2",16);
		$Mcurl->add_core("http://catwalker_api.bbserver1/shop-cron/test2",16);

		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
*//**/
		$Mcurl->add_core("http://catwalker_api.bbserver1/shop-cron/test2",20);
		$Mcurl->add_core("http://catwalker_api.bbserver1/shop-cron/test2",20);
		$Mcurl->add_core("http://catwalker_api.bbserver1/shop-cron/test2",20);

		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);
		$Mcurl->add_core(Url::base(true)."/shop-cron/test2", 10);


		$result = $Mcurl->loop($data);
		print_r($result);
		echo count($result);




	$Lg->show("E foreacg2");
	//	print_r($result);


	}

	public function actionTest2() {


		$data = json_decode($_REQUEST["data"],1);

		foreach ($data as &$r) {
		  for ($i = 0; $i < 10; $i++)
				$r = "Yes, it works " .md5(md5(md5(md5( md5(rand()) . md5(md5(rand(1,10))))))).$i;
		}


		return json_encode(array("data" => $data));

	}

	public function actionIndexProducts() {
			$Products = new ProductsCron();


			for ($i = 0; $i > -1; $i++)
				if ($Products->indexProduct($i) === true)
					break;



			if (!empty($_REQUEST["reload"]) && $cnt > 0) {
				echo '
					<script>

					var t = setTimeout(function() {location.href=location.href;}, 250);

					 document.addEventListener("click", function() {
						 clearTimeout(t);
					 });
					</script>
				';
				exit();
			}


			if (!empty($_REQUEST["reload"]) && $cnt == 0) {
				echo ' Done';
			}
	}

	public function actionIndexVisibility() {
		$Products = new ProductsCron();
		if ($Products->indexVisibility() === true) {
			sleep(1);
			$this->actionIndexVisibility();
		}
	}

	public function actionGetInvisibleProduct() {
		$Products = new ProductsCron();
		$Products->getInvisibleProduct();
	}

	public function actionIndexCategoriesGender() {
		$Products = new ProductsCron();
		$Products->indexProduct();
	}

	public function actionMakeBrandsSlug() {
		$IndexBrands = new IndexBrands();
		$IndexBrands->make_brand_slugs();
	}

	public function actionIndex()
	{
		echo "Empty page";
	}

	public function actionParseItemsFromXmlTable() {
		echo "da";
	}

	public function actionGetCategoryName() {


		if(empty($_REQUEST['slug']))
			return false;

		$slug = $_REQUEST['slug'];
		$name = (new Categories())->getBySlug($slug);
		return $name['category_name'];
	}


	public function actionSetShowFlag() {
		$result = (new ProductsCron())->setShowFlug();
	}



	/*
		Parsing XML
	*/
	public function actionXmlParse() {
		//set_time_limit(14400);
		ini_set('memory_limit', '3000M');
		echo "startTime : " . date('Y-m-d H:i:s', time());
		$time = time();

		$parsing = new parsingXml();
		$files = $parsing->getNextFile();

		if(empty($files)) {
			$parsing->removeAllFilesData();
			$files = $parsing->getNextFile();
		}

		// if(is_array($file)) {
		// 	$parsing->offset = $file['item_processed'];
		// 	$file = $file['file_name'];
		// }
		foreach ($files as $key => $file) {
			$parsing->offset = 0;
			$parsing->startParse($file);

			$resultTime = time() - $time;
			echo '<br />sec: ' . $resultTime;
		}


		echo "Parsing complete";
		echo "<br />startTime : " . date('Y-m-d H:i:s', time());
		//header('Location: ' . Url::to(['shop/xml-parse'], true));
		exit();

	}

	/**
	 * Checks are categories exist
	 *
	 * @param array $categories Input categories data from XML
	 *
	 * @return array $ret Formated array to be simple in use and able to insert batch
	 */
	private function actionCategoryCheking($categories = array()) {
		$ret = array();

		if (empty($categories)) {
			return $ret;
		}

		$sql = 'SELECT * FROM `categories`;';
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();
		foreach ($categories_data as $c_d) {
			$category_name = $c_d['category_name'];
			if (isset($categories[$category_name])) {
				unset($categories[$category_name]);
				continue;
			}
		}

		if (empty($categories)) {
			return $ret;
		}

		foreach ($categories as $c) {
			$ret[$c][$c] = $c;
			$tmp_slug = $this->slugify($c);
			$slug = $this->slugifyChecking($tmp_slug);
			$ret[$c]['slug'] = $this->slugify($slug);
		}

		return $ret;
	}

	/**
	 * Checks are products exist
	 *
	 * @param array $products Input products data from XML
	 *
	 * @return array $ret Formated array to be simple in use and able to insert batch
	 */
	private function actionProductCheking($products = array()) {
		$ret = array();
		if (empty($products)) {
			return $ret;
		}
		$sql = 'SELECT `product_id` FROM `products`;';
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		foreach ($products_data as $p_d) {
			$p_id = $p_d['product_id'];
			if (in_array($p_id, $products)) {
				$ret[$p_id] = $p_id;
			}
		}

		return $ret;
	}

	/**
	 * Get all categories for shop
	 *
	 * @return array
	 */
	public function actionGetAllCategories() {

		$categoriesModel = new Categories();
		$categories = $categoriesModel->getAll();
		return json_encode($categories);
	}

	/**
	 * Get product data for paginate
	 *
	 * @return array
	 */
	public function actionPaginateData() {
		$ret = array(
			'count' => 0,
			'products' => array(),
		);

		$offset = 0;
		if (!empty($_REQUEST['offset'])) {
			$offset = trim($_REQUEST['offset']);
		}
		$limit = 10;
		if (!empty($_REQUEST['limit'])) {
			$limit = trim($_REQUEST['limit']);
		}
		$where = '';

		if (!empty($_REQUEST['where']) && !empty($_REQUEST['needful'])) {
			$where = "WHERE `" .trim($_REQUEST['where']) . "` = " . "'" . trim($_REQUEST['needful']) . "'";
		}
		$sql = 'SELECT COUNT(*) FROM `products`' . $where . ';';
		$count = Yii::$app->db->createCommand($sql)->queryAll();
		$sql = 'SELECT * FROM `products` ' . $where . ' LIMIT ' . $limit . ' OFFSET ' . $offset . ';';
		$query = Yii::$app->db->createCommand($sql)->queryAll();

		$ret['count'] = $count[0]['COUNT(*)'];
		$ret['products'] = $query;
		return json_encode($ret);
	}

	/**
	 * Get some random products
	 *
	 * @return array
	 */
	public function actionGetRandomProduct() {
		$rand1 = mt_rand(1, 10000);
		$rand2 = mt_rand(10000, 20000);
		$sql = 'SELECT * FROM products where id between '.$rand1.' and '.$rand2.'   ORDER BY RAND() LIMIT 4';
		//$sql = 'SELECT * FROM `products` ORDER BY RAND() LIMIT 4';
		$rand_data = Yii::$app->db->createCommand($sql)->queryAll();
		return json_encode($rand_data);
	}

	private function slugify($text) {
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		$text = trim($text, '-');
		$text = preg_replace('~-+~', '-', $text);
		$text = strtolower($text);

		if (empty($text)) {
			return uniqid();
		}

		return $text;
	}

	/**
	* Checks slug existing in categories or products table
	*
	* @param string $slug
	*
	* @return string Checked slug (if is not checked will add suffix)
	*/
	private function slugifyChecking($slug) {
		$checked_slug = '';
		if (empty($slug)) {
			return $checked_slug;
		}

		$sql = "SELECT * FROM `categories` WHERE `slug` LIKE '" . $slug . "'";
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($categories_data)) {
			foreach ($categories_data as $c_d) {
				if ($c_d['slug'] == $slug) {
					$checked_slug = $slug . '-' . time();
				}
			}
		}
		$sql = "SELECT * FROM `products` WHERE `slug` LIKE '" . $slug . "'";
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($products_data)) {
			foreach ($products_data as $p_d) {
				if ($p_d['slug'] == $slug) {
					$checked_slug = $slug . '-' . time();
				}
			}
		}
		if (empty($checked_slug)) {
			$checked_slug = $slug;
		}

		return $checked_slug;
	}

	/**
	* Check place of data in DB by slug
	*
	* @param string $slug
	*
	* @return array place with important data
	*/
	public function actionCheckBySlug() {
		$ret = array(
			'place' => '',
			'data' => array(),
		);

		if (empty($_REQUEST['slug']) && empty($_REQUEST['gender'])) {
			return json_encode($ret);
		}

		$gender = !empty($_REQUEST['gender']) ?  $_REQUEST['gender'] : '';
		$slug = !empty($_REQUEST['slug']) ?  $_REQUEST['slug'] : '';


		$categories = new Categories();
		if(!empty($gender) && empty($slug)) {
			$ret['place'] = 'categories';
			$ret['data'] = ['category_name' => $gender, 'slug' =>'', 'id' => $gender];
			return json_encode($ret);
		}


		$sql = "SELECT * FROM `categories` WHERE `slug` = '" . $slug . "';";
		$categories_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($categories_data)) {
			$ret['place'] = 'categories';
			$ret['data'] = end($categories_data);
			$ret['parent'] = $categories->findInArray($ret['data']['category_name'], true);
			return json_encode($ret);
		}

		$brands_data = Yii::$app->db->createCommand("SELECT *, brand as category_name FROM `brands` WHERE `slug` = '" . $slug . "';")->queryAll();
		if (!empty($brands_data)) {
			$ret['place'] = 'categories';
			$ret['data'] = end($brands_data);
			return json_encode($ret);
		}

		$sql = "SELECT * FROM `products` WHERE `slug` = '" . $slug . "';";
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($products_data)) {
			$ret['place'] = 'products';
			$ret['data'] = end($products_data);
			return json_encode($ret);
		}

		return json_encode($ret);
	}

	/**
	* Get products (if db has some records with common group that return group)
	*
	* @return array
	*/
	public function actionGetProduct() {
		$ret = array();

		if (empty($_REQUEST['slug'])) {
			return json_encode($ret);
		}

		$slug = $_REQUEST['slug'];

		$sql = "SELECT * FROM `products` WHERE `slug` = '" . $slug . "';";
		$products_data = Yii::$app->db->createCommand($sql)->queryAll();
		if (!empty($products_data)) {
			$prod = end($products_data);
			$sku_number = $prod['sku_number'];
			if($sku_number > 0){
				$sql = "SELECT * FROM `products` WHERE `sku_number` = '" . $sku_number . "';";
				$products_data = Yii::$app->db->createCommand($sql)->queryAll();
			}

			$ret = $products_data;

		}

		return json_encode($ret);
	}

	public function actionGetShopData() {
		$products = new Products();

		$tmp_prm = array();
		$tmp_prm['where'] = $products->getWhere();

		if (!empty($_REQUEST['offset'])) {
			$tmp_prm['offset'] = $_REQUEST['offset'];
		}
		if (!empty($_REQUEST['limit'])) {
			$tmp_prm['limit'] = $_REQUEST['limit'];
		}
		$prod_data = $products->getProductsData($tmp_prm);

		return json_encode($prod_data);
	}

	public function actionAllProdColors() {
		$products = new Products();
		$where = $products->getWhere();
		$tmp_colors = $products->getProductColor($where);
		$colors = array();
		foreach ($tmp_colors as $t_c) {
			$color = $t_c['color'];
			$colors[$color] = $color;
		}
		return json_encode($colors);
	}

	/**
	* In order to fill brands table
	*
	* @return bool
	*/
	public function actionFillBrands() {
		(new ProductsCron())->fillBrandsTable();
		return true;
	}

	public function actionCountProductsDesigners() {
	  (new Designers())->count_products_in_designers();
		return true;
	}

	/**
	* In order to fill size table
	*
	* @return bool
	*/
	public function actionFillSize() {
		(new SizeCron())->fill_size_table();
		return true;
	}

	/**
	* In order to fill sku_number_id fields in some tables
	*
	* @return bool
	*/
	public function actionFillSkuNumberInProductCategory() {
		(new CategoriesCron())->fill_sku_number();
		return true;
	}

	/**
	* In order to fill slug field if is not exists
	*
	* @return bool
	*/
	public function actionFillEmptySlug() {
		(new ProductsCron())->fill_empty_slug();
		return true;
	}

	public function actionIndexBrandsVisibility() {
		$IndexBrandsVisibility = new IndexBrandsVisibility();
		return $IndexBrandsVisibility->start();
	}

	public function actionExportDb() {
		$Sql = new Sql();

		if (!file_exists("/tmp_ram/mysql_dump"))
			mkdir("/tmp_ram/mysql_dump");

		$Sql->export_database_data(false, "/tmp_ram/");
	}

	public function actionImportDb() {
		$Sql = new Sql();
		$Sql->import_database("/tmp/");
	}

	public function actionGroupProducts() {
		$gp = new GroupProducts();
		if ($gp->start() === false)
			return false;
		else
			$this->actionGroupProducts();
	}

	public function actionGroupProductsGetData() {
		$gp = new GroupProducts();
		$gp->get_data($_REQUEST["limit"], $_REQUEST["core"]);
	}

	public function actionGroupProductsSave() {

		if (empty($_REQUEST["data"]))
			return false;


		$data = json_decode($_REQUEST["data"],1);

		$gp = new GroupProducts();
		$gp->save($data);
		/*$gp = new GroupProducts();
		$gp->save($_REQUEST["limit"], $_REQUEST["core"]);*/
	}
}
