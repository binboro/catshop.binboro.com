<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Categories;
use app\models\Products;
use yii\db\QueryBuilder;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
////////////////////////////////////////////////////////////////////////////////
      $allBrends = (new Products())->allBrands();

      //var_dump($allBrends);

      // $sql = ->insert('brands', [
  		//     'id' => '1',
  		//     'brand' => 30,
  		// ]);

      if (Yii::$app->user->isGuest) {
          return $this->redirect(['site/login']);
      }

      $categories = new Categories();
      $categoriesAll = $categories->getAllCategoriesIds();
      $categoriesHierarchy = $categories->getCategoriesWithHierarhy();
      $categoryRelated = $categories->getRelated();

      //$html = $this->generateList($categoriesHierarchy, 0, 0, $categoriesAll, $categoryRelated);


      // return $this->render('index',
      // [
      //   'html' => $html,
      // ]);

      return $this->render('index',
      [
        'categoriesHierarchy' => $categoriesHierarchy,
        'categoriesAll' => $categoriesAll,
        'categoryRelated' => $categoryRelated,
      ]);
    }

    public function generateList($data, $parent, $level, $categoriesAll, $related) {
      $html = '';
      foreach ($data[$parent] as $key => $value) {
        $html .= $this->renderPartial('__category', [
          'level' => $level,
          'v' => $value,
          'categories' => $categoriesAll,
          'related' => $related,
          'parent' => $parent,
        ]);
        if(!empty($data[$value['id']])) {
          $html .= $this->generateList($data, $value['id'], ($level + 1), $categoriesAll, $related);
        }
      }

      return $html;
    }


    public function actionSaveCategories() {
      $data = $_REQUEST;
      (new Categories())->saveChanges($data);

      echo json_encode(['done' => 1]);
      exit();
    }



    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
