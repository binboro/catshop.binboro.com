function Categories() {

  this.init = function() {
    var self = this;
    //$('.c_parent select').selectric();

    // $('select[multiple]').multiselect({
    //     columns: 1,
    //     placeholder: 'Select options'
    // });

    $('#saveCategories').on('click', function(){
      self.saveChanges();
    });

  }

  this.saveChanges = function() {

    var self = this;
    var data = {};
    $('.categories_list>ul>li').each(function(){
      var id = $(this).data('id');
      data[id] = {};
      data[id]['parent']  = ($('#parent_' + id).val()) ? $('#parent_' + id).val() : 0;
      data[id]['gender']  = ($(this).find('input[name="gender_' + id + '"]:checked').length) ? $(this).find('input[name="gender_' + id + '"]:checked').val() : -1;
      data[id]['show']    = ($("#visible_" + id).prop('checked')) ? 1 : 0;
      data[id]['related'] = ($('#related_' + id).val()) ? $('#related_' + id).val() : [];
    });

    console.log(data);

    $.ajax({
      method: "POST",
      url: "/site/save-categories",
      data : data,
      //dataType: "script",
      success: function(data){
        console.log(data);
      },
    });

  }


}


var categories = false;
$(document).ready(function(){
  categories = new Categories();
  categories.init();
});
