function selectTabs(){
    $('.tabs a').on('click', function(e){
        e.preventDefault();
        $('.tabs_block').hide();

        var thisBlock = $('.block_' + this.id);
        thisBlock.show();
        var emptyBlock = thisBlock.html().trim();
        if(emptyBlock.length == 0){
            thisBlock.text("No child categories");
        }
    });
}


$(document).ready(function(){
    selectTabs();
});
