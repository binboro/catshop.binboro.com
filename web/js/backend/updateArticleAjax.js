function createArticleAjax(){
    $('.add-link-ajax').on('click', function(e){

        e.preventDefault();

        var id = $('.id-for-link').val();

        var links = {};
        $(".ln").each(function(){
           links[$(this).attr('id')] = $(this).val();
        });

        $.ajax({
            type: "POST",
            url: "/admin/links/setlinks",
            data: {id:id, links:links},
        }).success(function(data)
          {
              $("#w0").submit();
          });

    });
}

function updateArticleAjax(){
    $('.add-ajax').on('click', function(e){

        e.preventDefault();
        var self = $(this);

        // Saving
        self.text("Saving");
        self.css("background", "#00BCD4");
        $('.saving').text("");

        var id = $('.id-for-link').val();

        var links = {};
        $(".ln").each(function(){
           links[$(this).attr('id')] = $(this).val();
        });

        $.ajax({
            type: "POST",
            url: "/admin/links/setlinks",
            data: {id:id, links:links},
        }).success(function(data)
          {
              // After saving
              setTimeout(function(){
                  self.text("Update");
                  self.css("background", "#009688");
                  $('.saving').text("Article has been successfully updated");
              }, 500)

          });

        // Update other fields for article
        var title = $('#articles-title').val();
        var description = $('#articles-description').val();
        var keyword = $('#articles-keyword').val();
        var video = $('#articles-video').val();
        var content = $('.redactor-editor').html();
        var status = $('#articles-status').val();
        var readFullArticle = $('#articles-read_full_article').val();

        $.ajax({
            type: "POST",
            url: "/admin/articles/update-article-ajax",
            data: {
              id : id,
              title : title,
              video : video,
              status : status,
              content : content,
              keyword : keyword,
              description : description,
              readFullArticle : readFullArticle,
              },
        });
    });
}

function updateImageArticleAjax(){
    $('#articles-file').change(function(){

    		var id = $('.id-for-link').val();

    		var formElement = document.querySelector("form");
    		var request = new XMLHttpRequest();
    		request.open("POST", "/admin/articles/update-article-ajax-image?id=" + id);
    		request.send(new FormData(formElement));
    		request.onload = function(e){
    				if (request.readyState === 4) {
    					if (request.status === 200) {
    							$('.admin-main-image').attr("src", "/web/uploads/articles/" + request.responseText);
    					}
    			}
    		}

    });
}

function articleTranspostion(btn) {
  var data = {
    'site': jQuery(btn).data('site'),
    'db': jQuery(btn).data('db'),
    'id': jQuery(btn).data('id'),
    'isChecking': true,
  }
  jQuery.ajax({
    url: '/test.php',
    type: 'POST',
    data: data,
    success: function(returnData) {
      delete data.isChecking;
      if (returnData == 'true') {
        var isConfirm = confirm('Record has already existed. Do you really want to create new record?');
        if (isConfirm != true) {
          return -1;
        }
      }
      jQuery.ajax({
        url: '/test.php',
        type: 'POST',
        data: data,
        success: function() {
          alert('Record has been created!');
        },
        error: function() {
          alert('Something wronged! Please talk to developers!')
        }
      });
    }
  });
}

$(document).ready(function(){

    // Update articles for ajax
    createArticleAjax();
    updateArticleAjax();
    updateImageArticleAjax();

});
