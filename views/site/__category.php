<li data-id='<?= $v['id'] ?>' style="margin-left: <?= 20 * $level; ?>px">
  <div class="categriesParams c_name">
    <?= $v['category_name']; ?>
  </div>

  <div class="categriesParams c_related">
    <select id="related_<?= $v['id'] ?>" name="parent" multiple data-id="<?= $v['id'] ?>">
      <?php foreach ($categories as $c_id => $c_name): ?>
        <?php
          $selected = '';
          if(!empty($related[$v['id']]))
            if(in_array($c_id, $related[$v['id']]))
              {
                  $selected  = "selected";
              }
           ?>
        <option <?= $selected; ?> value="<?= $c_id ?>"><?= $c_name ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="categriesParams c_parent">
    <select id="parent_<?= $v['id'] ?>" name="parent" data-id="<?= $v['id'] ?>">
      <option value="0">None</option>
      <?php foreach ($categories as $c_id => $c_name): ?>

        <option  <?php if($parent == $c_id) { echo "selected"; } ?> value="<?= $c_id ?>"><?= $c_name ?></option>
      <?php endforeach; ?>
    </select>
  </div>

  <div class="categriesParams c_gender">
    <input <?php if($v['gender'] == 2 ) { echo "checked"; } ?> id="men_gender_<?= $v['id']; ?>" type='radio' name="gender_<?= $v['id']; ?>" value="2" />
    <label for="men_gender_<?= $v['id']; ?>">Men</label>

    <input <?php if($v['gender'] == 1 ) { echo "checked"; } ?> id="women_gender_<?= $v['id']; ?>" type='radio' name="gender_<?= $v['id']; ?>" value="1" />
    <label for="women_gender_<?= $v['id']; ?>">Women</label>

    <input <?php if($v['gender'] == 0 ) { echo "checked"; } ?> id="unisex_gender_<?= $v['id']; ?>" type='radio' name="gender_<?= $v['id']; ?>" value="0" />
    <label for="unisex_gender_<?= $v['id']; ?>">Unisex</label>
  </div>

  <div class="categriesParams c_visible">
    <input <?php if($v['show'] == 1 ) { echo "checked"; } ?> type="checkbox" id="visible_<?= $v['id'] ?>" data-id="<?= $v['id'] ?>" />
    <label for="visible_<?= $v['id'] ?>">Show</label>
  </div>

</li>
