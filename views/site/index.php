<?php

/* @var $this yii\web\View */

$this->title = 'Shop Categories';

$mainCategs = ['3' => 'News', '4' => 'Designers', '5' => 'Clothing', '6' => 'Shoes', '7' => 'Bags',
               '8' => 'Accessories', '9' => 'Jewelery', '10' => 'Vintage', '11' => 'Beauty', '12' => 'Boutuques'];

?>
<div id="categories">

  <div class="categories_list">
    <div class="main_categories ul_blocks first_block">
      <ul id="categ_buttons">

        <?php foreach ($mainCategs as $id => $name) : ?>
          <li id="main_categ_<?= $id ?>">
            <?= $name ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>

    <div class="second_block ul_blocks">
      <ul id="selected_categs">

      </ul>
    </div>

    <div class="second_block ul_blocks">
      <ul id="unselected_categs">

      </ul>
    </div>

    <div id="saveCategories">
      Save
    </div>
  </div>

</div>
