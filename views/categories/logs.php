<style media="screen">
    .short-logs {
        margin: 10px 0 50px 0;
    }
    table {
        border-collapse: collapse;
    }
    thead {
        background-color: #f3f3f3;
    }
    tr td:first-child {
        background-color: #f3f3f3;
    }
    td {
        padding: 5px;
        border: 1px solid black;
    }
    .logs {
        margin: 0 10px;
    }
    .logs_head {
        font-size: 20px;
        margin-bottom: 10px;
    }
    .regulable-categories, .categories-without-relations, .categories-with-relations, .related-categories {
        display: inline-block;
    }
</style>

<div class="short-logs">
    <p>
        Count
    </p>
    <div>
        All: <?php echo $count; ?>
    </div>
    <div>
        Categories without relations: <?php echo $without_relations['count'];  ?>
    </div>
    <div>
        Categories with relations: <?php echo $with_relations['count']; ?>
    </div>

    <p>
        Related count: <?php echo $related['count']; ?>
    </p>
</div>

<div class="logs regulable-categories">
    <div class="logs_head">Regulable categories</div>
    <table >
        <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Id
                </td>
                <td>
                    Name
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 1;
                foreach ($regulable as $r) {
            ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $r['category_id']; ?>
                        </td>
                        <td>
                            <?php echo $r['category_name']; ?>
                        </td>
                    </tr>
            <?php
                    ++$i;
                }
            ?>
        </tbody>
    </table>
</div>

<div class="logs categories-without-relations">
    <div class="logs_head">Categories without relations</div>
    <table>
        <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Id
                </td>
                <td>
                    Name
                </td>
                <td>
                    Gender
                </td>
                <td>
                    Show
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 1;
                foreach ($without_relations['categories'] as $cwr) {
            ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $cwr['id']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['category_name']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['gender']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['show']; ?>
                        </td>
                    </tr>
            <?php
                    ++$i;
                }
            ?>
        </tbody>
    </table>
</div>

<div class="logs categories-with-relations">
    <div class="logs_head">Categories with relations</div>
    <table>
        <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Id
                </td>
                <td>
                    Name
                </td>
                <td>
                    Parent Id
                </td>
                <td>
                    Gender
                </td>
                <td>
                    Show
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 1;
                foreach ($with_relations['categories'] as $cwr) {
            ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $cwr['id']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['category_name']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['parent_category']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['gender']; ?>
                        </td>
                        <td>
                            <?php echo $cwr['show']; ?>
                        </td>
                    </tr>
            <?php
                    ++$i;
                }
            ?>
        </tbody>
    </table>
</div>

<div class="logs related-categories">
    <div class="logs_head">Related categories</div>
    <table>
        <thead>
            <tr>
                <td>
                    #
                </td>
                <td>
                    Id
                </td>
                <td>
                    Name
                </td>
                <td>
                    Main Id
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
                $i = 1;
                foreach ($related['categories'] as $r) {
            ?>
                    <tr>
                        <td>
                            <?php echo $i; ?>
                        </td>
                        <td>
                            <?php echo $r['related']; ?>
                        </td>
                        <td>
                            <?php echo $r['category_name']; ?>
                        </td>
                        <td>
                            <?php echo $r['main_category']; ?>
                        </td>
                    </tr>
            <?php
                    ++$i;
                }
            ?>
        </tbody>
    </table>
</div>
